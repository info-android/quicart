package com.quicart.quicart.Helper;

public class Constants {
    public static final  String PRODUCT_NAME="PRODUCT_NAME";
    public static final  String PK_TEST="pk_test_679fe49f1b508ad012521e06bdebb910f89ce4db";
    public static final  String PK_LIVE="pk_live_c53c50171cbd1cd355d1d1d6616f58e499898203";
    public static final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    public static final  String PRODUCT_ID="PRODUCT_ID";
    public static final  String PRODUCT_IMAGE="PRODUCT_IMAGE";
    public static final  String CATEGORY_IMG_URL="https://www.app.quicart.ng/storage/app/public/category_images/thumbs/";
    public static final  String PRODUCT_IMG_URL="https://www.app.quicart.ng/storage/app/public/products/thumbs/";
    public static final  String PROFILE_IMG_URL="https://www.app.quicart.ng/storage/app/public/profile_picture/";
    public static final  String BANNER_IMG_URL="https://www.app.quicart.ng/storage/app/public/content/";
    public static final  String PRODUCT_PRICE="PRODUCT_PRICE";
    public static final  String PRODUCT_DESC="PRODUCT_DESC";
    public static final  String PRODUCT_DIC_PRICE="PRODUCT_DIC_PRICE";
    public static final  String DESC="DESC";
    public static final  String IS_FAVORITE="IS_FAVORITE";
    public static final  String OWNER_ID="OWNER_ID";
    public static final  String LOCAL_ADDRESS="LOCAL_ADDRESS";
    public static final  String CART_ID="CART_ID";
    public static final  String CART_NAME="CART_NAME";
    public static final  String CART_MEMBER="CART_MEMBER";
    public static final  String QUANTITY="QUANTITY";
    public static final  String NOTE="NOTE";
    public static final  String ATTR_NAME="ATTR_NAME";
    public static final  String FROM="FROM";
    public static final  String CURRENCY="₦";
    public static String FILTER_MIN="";
    public static String FILTER_MAX="";

    public static final String REGISTRATION_COMPLETE = "registrationComplete";
    public static final String PUSH_NOTIFICATION = "pushNotification";
    public static final String SHARED_PREF = "ah_firebase";


    public static final  String DETAILS_ARRAY="DETAILS_ARRAY";
    public static String USER_SELECTED_GOAL = "USER_SELECTED_GOAL";


    public static final  String GRP_USR_ID="GRP_USR_ID";
    public static final  String GRP_USR="GRP_USR";
    public static String SRCH_KEY="";
    public static String RADIUS="5";
    public static Integer FIRST=1;




    public static final  String DETAILS_ID="DETAILS_ID";
    public static final  String ORD_MASTER_ID="ORD_MASTER_ID";
    public static final  String SELLER_ID="SELLER_ID";
    public static final  String SELLER_DESCRIPTION="SELLER_DESCRIPTION";
    public static final  String SELLER_CITY="SELLER_CITY";
    public static final  String SELLER_STATE="SELLER_STATE";
    public static final  String SELLER_IMG="SELLER_IMG";
    public static final  String SELLER_NAME="SELLER_NAME";
    public static final  String SELLER_COMPANY="SELLER_COMPANY";
    public static final  String SELLER_ADDRESS="SELLER_ADDRESS";
    public static final  String SELLER_LAT="SELLER_LAT";
    public static final  String SELLER_LNG="SELLER_LNG";
    public static final  String DISC_DATE="DISC_DATE";


    public static final  String SHOPPER_ADDRESS="SHOPPER_ADDRESS";
    public static final  String SHOPPER_NAME="SHOPPER_NAME";
    public static final  String SHOPPER_IMAGE="SHOPPER_IMAGE";
    public static final  String SHOPPER_DESC="SHOPPER_DESC";
    public static final  String SHOPPER_ID="SHOPPER_ID";
    public static final  String SHOPPER_REVW="SHOPPER_REVW";
    public static final  String ADD_ID="ADD_ID";


    public static final  String ORD_ID="ORD_ID";
    public static final  String ORD_NUMBER="ORD_NUMBER";
    public static final  String ORD_DATE="ORD_DATE";
    public static final  String CRE_DATE="CRE_DATE";
    public static final  String ORD_TOTAL="ORD_TOTAL";
    public static final  String CANCEL_FEE="CANCEL_FEE";
    public static final  String ORD_STAT="ORD_STAT";
    public static final  String SHOPPER_AMOUNT="SHOPPER_AMOUNT";
    public static final  String TOTAL_PAYBLE="TOTAL_PAYBLE";
    public static final  String PAYMENT_DATE="PAYMENT_DATE";
    public static final  String CUS_NAME="CUS_NAME";
    public static final  String USER_TO_SELLER="USER_TO_SELLER";
    public static final  String USER_TO_SHOPPER="USER_TO_SHOPPER";
    public static final  String MESSAGE="MESSAGE";
    public static final  String DATE="DATE";
    public static final  String WALL_USED="WALL_USED";
    public static final  String IS_PAID="IS_PAID";
    public static final  String IS_SELLER_REQUEST="IS_SELLER_REQUEST";
    public static final  String PAYMENT_ME5HOD="PAYMENT_ME5HOD";
    public static final  String ORD_TYPE="ORD_TYPE";
    public static final  String RS_DATE="RS_DATE";
    public static final  String RS_TIME="RS_TIME";
    public static final  String ORD_SPAN="ORD_SPAN";
    public static final  String ORD_DAY="ORD_DAY";

    public static final  String RATE_POINT="RATE_POINT";
    public static final  String RATE_DATE="RATE_DATE";
    public static final  String RATE_COMMENT="RATE_COMMENT";



    public static String DEVICE_ID="DEVICE_ID";


    public  static Double PICK_LANG = null;
    public static Double PICK_LAT = null;


    public static Double USER_LAT = null;
    public static Double USER_LANG = null;


    public static String[] RATINGS;
    public static String[] COMMENTS;
    public static String[] SELLERS;

    public static String[] COUNTRIES ;


}
