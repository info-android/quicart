package com.quicart.quicart.Adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.quicart.quicart.Activity.ProductDetailsActivity;
import com.quicart.quicart.Activity.ShoppingCartActivity;
import com.quicart.quicart.Helper.MySingleton;
import com.quicart.quicart.R;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import static com.quicart.quicart.Helper.Constants.ATTR_NAME;
import static com.quicart.quicart.Helper.Constants.DETAILS_ID;
import static com.quicart.quicart.Helper.Constants.DEVICE_ID;
import static com.quicart.quicart.Helper.Constants.NOTE;
import static com.quicart.quicart.Helper.Constants.PRODUCT_DESC;
import static com.quicart.quicart.Helper.Constants.PRODUCT_DIC_PRICE;
import static com.quicart.quicart.Helper.Constants.PRODUCT_ID;
import static com.quicart.quicart.Helper.Constants.PRODUCT_IMAGE;
import static com.quicart.quicart.Helper.Constants.PRODUCT_IMG_URL;
import static com.quicart.quicart.Helper.Constants.PRODUCT_NAME;
import static com.quicart.quicart.Helper.Constants.PRODUCT_PRICE;
import static com.quicart.quicart.Helper.Constants.QUANTITY;

public class CartSubAdapter extends RecyclerView.Adapter<CartSubAdapter.ViewHolder> {
    private Activity activity;
    private ArrayList<HashMap<String,String>> map_list;
    private ArrayList<HashMap<String,String>> cart_sub_map_list;
    String user_id="";
    private Integer[] qaunti = new Integer[10000];

    public CartSubAdapter(Activity activity, ArrayList<HashMap<String, String>> map_list) {
        this.activity = activity;
        this.map_list = map_list;

    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.cart_sub_item, parent, false);

        return new ViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        final HashMap<String,String> map =map_list.get(position);
        qaunti[position] = 1;
        Log.e("ISD", map.get(PRODUCT_ID));
        user_id = PreferenceManager.getDefaultSharedPreferences(activity).getString("user_id","");
        holder.container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //MethodClass.go_to_next_activity(activity, ProductDetailsActivity.class);
                Intent intent=new Intent(activity, ProductDetailsActivity.class);
                intent.putExtra("id",map.get(PRODUCT_ID));
                activity.startActivity(intent);
            }
        });

        Picasso.with(activity)
                .load(PRODUCT_IMG_URL+map.get(PRODUCT_IMAGE))
                .placeholder(R.drawable.ic_spinner_of_dots) //this is optional the image to display while the url image is downloading
                .error(R.drawable.ic_spinner_of_dots)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                .into(holder.prdct_iv);

        holder.prdct_title_tv.setText(map.get(PRODUCT_NAME));
        holder.prdct_dtls_tv.setText(map.get(PRODUCT_DESC));
        if(!map.get(ATTR_NAME).equals("null") && !map.get(ATTR_NAME).equals("") && !map.get(ATTR_NAME).equals(null)){
            holder.price.setText("₦"+map.get(PRODUCT_PRICE)+"/"+map.get(ATTR_NAME));
        }else {
            holder.price.setText("₦"+map.get(PRODUCT_PRICE));
        }

        if(!map.get(PRODUCT_DIC_PRICE).equals("null") && !map.get(PRODUCT_DIC_PRICE).equals(null)&& !map.get(PRODUCT_DIC_PRICE).equals("0.00")){
            double res = (Double.parseDouble(map.get(PRODUCT_DIC_PRICE)) / Double.parseDouble(map.get(PRODUCT_PRICE))) * 100;
            holder.discount.setText(String.format("%.2f",res)+"% Off");
            holder.discount.setVisibility(View.VISIBLE);
        }else{
            holder.discount.setVisibility(View.GONE);
        }
        Log.e("QUAN", map.get(QUANTITY) );
        holder.qantity.setText(map.get(QUANTITY));
        qaunti[position] = Integer.parseInt(map.get(QUANTITY));

        holder.plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(qaunti[position] >= 1){
                    qaunti[position] = qaunti[position]+1;
                    String server_url = activity.getString(R.string.SERVER_URL)+"update-to-cart";
                    final String token = activity.getString(R.string.AUTH_KEY);
                    HashMap<String, String> params1 = new HashMap<String, String>();
                    params1.put("secret_key", token);
                    if(!user_id.equals("")){
                        params1.put("owner_id", user_id);
                        params1.put("logged_in", "true");

                    }else{
                        params1.put("owner_id", DEVICE_ID);
                        params1.put("logged_in", "false");
                    }
                    params1.put("cart_details_id", map.get(DETAILS_ID));
                    params1.put("quantity", String.valueOf(qaunti[position]));
                    Log.e("avv", new JSONObject(params1).toString());
                    showProgressDialog();
                    JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, server_url, new JSONObject(params1), new Response.Listener<JSONObject>() {

                        @Override
                        public void onResponse(JSONObject response) {
                            Log.e("ERROR", response.toString());
                            //Toast.makeText(BussinessSignupActivity.this, response.toString(), Toast.LENGTH_LONG).show();
                            try {
                                hideProgressDialog();
                                String status = response.getString("status");
                                //Toast.makeText(BussinessSignupActivity.this, status, Toast.LENGTH_LONG).show();
                                if (status.equals("SUCCESS")) {
                                    Intent I = new Intent(activity,ShoppingCartActivity.class);
                                    activity.startActivity(I);
                                    activity.finish();
                                }else {
                                    new SweetAlertDialog(activity, SweetAlertDialog.ERROR_TYPE)
                                            .setTitleText("Network Error")
                                            .setContentText("Please check your internet connection.")
                                            .setConfirmText("Settings")
                                            .setCancelText("Okay")
                                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                @Override
                                                public void onClick(SweetAlertDialog sDialog) {
                                                    sDialog.dismissWithAnimation();
                                                    activity.startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 1);
                                                }
                                            })
                                            .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                @Override
                                                public void onClick(SweetAlertDialog sDialog) {
                                                    sDialog.dismissWithAnimation();
                                                }
                                            })
                                            .show();
                                }
                            } catch (JSONException e) {
                                hideProgressDialog();
                                Log.e("ERROR", e.toString());
                                new SweetAlertDialog(activity, SweetAlertDialog.ERROR_TYPE)
                                        .setTitleText("Network Error")
                                        .setContentText("Please check your internet connection.")
                                        .setConfirmText("Settings")
                                        .setCancelText("Okay")
                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sDialog) {
                                                sDialog.dismissWithAnimation();
                                                activity.startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 1);
                                            }
                                        })
                                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sDialog) {
                                                sDialog.dismissWithAnimation();
                                            }
                                        })
                                        .show();
                            }
                        }
                    }, new Response.ErrorListener() {

                        @Override
                        public void onErrorResponse(VolleyError error) {
                            hideProgressDialog();
                            Log.e("ERROR", error.toString());
                            new SweetAlertDialog(activity, SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText("Network Error")
                                    .setContentText("Please check your internet connection.")
                                    .setConfirmText("Settings")
                                    .setCancelText("Okay")
                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sDialog) {
                                            sDialog.dismissWithAnimation();
                                            activity.startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 1);
                                        }
                                    })
                                    .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sDialog) {
                                            sDialog.dismissWithAnimation();
                                        }
                                    })
                                    .show();
                        }
                    });
                    jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(50 * 1000, 3,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    // Access the RequestQueue through your singleton class.
                    MySingleton.getInstance(activity).addToRequestQueue(jsObjRequest);
                }
            }
        });
        holder.minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(qaunti[position] >1){
                    qaunti[position] = qaunti[position]-1;
                    String server_url = activity.getString(R.string.SERVER_URL)+"update-to-cart";
                    final String token = activity.getString(R.string.AUTH_KEY);
                    HashMap<String, String> params1 = new HashMap<String, String>();
                    params1.put("secret_key", token);
                    if(!user_id.equals("")){
                        params1.put("owner_id", user_id);
                        params1.put("logged_in", "true");

                    }else{
                        params1.put("owner_id", DEVICE_ID);
                        params1.put("logged_in", "false");
                    }
                    params1.put("cart_details_id", map.get(DETAILS_ID));
                    params1.put("quantity", String.valueOf(qaunti[position]));
                    Log.e("avv", new JSONObject(params1).toString());
                    showProgressDialog();
                    JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, server_url, new JSONObject(params1), new Response.Listener<JSONObject>() {

                        @Override
                        public void onResponse(JSONObject response) {
                            Log.e("ERROR", response.toString());
                            //Toast.makeText(BussinessSignupActivity.this, response.toString(), Toast.LENGTH_LONG).show();
                            try {
                                hideProgressDialog();
                                String status = response.getString("status");
                                //Toast.makeText(BussinessSignupActivity.this, status, Toast.LENGTH_LONG).show();
                                if (status.equals("SUCCESS")) {
                                    Intent I = new Intent(activity,ShoppingCartActivity.class);
                                    activity.startActivity(I);
                                    activity.finish();
                                }else {
                                    new SweetAlertDialog(activity, SweetAlertDialog.ERROR_TYPE)
                                            .setTitleText("Network Error")
                                            .setContentText("Please check your internet connection.")
                                            .setConfirmText("Settings")
                                            .setCancelText("Okay")
                                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                @Override
                                                public void onClick(SweetAlertDialog sDialog) {
                                                    sDialog.dismissWithAnimation();
                                                    activity.startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 1);
                                                }
                                            })
                                            .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                @Override
                                                public void onClick(SweetAlertDialog sDialog) {
                                                    sDialog.dismissWithAnimation();
                                                }
                                            })
                                            .show();
                                }
                            } catch (JSONException e) {
                                hideProgressDialog();
                                Log.e("ERROR", e.toString());
                                new SweetAlertDialog(activity, SweetAlertDialog.ERROR_TYPE)
                                        .setTitleText("Network Error")
                                        .setContentText("Please check your internet connection.")
                                        .setConfirmText("Settings")
                                        .setCancelText("Okay")
                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sDialog) {
                                                sDialog.dismissWithAnimation();
                                                activity.startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 1);
                                            }
                                        })
                                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sDialog) {
                                                sDialog.dismissWithAnimation();
                                            }
                                        })
                                        .show();
                            }
                        }
                    }, new Response.ErrorListener() {

                        @Override
                        public void onErrorResponse(VolleyError error) {
                            hideProgressDialog();
                            Log.e("ERROR", error.toString());
                            new SweetAlertDialog(activity, SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText("Network Error")
                                    .setContentText("Please check your internet connection.")
                                    .setConfirmText("Settings")
                                    .setCancelText("Okay")
                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sDialog) {
                                            sDialog.dismissWithAnimation();
                                            activity.startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 1);
                                        }
                                    })
                                    .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sDialog) {
                                            sDialog.dismissWithAnimation();
                                        }
                                    })
                                    .show();
                        }
                    });
                    jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(50 * 1000, 3,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    // Access the RequestQueue through your singleton class.
                    MySingleton.getInstance(activity).addToRequestQueue(jsObjRequest);
                }
            }
        });


        holder.img_add_notes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.img_add_notes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        final Dialog dialog = new Dialog(activity);
                        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialog.setContentView(R.layout.add_notes_lay_popup);
                        ImageView close = (ImageView) dialog.findViewById(R.id.close);
                        Button save = (Button) dialog.findViewById(R.id.save);
                        final EditText editNote = (EditText) dialog.findViewById(R.id.editNote);
                        Button cancel = (Button) dialog.findViewById(R.id.cancel);
                        if(!map.get(NOTE).equals("null") && !map.get(NOTE).equals(null)){
                            editNote.setText(map.get(NOTE));
                        }
                        close.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                dialog.dismiss();
                            }
                        });
                        save.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                if(editNote.getText().toString().length() == 0){
                                    editNote.setError("Please add your note");
                                    editNote.requestFocus();
                                    return;
                                }
                                String server_url = activity.getString(R.string.SERVER_URL)+"update-cart-note";
                                final String token = activity.getString(R.string.AUTH_KEY);
                                HashMap<String, String> params1 = new HashMap<String, String>();
                                params1.put("secret_key", token);
                                if(!user_id.equals("")){
                                    params1.put("owner_id", user_id);
                                    params1.put("logged_in", "true");

                                }else{
                                    params1.put("owner_id", DEVICE_ID);
                                    params1.put("logged_in", "false");
                                }
                                params1.put("cart_details_id", map.get(DETAILS_ID));
                                params1.put("note", editNote.getText().toString());
                                Log.e("avv", new JSONObject(params1).toString());
                                showProgressDialog();
                                JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, server_url, new JSONObject(params1), new Response.Listener<JSONObject>() {

                                    @Override
                                    public void onResponse(JSONObject response) {
                                        Log.e("ERROR", response.toString());
                                        //Toast.makeText(BussinessSignupActivity.this, response.toString(), Toast.LENGTH_LONG).show();
                                        try {
                                            hideProgressDialog();
                                            String status = response.getString("status");
                                            //Toast.makeText(BussinessSignupActivity.this, status, Toast.LENGTH_LONG).show();
                                            if (status.equals("SUCCESS")) {
                                                dialog.dismiss();
                                                new SweetAlertDialog(activity, SweetAlertDialog.SUCCESS_TYPE)
                                                        .setTitleText("Note Added")
                                                        .setContentText("You have added a note for this item.")
                                                        .setConfirmText("Okay")
                                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                            @Override
                                                            public void onClick(SweetAlertDialog sDialog) {
                                                                sDialog.dismissWithAnimation();
                                                                ((ShoppingCartActivity)activity).get_product_list();
                                                            }
                                                        })

                                                        .show();

                                            }else {
                                                new SweetAlertDialog(activity, SweetAlertDialog.ERROR_TYPE)
                                                        .setTitleText("Network Error")
                                                        .setContentText("Please check your internet connection.")
                                                        .setConfirmText("Settings")
                                                        .setCancelText("Okay")
                                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                            @Override
                                                            public void onClick(SweetAlertDialog sDialog) {
                                                                sDialog.dismissWithAnimation();
                                                                activity.startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 1);
                                                            }
                                                        })
                                                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                            @Override
                                                            public void onClick(SweetAlertDialog sDialog) {
                                                                sDialog.dismissWithAnimation();
                                                            }
                                                        })
                                                        .show();
                                            }
                                        } catch (JSONException e) {
                                            Log.e("ERROR", e.toString());
                                            hideProgressDialog();
                                            new SweetAlertDialog(activity, SweetAlertDialog.ERROR_TYPE)
                                                    .setTitleText("Network Error")
                                                    .setContentText("Please check your internet connection.")
                                                    .setConfirmText("Settings")
                                                    .setCancelText("Okay")
                                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                        @Override
                                                        public void onClick(SweetAlertDialog sDialog) {
                                                            sDialog.dismissWithAnimation();
                                                            activity.startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 1);
                                                        }
                                                    })
                                                    .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                        @Override
                                                        public void onClick(SweetAlertDialog sDialog) {
                                                            sDialog.dismissWithAnimation();
                                                        }
                                                    })
                                                    .show();
                                        }
                                    }
                                }, new Response.ErrorListener() {

                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        Log.e("ERROR", error.toString());
                                        hideProgressDialog();
                                        new SweetAlertDialog(activity, SweetAlertDialog.ERROR_TYPE)
                                                .setTitleText("Network Error")
                                                .setContentText("Please check your internet connection.")
                                                .setConfirmText("Settings")
                                                .setCancelText("Okay")
                                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                    @Override
                                                    public void onClick(SweetAlertDialog sDialog) {
                                                        sDialog.dismissWithAnimation();
                                                        activity.startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 1);
                                                    }
                                                })
                                                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                    @Override
                                                    public void onClick(SweetAlertDialog sDialog) {
                                                        sDialog.dismissWithAnimation();
                                                    }
                                                })
                                                .show();
                                    }
                                });
                                jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(50 * 1000, 3,
                                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                                // Access the RequestQueue through your singleton class.
                                MySingleton.getInstance(activity).addToRequestQueue(jsObjRequest);
                            }
                        });
                        cancel.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                dialog.dismiss();
                            }
                        });
                        dialog.show();
                    }
                });
            }
        });
        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new SweetAlertDialog(activity, SweetAlertDialog.WARNING_TYPE)
                        .setTitleText("Remove Product")
                        .setContentText("Do you want to remove this product?")
                        .setConfirmText("Yes")
                        .setCancelText("No")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();
                                String server_url = activity.getString(R.string.SERVER_URL)+"delete-to-cart";
                                final String token = activity.getString(R.string.AUTH_KEY);
                                HashMap<String, String> params1 = new HashMap<String, String>();
                                params1.put("secret_key", token);
                                if(!user_id.equals("")){
                                    params1.put("owner_id", user_id);
                                    params1.put("logged_in", "true");

                                }else{
                                    params1.put("owner_id", DEVICE_ID);
                                    params1.put("logged_in", "false");
                                }
                                params1.put("cart_details_id", map.get(DETAILS_ID));
                                Log.e("avv", new JSONObject(params1).toString());
                                showProgressDialog();
                                JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, server_url, new JSONObject(params1), new Response.Listener<JSONObject>() {

                                    @Override
                                    public void onResponse(JSONObject response) {
                                        Log.e("ERROR", response.toString());
                                        //Toast.makeText(BussinessSignupActivity.this, response.toString(), Toast.LENGTH_LONG).show();
                                        try {
                                            hideProgressDialog();
                                            String status = response.getString("status");
                                            //Toast.makeText(BussinessSignupActivity.this, status, Toast.LENGTH_LONG).show();
                                            if (status.equals("SUCCESS")) {
                                                new SweetAlertDialog(activity, SweetAlertDialog.SUCCESS_TYPE)
                                                        .setTitleText("Item Removed")
                                                        .setContentText("You have removed the item from your cart.")
                                                        .setConfirmText("Okay")
                                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                            @Override
                                                            public void onClick(SweetAlertDialog sDialog) {
                                                                sDialog.dismissWithAnimation();
                                                                ((ShoppingCartActivity)activity).get_product_list();
                                                            }
                                                        })

                                                        .show();

                                            }else {
                                                new SweetAlertDialog(activity, SweetAlertDialog.ERROR_TYPE)
                                                        .setTitleText("Network Error")
                                                        .setContentText("Please check your internet connection.")
                                                        .setConfirmText("Settings")
                                                        .setCancelText("Okay")
                                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                            @Override
                                                            public void onClick(SweetAlertDialog sDialog) {
                                                                sDialog.dismissWithAnimation();
                                                                activity.startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 1);
                                                            }
                                                        })
                                                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                            @Override
                                                            public void onClick(SweetAlertDialog sDialog) {
                                                                sDialog.dismissWithAnimation();
                                                            }
                                                        })
                                                        .show();
                                            }
                                        } catch (JSONException e) {
                                            Log.e("ERROR", e.toString());
                                            hideProgressDialog();
                                            new SweetAlertDialog(activity, SweetAlertDialog.ERROR_TYPE)
                                                    .setTitleText("Network Error")
                                                    .setContentText("Please check your internet connection.")
                                                    .setConfirmText("Settings")
                                                    .setCancelText("Okay")
                                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                        @Override
                                                        public void onClick(SweetAlertDialog sDialog) {
                                                            sDialog.dismissWithAnimation();
                                                            activity.startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 1);
                                                        }
                                                    })
                                                    .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                        @Override
                                                        public void onClick(SweetAlertDialog sDialog) {
                                                            sDialog.dismissWithAnimation();
                                                        }
                                                    })
                                                    .show();
                                        }
                                    }
                                }, new Response.ErrorListener() {

                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        Log.e("ERROR", error.toString());
                                        hideProgressDialog();
                                        new SweetAlertDialog(activity, SweetAlertDialog.ERROR_TYPE)
                                                .setTitleText("Network Error")
                                                .setContentText("Please check your internet connection.")
                                                .setConfirmText("Settings")
                                                .setCancelText("Okay")
                                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                    @Override
                                                    public void onClick(SweetAlertDialog sDialog) {
                                                        sDialog.dismissWithAnimation();
                                                        activity.startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 1);
                                                    }
                                                })
                                                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                    @Override
                                                    public void onClick(SweetAlertDialog sDialog) {
                                                        sDialog.dismissWithAnimation();
                                                    }
                                                })
                                                .show();
                                    }
                                });
                                jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(50 * 1000, 3,
                                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                                // Access the RequestQueue through your singleton class.
                                MySingleton.getInstance(activity).addToRequestQueue(jsObjRequest);
                            }
                        })
                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();
                            }
                        })

                        .show();
            }
        });
        //holder.edt_price.setId(Integer.parseInt((map.get(BUSINESS_SERVICE_ID))));
    }

    @Override
    public int getItemCount() {
        return map_list == null ? 0 : map_list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private RecyclerView rv_cart_item;
        private TextView img_add_notes;
        private TextView delete;
        private TextView qantity;
        LinearLayout container;
        ImageView prdct_iv;
        TextView discount,prdct_title_tv,prdct_dtls_tv,price;
        private ImageView plus,minus;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            rv_cart_item=(RecyclerView)itemView.findViewById(R.id.rv_cart_item);
            img_add_notes=(TextView)itemView.findViewById(R.id.img_add_notes);
            delete=(TextView)itemView.findViewById(R.id.delete);
            container = (LinearLayout) itemView.findViewById(R.id.container);
            prdct_iv = (ImageView) itemView.findViewById(R.id.prdct_iv);
            plus = (ImageView) itemView.findViewById(R.id.plus);
            minus = (ImageView) itemView.findViewById(R.id.minus);
            discount = (TextView) itemView.findViewById(R.id.discount);
            prdct_title_tv = (TextView) itemView.findViewById(R.id.prdct_title_tv);
            prdct_dtls_tv = (TextView) itemView.findViewById(R.id.prdct_dtls_tv);
            price = (TextView) itemView.findViewById(R.id.price);
            qantity = (TextView) itemView.findViewById(R.id.qantity);

        }
    }

    Dialog mDialog;
    private void showProgressDialog(){
        mDialog = new Dialog(activity);
        mDialog.setCancelable(false);
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        mDialog.setContentView(R.layout.custom_progress_dialog);
        mDialog.show();
    }
    private void hideProgressDialog(){
        mDialog.dismiss();
    }

}
