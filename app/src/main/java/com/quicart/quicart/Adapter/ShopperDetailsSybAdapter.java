package com.quicart.quicart.Adapter;

import android.app.Activity;
import android.content.Intent;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.quicart.quicart.Activity.ProductDetailsActivity;
import com.quicart.quicart.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;

import static com.quicart.quicart.Helper.Constants.ATTR_NAME;
import static com.quicart.quicart.Helper.Constants.NOTE;
import static com.quicart.quicart.Helper.Constants.PRODUCT_DESC;
import static com.quicart.quicart.Helper.Constants.PRODUCT_ID;
import static com.quicart.quicart.Helper.Constants.PRODUCT_IMAGE;
import static com.quicart.quicart.Helper.Constants.PRODUCT_IMG_URL;
import static com.quicart.quicart.Helper.Constants.PRODUCT_NAME;
import static com.quicart.quicart.Helper.Constants.PRODUCT_PRICE;
import static com.quicart.quicart.Helper.Constants.QUANTITY;

public class ShopperDetailsSybAdapter extends RecyclerView.Adapter<ShopperDetailsSybAdapter.OrderVh> {
    public ArrayList<HashMap<String, String>> list;
    Activity activity;
    String user_id = "";

    //innitialize listview adapter
    public ShopperDetailsSybAdapter(Activity activity, ArrayList<HashMap<String, String>> list) {
        super();
        this.activity = activity;
        this.list = list;
    }

    @Override
    public OrderVh onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.shopperdetailsitem, parent, false);
        return new OrderVh(view);
    }

    //override function for adapter extend to baseadater
    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    @Override
    public void onBindViewHolder(final OrderVh holder, final int position) {
        final HashMap<String, String> map = list.get(position);
        user_id = PreferenceManager.getDefaultSharedPreferences(activity).getString("user_id", "");
        holder.container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //MethodClass.go_to_next_activity(activity, ProductDetailsActivity.class);
                Intent intent = new Intent(activity, ProductDetailsActivity.class);
                intent.putExtra("id", map.get(PRODUCT_ID));
                activity.startActivity(intent);
            }
        });

        Picasso.with(activity)
                .load(PRODUCT_IMG_URL + map.get(PRODUCT_IMAGE))
                .placeholder(R.drawable.ic_spinner_of_dots) //this is optional the image to display while the url image is downloading
                .error(R.drawable.ic_spinner_of_dots)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                .into(holder.prdct_iv);

        holder.prdct_title_tv.setText(map.get(PRODUCT_NAME));
        holder.prdct_dtls_tv.setText(map.get(PRODUCT_DESC));
        if (!map.get(ATTR_NAME).equals("null") && !map.get(ATTR_NAME).equals("") && !map.get(ATTR_NAME).equals(null)) {
            holder.price.setText("₦" + map.get(PRODUCT_PRICE) + "/" + map.get(ATTR_NAME));
        } else {
            holder.price.setText("₦" + map.get(PRODUCT_PRICE));
        }

        holder.qantity.setText(map.get(QUANTITY));
        if(!map.get(NOTE).equals("null") && !map.get(NOTE).equals("") && !map.get(NOTE).equals(null)){
            holder.prdct_note_tv.setText("Note: "+map.get(NOTE));
            holder.prdct_note_tv.setVisibility(View.VISIBLE);
        }else {
            holder.prdct_note_tv.setVisibility(View.GONE);
        }
    }


    //create holder according to id of view page
    public static class OrderVh extends RecyclerView.ViewHolder {

        private RecyclerView rv_cart_item;
        private ImageView img_add_notes;
        private ImageView delete;
        private TextView qantity;
        LinearLayout container;
        ImageView prdct_iv;
        TextView discount, prdct_title_tv, prdct_dtls_tv, price,prdct_note_tv;

        public OrderVh(@NonNull View itemView) {
            super(itemView);
            rv_cart_item = (RecyclerView) itemView.findViewById(R.id.rv_cart_item);
            //img_add_notes = (ImageView) itemView.findViewById(R.id.img_add_notes);
            delete = (ImageView) itemView.findViewById(R.id.delete);
            container = (LinearLayout) itemView.findViewById(R.id.container);
            prdct_iv = (ImageView) itemView.findViewById(R.id.prdct_iv);
            discount = (TextView) itemView.findViewById(R.id.discount);
            prdct_title_tv = (TextView) itemView.findViewById(R.id.prdct_title_tv);
            prdct_dtls_tv = (TextView) itemView.findViewById(R.id.prdct_dtls_tv);
            prdct_note_tv = (TextView) itemView.findViewById(R.id.prdct_note_tv);
            price = (TextView) itemView.findViewById(R.id.price);
            qantity = (TextView) itemView.findViewById(R.id.qantity);

        }
    }
}