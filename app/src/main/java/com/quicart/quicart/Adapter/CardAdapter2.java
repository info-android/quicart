package com.quicart.quicart.Adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.preference.PreferenceManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.quicart.quicart.Activity.PaymentMethodsActivity;
import com.quicart.quicart.Helper.MySingleton;
import com.quicart.quicart.R;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import static com.quicart.quicart.Helper.Constants.DATE;
import static com.quicart.quicart.Helper.Constants.PRODUCT_DESC;
import static com.quicart.quicart.Helper.Constants.PRODUCT_ID;
import static com.quicart.quicart.Helper.Constants.PRODUCT_NAME;

public class CardAdapter2 extends RecyclerView.Adapter<CardAdapter2.OrderVh> {
    public ArrayList<HashMap<String, String>> list;
    Activity activity;
    String user_id;
    //innitialize listview adapter
    public CardAdapter2(Activity activity, ArrayList<HashMap<String, String>> list) {
        super();
        this.activity = activity;
        this.list = list;
    }

    @Override
    public OrderVh onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.card_layout2, parent, false);
        return new OrderVh(view);
    }

    //override function for adapter extend to baseadater
    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    @Override
    public void onBindViewHolder(final OrderVh holder, final int position) {
        final HashMap<String, String> map = list.get(position);
        user_id = PreferenceManager.getDefaultSharedPreferences(activity).getString("user_id","");

        holder.prdct_title_tv.setText("Card Number: **** **** **** "+map.get(PRODUCT_NAME));
        holder.exp_tv.setText("Expiry: "+map.get(DATE));
        holder.type_tv.setText("Type: "+map.get(PRODUCT_DESC));

        holder.remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new SweetAlertDialog(activity, SweetAlertDialog.WARNING_TYPE)
                        .setTitleText("Delete Card")
                        .setContentText("Do you want to delete the card?")
                        .setConfirmText("Yes")
                        .setCancelText("No")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();
                                String server_url = activity.getString(R.string.SERVER_URL)+"card-details-remove";
                                final String token = activity.getString(R.string.AUTH_KEY);
                                HashMap<String, String> params1 = new HashMap<String, String>();
                                params1.put("secret_key", token);
                                params1.put("id", map.get(PRODUCT_ID));
                                params1.put("user_id", user_id);
                                Log.e("avv", new JSONObject(params1).toString());
                                showProgressDialog();
                                JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, server_url, new JSONObject(params1), new Response.Listener<JSONObject>() {

                                    @Override
                                    public void onResponse(JSONObject response) {
                                        Log.e("ERROR", response.toString());
                                        //Toast.makeText(BussinessSignupActivity.this, response.toString(), Toast.LENGTH_LONG).show();
                                        try {
                                            hideProgressDialog();
                                            String status = response.getString("status");
                                            //Toast.makeText(BussinessSignupActivity.this, status, Toast.LENGTH_LONG).show();
                                            if (status.equals("SUCCESS")) {
                                                ((PaymentMethodsActivity)activity).getCard();
                                                new SweetAlertDialog(activity, SweetAlertDialog.SUCCESS_TYPE)
                                                        .setTitleText("Card deleted")
                                                        .setContentText("Selected card removed succesfully")
                                                        .setConfirmText("Okay")
                                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                            @Override
                                                            public void onClick(SweetAlertDialog sDialog) {
                                                                sDialog.dismissWithAnimation();

                                                            }
                                                        })

                                                        .show();

                                            }else {
                                                new SweetAlertDialog(activity, SweetAlertDialog.ERROR_TYPE)
                                                        .setTitleText("Network Error")
                                                        .setContentText("Please check your internet connection.")
                                                        .setConfirmText("Settings")
                                                        .setCancelText("Okay")
                                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                            @Override
                                                            public void onClick(SweetAlertDialog sDialog) {
                                                                sDialog.dismissWithAnimation();
                                                                activity.startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 1);
                                                            }
                                                        })
                                                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                            @Override
                                                            public void onClick(SweetAlertDialog sDialog) {
                                                                sDialog.dismissWithAnimation();
                                                            }
                                                        })
                                                        .show();
                                            }
                                        } catch (JSONException e) {
                                            Log.e("ERROR", e.toString());
                                            hideProgressDialog();
                                            new SweetAlertDialog(activity, SweetAlertDialog.ERROR_TYPE)
                                                    .setTitleText("Network Error")
                                                    .setContentText("Please check your internet connection.")
                                                    .setConfirmText("Settings")
                                                    .setCancelText("Okay")
                                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                        @Override
                                                        public void onClick(SweetAlertDialog sDialog) {
                                                            sDialog.dismissWithAnimation();
                                                            activity.startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 1);
                                                        }
                                                    })
                                                    .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                        @Override
                                                        public void onClick(SweetAlertDialog sDialog) {
                                                            sDialog.dismissWithAnimation();
                                                        }
                                                    })
                                                    .show();
                                        }
                                    }
                                }, new Response.ErrorListener() {

                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        Log.e("ERROR", error.toString());
                                        hideProgressDialog();
                                        new SweetAlertDialog(activity, SweetAlertDialog.ERROR_TYPE)
                                                .setTitleText("Network Error")
                                                .setContentText("Please check your internet connection.")
                                                .setConfirmText("Settings")
                                                .setCancelText("Okay")
                                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                    @Override
                                                    public void onClick(SweetAlertDialog sDialog) {
                                                        sDialog.dismissWithAnimation();
                                                        activity.startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 1);
                                                    }
                                                })
                                                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                    @Override
                                                    public void onClick(SweetAlertDialog sDialog) {
                                                        sDialog.dismissWithAnimation();
                                                    }
                                                })
                                                .show();
                                    }
                                });
                                jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(50 * 1000, 3,
                                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                                // Access the RequestQueue through your singleton class.
                                MySingleton.getInstance(activity).addToRequestQueue(jsObjRequest);
                            }
                        }).setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();

                                }
                            })
                        .show();
            }
        });
    }


    //create holder according to id of view page
    public static class OrderVh extends RecyclerView.ViewHolder {

        TextView prdct_title_tv;
        TextView exp_tv;
        TextView type_tv;
        RelativeLayout container;
        ImageView remove;
        public OrderVh(View v) {
            super(v);

            prdct_title_tv = (TextView) v.findViewById(R.id.prdct_title_tv);
            exp_tv = (TextView) v.findViewById(R.id.exp_tv);
            type_tv = (TextView) v.findViewById(R.id.type_tv);
            container = (RelativeLayout) v.findViewById(R.id.container);
            remove = (ImageView) v.findViewById(R.id.remove);
        }
    }
    Dialog mDialog;
    private void showProgressDialog(){
        mDialog = new Dialog(activity);
        mDialog.setCancelable(false);
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        mDialog.setContentView(R.layout.custom_progress_dialog);
        mDialog.show();
    }
    private void hideProgressDialog(){
        mDialog.dismiss();
    }
}
