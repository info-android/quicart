package com.quicart.quicart.Adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.quicart.quicart.Activity.ProductDetailsActivity;
import com.quicart.quicart.Helper.MethodClass;
import com.quicart.quicart.R;

import java.util.ArrayList;
import java.util.HashMap;

public class HomeAdapter2 extends RecyclerView.Adapter<HomeAdapter2.ViewHolder> {
    private Activity activity;
    private ArrayList<HashMap<String,String>> map_list;

    public HomeAdapter2(Activity activity, ArrayList<HashMap<String, String>> map_list) {
        this.activity = activity;
        this.map_list = map_list;

    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.home_product_item, parent, false);
        return new ViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        final HashMap<String,String> map =map_list.get(position);
        holder.container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MethodClass.go_to_next_activity(activity, ProductDetailsActivity.class);
            }
        });
        //holder.edt_price.setId(Integer.parseInt((map.get(BUSINESS_SERVICE_ID))));
    }

    @Override
    public int getItemCount() {
        return map_list == null ? 0 : map_list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        RelativeLayout container;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            container = (RelativeLayout) itemView.findViewById(R.id.container);
        }
    }


}
