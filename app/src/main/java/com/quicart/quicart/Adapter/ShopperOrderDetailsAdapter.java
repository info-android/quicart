package com.quicart.quicart.Adapter;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.quicart.quicart.Activity.Shopper.MapActivity;
import com.quicart.quicart.Activity.Shopper.OrderDetailsActivity;
import com.quicart.quicart.R;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.quicart.quicart.Helper.Constants.ATTR_NAME;
import static com.quicart.quicart.Helper.Constants.DETAILS_ARRAY;
import static com.quicart.quicart.Helper.Constants.DETAILS_ID;
import static com.quicart.quicart.Helper.Constants.NOTE;
import static com.quicart.quicart.Helper.Constants.PRODUCT_DESC;
import static com.quicart.quicart.Helper.Constants.PRODUCT_DIC_PRICE;
import static com.quicart.quicart.Helper.Constants.PRODUCT_ID;
import static com.quicart.quicart.Helper.Constants.PRODUCT_IMAGE;
import static com.quicart.quicart.Helper.Constants.PRODUCT_NAME;
import static com.quicart.quicart.Helper.Constants.PRODUCT_PRICE;
import static com.quicart.quicart.Helper.Constants.PROFILE_IMG_URL;
import static com.quicart.quicart.Helper.Constants.QUANTITY;
import static com.quicart.quicart.Helper.Constants.SELLER_ADDRESS;
import static com.quicart.quicart.Helper.Constants.SELLER_COMPANY;
import static com.quicart.quicart.Helper.Constants.SELLER_IMG;
import static com.quicart.quicart.Helper.Constants.SELLER_LAT;
import static com.quicart.quicart.Helper.Constants.SELLER_LNG;
import static com.quicart.quicart.Helper.Constants.SELLER_NAME;

public class ShopperOrderDetailsAdapter extends RecyclerView.Adapter<ShopperOrderDetailsAdapter.OrderVh> {
    public ArrayList<HashMap<String, String>> list;
    public ArrayList<HashMap<String, String>> orderlist;
    Activity activity;

    //innitialize listview adapter
    public ShopperOrderDetailsAdapter(Activity activity, ArrayList<HashMap<String, String>> list) {
        super();
        this.activity = activity;
        this.list = list;
    }

    @Override
    public OrderVh onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.checkoutmain, parent, false);
        return new OrderVh(view);
    }

    //override function for adapter extend to baseadater
    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    @Override
    public void onBindViewHolder(final OrderVh holder, final int position) {
        final HashMap<String, String> map = list.get(position);
        if(!map.get(SELLER_COMPANY).equals("null") && !map.get(SELLER_COMPANY).equals("") && !map.get(SELLER_COMPANY).equals(null)){
            holder.seller.setText("Seller: " + map.get(SELLER_COMPANY));
        }else {
            holder.seller.setText("Seller: " + map.get(SELLER_NAME));
        }

//        holder.seller.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                MethodClass.go_to_next_activity(activity, SellerPublicProfileActivity.class);
//                Intent I = new Intent(activity, SellerPublicProfileActivity.class);
//                I.putExtra("store_id", map.get(SELLER_ID));
//                activity.startActivity(I);
//            }
//        });
        get_product_list(holder.rv_Myorder, map.get(DETAILS_ARRAY));

        holder.location.setText(map.get(SELLER_ADDRESS));
        holder.location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent I = new Intent(activity, MapActivity.class);
                I.putExtra("lat",map.get(SELLER_LAT));
                I.putExtra("lng",map.get(SELLER_LNG));
                if(!map.get(SELLER_COMPANY).equals("null") && !map.get(SELLER_COMPANY).equals("") && !map.get(SELLER_COMPANY).equals(null)){
                    I.putExtra("name",map.get(SELLER_COMPANY));
                }else {
                    I.putExtra("name",map.get(SELLER_NAME));
                }

                I.putExtra("loc",map.get(SELLER_ADDRESS));
                I.putExtra("image",map.get(SELLER_IMG));
                activity.startActivity(I);
//
//                String uri = "https://www.google.com/maps/dir/?api=1&origin=Bd-189,Saltlake,Kolkata,Spain&destination=Gariahat,Westbengal&waypoints=ScienceCity,Kolkata%7CUltodanga,Kolkata&travelmode=driving&dir_action=navigate";
//                Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(uri));
//                intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
//                activity.startActivity(intent);
            }
        });
        Picasso.with(activity).load(PROFILE_IMG_URL + map.get(SELLER_IMG)).placeholder(R.drawable.man).placeholder(R.drawable.man).into(holder.img);
    }

    //create holder according to id of view page
    public static class OrderVh extends RecyclerView.ViewHolder {
        RecyclerView rv_Myorder;
        TextView location, seller;
        CircleImageView img;

        public OrderVh(View v) {
            super(v);
            rv_Myorder = (RecyclerView) v.findViewById(R.id.rv_Myorder);
            location = (TextView) v.findViewById(R.id.location);
            seller = (TextView) v.findViewById(R.id.seller);
            img = (CircleImageView) v.findViewById(R.id.img);
        }
    }

    private void get_product_list(RecyclerView recyclerView, String detArrs) {
        try {
            JSONArray detArr = new JSONArray(detArrs);
            orderlist = new ArrayList<HashMap<String, String>>();
            for (int i = 0; i < detArr.length(); i++) {
                Log.e("ISD", detArr.getJSONObject(i).getString("id"));
                String id = detArr.getJSONObject(i).getString("id");
                String product_id = detArr.getJSONObject(i).getString("product_id");
                String attribute = detArr.getJSONObject(i).getString("attribute");
                String get_product_note = detArr.getJSONObject(i).getString("note");
                String qty = detArr.getJSONObject(i).getString("qty");
                String price = detArr.getJSONObject(i).getString("price");
                String description = detArr.getJSONObject(i).getString("description");
                String discount_price = detArr.getJSONObject(i).getString("discount_price");
                String product_name = detArr.getJSONObject(i).getString("product_name");

                String product_default_image = detArr.getJSONObject(i).getString("image");
                HashMap<String, String> map_list5 = new HashMap<String, String>();
                map_list5.put(PRODUCT_NAME, product_name);
                map_list5.put(PRODUCT_ID, product_id);
                map_list5.put(DETAILS_ID, id);
                map_list5.put(PRODUCT_PRICE, price);
                map_list5.put(PRODUCT_IMAGE, product_default_image);
                map_list5.put(PRODUCT_DESC, description);
                map_list5.put(PRODUCT_DIC_PRICE, discount_price);
                map_list5.put(QUANTITY, qty);
                map_list5.put(ATTR_NAME, attribute);
                map_list5.put(NOTE, get_product_note);
                orderlist.add(map_list5);
            }
            ShopperDetailsSybAdapter cartSubAdapter = new ShopperDetailsSybAdapter(activity, orderlist);
            recyclerView.setAdapter(cartSubAdapter);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("ERR", e.toString());
        }

    }
}