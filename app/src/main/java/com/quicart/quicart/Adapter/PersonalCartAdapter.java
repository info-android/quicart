package com.quicart.quicart.Adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.quicart.quicart.R;

import java.util.ArrayList;
import java.util.HashMap;

import static com.quicart.quicart.Helper.Constants.PRODUCT_NAME;

public class PersonalCartAdapter extends RecyclerView.Adapter<PersonalCartAdapter.ViewHolder> {
    private Activity activity;
    private ArrayList<HashMap<String,String>> map_list;
    private ArrayList<HashMap<String,String>> cart_sub_map_list;

    public PersonalCartAdapter(Activity activity, ArrayList<HashMap<String, String>> map_list) {
        this.activity = activity;
        this.map_list = map_list;

    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.personal_cart_item, parent, false);

        return new ViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        final HashMap<String,String> map =map_list.get(position);


        get_product_list(holder.rv_cart_item);
        //holder.edt_price.setId(Integer.parseInt((map.get(BUSINESS_SERVICE_ID))));
    }

    @Override
    public int getItemCount() {
        return map_list == null ? 0 : map_list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private RecyclerView rv_cart_item;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            rv_cart_item=(RecyclerView)itemView.findViewById(R.id.rv_cart_item);

        }
    }
    private void get_product_list(RecyclerView recyclerView){
        cart_sub_map_list=new ArrayList<HashMap<String, String>>();
        for (int i = 0; i <3 ; i++) {
            HashMap<String,String> sub_map=new HashMap<String, String>();
            sub_map.put(PRODUCT_NAME,"Rice Oil");
            cart_sub_map_list.add(sub_map);
        }
        CartSubAdapter cartSubAdapter=new CartSubAdapter(activity,cart_sub_map_list);
        recyclerView.setFocusable(false);
        recyclerView.setAdapter(cartSubAdapter);
    }


}
