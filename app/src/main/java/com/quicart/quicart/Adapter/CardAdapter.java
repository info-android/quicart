package com.quicart.quicart.Adapter;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.preference.PreferenceManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.quicart.quicart.Activity.PayStackActivity;
import com.quicart.quicart.R;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;

import java.util.ArrayList;
import java.util.HashMap;

import static com.quicart.quicart.Helper.Constants.DATE;
import static com.quicart.quicart.Helper.Constants.FROM;
import static com.quicart.quicart.Helper.Constants.PRODUCT_DESC;
import static com.quicart.quicart.Helper.Constants.PRODUCT_ID;
import static com.quicart.quicart.Helper.Constants.PRODUCT_NAME;

public class CardAdapter extends RecyclerView.Adapter<CardAdapter.OrderVh> {
    public ArrayList<HashMap<String, String>> list;
    Activity activity;
    String user_id;
    //innitialize listview adapter
    public CardAdapter(Activity activity, ArrayList<HashMap<String, String>> list) {
        super();
        this.activity = activity;
        this.list = list;
    }

    @Override
    public OrderVh onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.card_layout, parent, false);
        return new OrderVh(view);
    }

    //override function for adapter extend to baseadater
    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    @Override
    public void onBindViewHolder(final OrderVh holder, final int position) {
        final HashMap<String, String> map = list.get(position);
        user_id = PreferenceManager.getDefaultSharedPreferences(activity).getString("user_id","");

        holder.prdct_title_tv.setText("Card Number: **** **** **** "+map.get(PRODUCT_NAME));
        holder.exp_tv.setText("Expiry: "+map.get(DATE));
        holder.type_tv.setText("Type: "+map.get(PRODUCT_DESC));

        holder.container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new SweetAlertDialog(activity, SweetAlertDialog.WARNING_TYPE)
                        .setTitleText("Pay Now")
                        .setContentText("Do you want to pay with this card?")
                        .setConfirmText("Yes")
                        .setCancelText("No")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();
                                if(map.get(FROM).equals("sub")){
                                    ((PayStackActivity)activity).makepay3(map.get(PRODUCT_ID));
                                }else{
                                    ((PayStackActivity)activity).makepay2(map.get(PRODUCT_ID));
                                }


                            }
                        }).setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();
                    }
                })
                        .show();
            }
        });

    }


    //create holder according to id of view page
    public static class OrderVh extends RecyclerView.ViewHolder {

        TextView prdct_title_tv;
        TextView exp_tv;
        TextView type_tv;
        RelativeLayout container;
        public OrderVh(View v) {
            super(v);

            prdct_title_tv = (TextView) v.findViewById(R.id.prdct_title_tv);
            exp_tv = (TextView) v.findViewById(R.id.exp_tv);
            type_tv = (TextView) v.findViewById(R.id.type_tv);
            container = (RelativeLayout) v.findViewById(R.id.container);
        }
    }
    Dialog mDialog;
    private void showProgressDialog(){
        mDialog = new Dialog(activity);
        mDialog.setCancelable(false);
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        mDialog.setContentView(R.layout.custom_progress_dialog);
        mDialog.show();
    }
    private void hideProgressDialog(){
        mDialog.dismiss();
    }
}
