package com.quicart.quicart.Adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.preference.PreferenceManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.quicart.quicart.Activity.ProductDetailsActivity;
import com.quicart.quicart.Activity.YourItemsActivity;
import com.quicart.quicart.Helper.Common;
import com.quicart.quicart.Helper.MySingleton;
import com.quicart.quicart.R;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import static com.quicart.quicart.Helper.Constants.DISC_DATE;
import static com.quicart.quicart.Helper.Constants.IS_FAVORITE;
import static com.quicart.quicart.Helper.Constants.PRODUCT_DESC;
import static com.quicart.quicart.Helper.Constants.PRODUCT_DIC_PRICE;
import static com.quicart.quicart.Helper.Constants.PRODUCT_ID;
import static com.quicart.quicart.Helper.Constants.PRODUCT_IMAGE;
import static com.quicart.quicart.Helper.Constants.PRODUCT_IMG_URL;
import static com.quicart.quicart.Helper.Constants.PRODUCT_NAME;
import static com.quicart.quicart.Helper.Constants.PRODUCT_PRICE;

public class ItemAdapter extends RecyclerView.Adapter<ItemAdapter.OrderVh> {
    public ArrayList<HashMap<String, String>> list;
    Activity activity;
    String user_id = "";

    //innitialize listview adapter
    public ItemAdapter(Activity activity, ArrayList<HashMap<String, String>> list) {
        super();
        this.activity = activity;
        this.list = list;
    }

    @Override
    public OrderVh onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.search_layout, parent, false);
        return new OrderVh(view);
    }

    //override function for adapter extend to baseadater
    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    @Override
    public void onBindViewHolder(final OrderVh holder, final int position) {
        final HashMap<String, String> map = list.get(position);
        user_id = PreferenceManager.getDefaultSharedPreferences(activity).getString("user_id", "");
        holder.removeItem_img.setVisibility(View.VISIBLE);
        /*
map.put(PRODUCT_NAME,name);
        map.put(PRODUCT_ID,id);
        map.put(PRODUCT_PRICE,price);
        map.put(PRODUCT_IMAGE,imgOBJ.getString("image"));
        map.put(PRODUCT_DESC,description);
        map.put(PRODUCT_DIC_PRICE,discounted_price);*/

        holder.container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //MethodClass.go_to_next_activity(activity, ProductDetailsActivity.class);
                Intent intent = new Intent(activity, ProductDetailsActivity.class);
                intent.putExtra("id", map.get(PRODUCT_ID));
                activity.startActivity(intent);
            }
        });

        holder.removeItem_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new SweetAlertDialog(activity, SweetAlertDialog.WARNING_TYPE)
                        .setTitleText("Remove Item")
                        .setContentText("Do you want to remove this item?")
                        .setConfirmText("Yes")
                        .setCancelText("No")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();
                                removeItem( map.get(PRODUCT_ID),map.get(IS_FAVORITE));
                            }
                        })
                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();
                            }
                        })
                        .show();
            }
        });

        Picasso.with(activity).load(PRODUCT_IMG_URL + map.get(PRODUCT_IMAGE)).placeholder(R.drawable.ic_spinner_of_dots) //this is optional the image to display while the url image is downloading
                .error(R.drawable.ic_spinner_of_dots)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                .into(holder.prdct_iv);

        holder.prdct_title_tv.setText(map.get(PRODUCT_NAME));
        holder.prdct_dtls_tv.setText(map.get(PRODUCT_DESC));
        holder.price.setText("₦" + map.get(PRODUCT_PRICE));
        if(!map.get(DISC_DATE).equals("null") && !map.get(DISC_DATE).equals(null) && !map.get(DISC_DATE).equals("")){
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd");

            Date strDate = null;
            try {
                String dat = sdf.format(sdf2.parse(map.get(DISC_DATE)));
                strDate = sdf.parse(dat);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            if(!map.get(PRODUCT_DIC_PRICE).equals("null") && !map.get(PRODUCT_DIC_PRICE).equals(null) && strDate.after(new Date())){
                Double off = (Double.parseDouble(map.get(PRODUCT_PRICE))-Double.parseDouble(map.get(PRODUCT_DIC_PRICE)));
                double res = (off / Double.parseDouble(map.get(PRODUCT_PRICE))) * 100;
                holder.discount.setText(String.format("%.2f",res)+"% Off");
                holder.discount.setVisibility(View.VISIBLE);
                holder.price.setText("₦"+map.get(PRODUCT_DIC_PRICE));
            }else{
                holder.discount.setVisibility(View.GONE);
                holder.price.setText("₦"+map.get(PRODUCT_PRICE));
            }
        }else {
            holder.discount.setVisibility(View.GONE);
            holder.price.setText("₦"+map.get(PRODUCT_PRICE));
        }
        holder.add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!user_id.equals("")) {
                    Common.add_to_cart(activity, map.get(PRODUCT_ID), user_id, "1", "true");
                } else {
                    Common.add_to_cart(activity, map.get(PRODUCT_ID), "", "1", "false");
                }

            }
        });
    }


    //create holder according to id of view page
    public static class OrderVh extends RecyclerView.ViewHolder {
        RelativeLayout container;
        ImageView prdct_iv, removeItem_img;
        TextView discount, prdct_title_tv, prdct_dtls_tv, price;
        LinearLayout add;

        public OrderVh(View v) {
            super(v);
            container = (RelativeLayout) itemView.findViewById(R.id.container);
            prdct_iv = (ImageView) itemView.findViewById(R.id.prdct_iv);
            removeItem_img = (ImageView) itemView.findViewById(R.id.removeItem_img);
            discount = (TextView) itemView.findViewById(R.id.discount);
            prdct_title_tv = (TextView) itemView.findViewById(R.id.prdct_title_tv);
            prdct_dtls_tv = (TextView) itemView.findViewById(R.id.prdct_dtls_tv);
            price = (TextView) itemView.findViewById(R.id.price);
            add = itemView.findViewById(R.id.add);
        }
    }

    private void removeItem(String product_id,String is_favorite){
        String server_url="";
        if (is_favorite.equals("Y")){
            server_url = activity.getString(R.string.SERVER_URL)+"remove-to-favourite";
        }else {
            server_url = activity.getString(R.string.SERVER_URL)+"remove-from-items";
        }
        Log.e("server_url",server_url);
        final String token = activity.getString(R.string.AUTH_KEY);
        HashMap<String, String> params1 = new HashMap<String, String>();
        params1.put("secret_key", token);
        params1.put("user_id", user_id);
        params1.put("product_id", product_id);
        Log.e("removeParams", new JSONObject(params1).toString());
        showProgressDialog();
        JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, server_url, new JSONObject(params1), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("ERROR", response.toString());
                try {
                    hideProgressDialog();
                    String status = response.getString("status");
                    if (status.equals("SUCCESS")) {
                        ((YourItemsActivity)activity).getItems();
                        new SweetAlertDialog(activity, SweetAlertDialog.SUCCESS_TYPE)
                                .setTitleText("Remove Item")
                                .setContentText("The item has been successfully removed")
                                .setConfirmText("Okay")
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sDialog) {
                                        sDialog.dismissWithAnimation();
                                    }
                                })
                                .show();
                    }else {
                        new SweetAlertDialog(activity, SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("Network Error")
                                .setContentText("Please check your internet connection.")
                                .setConfirmText("Settings")
                                .setCancelText("Okay")
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sDialog) {
                                        sDialog.dismissWithAnimation();
                                        activity.startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 1);
                                    }
                                })
                                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sDialog) {
                                        sDialog.dismissWithAnimation();
                                    }
                                })
                                .show();
                    }
                } catch (JSONException e) {
                    Log.e("ERROR", e.toString());
                    new SweetAlertDialog(activity, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Network Error")
                            .setContentText("Please check your internet connection.")
                            .setConfirmText("Settings")
                            .setCancelText("Okay")
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();
                                    activity.startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 1);
                                }
                            })
                            .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();
                                }
                            })
                            .show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("ERROR", error.toString());
                new SweetAlertDialog(activity, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Network Error")
                        .setContentText("Please check your internet connection.")
                        .setConfirmText("Settings")
                        .setCancelText("Okay")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();
                                activity.startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 1);
                            }
                        })
                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();
                            }
                        })
                        .show();
            }
        });
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(50 * 1000, 3,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        // Access the RequestQueue through your singleton class.
        MySingleton.getInstance(activity).addToRequestQueue(jsObjRequest);
    }


    Dialog mDialog;
    private void showProgressDialog(){
        mDialog = new Dialog(activity);
        mDialog.setCancelable(false);
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        mDialog.setContentView(R.layout.custom_progress_dialog);
        mDialog.show();
    }
    private void hideProgressDialog(){
        mDialog.dismiss();
    }

}
