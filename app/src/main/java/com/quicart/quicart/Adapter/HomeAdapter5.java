package com.quicart.quicart.Adapter;

import android.app.Activity;
import android.content.Intent;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.quicart.quicart.Activity.ProductDetailsActivity;
import com.quicart.quicart.Helper.Common;
import com.quicart.quicart.R;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import static com.quicart.quicart.Helper.Constants.ATTR_NAME;
import static com.quicart.quicart.Helper.Constants.DISC_DATE;
import static com.quicart.quicart.Helper.Constants.PRODUCT_DESC;
import static com.quicart.quicart.Helper.Constants.PRODUCT_DIC_PRICE;
import static com.quicart.quicart.Helper.Constants.PRODUCT_ID;
import static com.quicart.quicart.Helper.Constants.PRODUCT_IMAGE;
import static com.quicart.quicart.Helper.Constants.PRODUCT_IMG_URL;
import static com.quicart.quicart.Helper.Constants.PRODUCT_NAME;
import static com.quicart.quicart.Helper.Constants.PRODUCT_PRICE;

public class HomeAdapter5 extends RecyclerView.Adapter<HomeAdapter5.ViewHolder> {
    private Activity activity;
    private ArrayList<HashMap<String,String>> map_list;
    private String user_id = "";
    public HomeAdapter5(Activity activity, ArrayList<HashMap<String, String>> map_list) {
        this.activity = activity;
        this.map_list = map_list;
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.home_product_item, parent, false);
        return new ViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        final HashMap<String,String> map =map_list.get(position);

        user_id = PreferenceManager.getDefaultSharedPreferences(activity).getString("user_id","");
        holder.container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //MethodClass.go_to_next_activity(activity, ProductDetailsActivity.class);
                Intent intent=new Intent(activity, ProductDetailsActivity.class);
                intent.putExtra("id",map.get(PRODUCT_ID));
                activity.startActivity(intent);
            }
        });

        Picasso.with(activity)
                .load(PRODUCT_IMG_URL+map.get(PRODUCT_IMAGE))
                .placeholder(R.drawable.ic_spinner_of_dots) //this is optional the image to display while the url image is downloading
                .error(R.drawable.ic_spinner_of_dots)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                .into(holder.prdct_iv);
        holder.prdct_title_tv.setText(map.get(PRODUCT_NAME));
        holder.prdct_dtls_tv.setText(map.get(PRODUCT_DESC));
        if(!map.get(ATTR_NAME).equals("")){
            holder.price.setText("₦"+map.get(PRODUCT_PRICE)+"/"+map.get(ATTR_NAME));
        }else {
            holder.price.setText("₦"+map.get(PRODUCT_PRICE));
        }

        if(!map.get(DISC_DATE).equals("null") && !map.get(DISC_DATE).equals(null)){
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd");

            Date strDate = null;
            try {
                String dat = sdf.format(sdf2.parse(map.get(DISC_DATE)));
                strDate = sdf.parse(dat);
                Log.e("DATE", strDate.toString()+"-------"+new Date().toString() );
            } catch (ParseException e) {
                e.printStackTrace();
            }

            if(!map.get(PRODUCT_DIC_PRICE).equals("null") && !map.get(PRODUCT_DIC_PRICE).equals(null) && strDate.after(new Date())){
                Double off = (Double.parseDouble(map.get(PRODUCT_PRICE))-Double.parseDouble(map.get(PRODUCT_DIC_PRICE)));
                double res = (off / Double.parseDouble(map.get(PRODUCT_PRICE))) * 100;
                holder.discount.setText(String.format("%.2f",res)+"% Off");
                holder.discount.setVisibility(View.VISIBLE);
                holder.price.setText("₦"+map.get(PRODUCT_DIC_PRICE));
            }else{
                holder.discount.setVisibility(View.GONE);
                holder.price.setText("₦"+map.get(PRODUCT_PRICE));
            }
        }else {
            holder.discount.setVisibility(View.GONE);
            holder.price.setText("₦"+map.get(PRODUCT_PRICE));
        }

        holder.add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!user_id.equals("")){
                    Common.add_to_cart(activity,map.get(PRODUCT_ID),user_id,"1","true");
                }else{
                    Common.add_to_cart(activity,map.get(PRODUCT_ID),"","1","false");
                }

            }
        });

        //holder.edt_price.setId(Integer.parseInt((map.get(BUSINESS_SERVICE_ID))));
    }

    @Override
    public int getItemCount() {
        return map_list == null ? 0 : map_list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        RelativeLayout container;
        ImageView prdct_iv;
        TextView discount,prdct_title_tv,prdct_dtls_tv,price;
        LinearLayout add;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            container = (RelativeLayout) itemView.findViewById(R.id.container);
            prdct_iv = (ImageView) itemView.findViewById(R.id.prdct_iv);
            discount = (TextView) itemView.findViewById(R.id.discount);
            prdct_title_tv = (TextView) itemView.findViewById(R.id.prdct_title_tv);
            prdct_dtls_tv = (TextView) itemView.findViewById(R.id.prdct_dtls_tv);
            price = (TextView) itemView.findViewById(R.id.price);
            add = (LinearLayout) itemView.findViewById(R.id.add);
        }
    }


}
