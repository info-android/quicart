package com.quicart.quicart.Adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.quicart.quicart.Activity.SellerPublicProfileActivity;
import com.quicart.quicart.R;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.quicart.quicart.Helper.Constants.ATTR_NAME;
import static com.quicart.quicart.Helper.Constants.DETAILS_ARRAY;
import static com.quicart.quicart.Helper.Constants.DETAILS_ID;
import static com.quicart.quicart.Helper.Constants.NOTE;
import static com.quicart.quicart.Helper.Constants.PRODUCT_DESC;
import static com.quicart.quicart.Helper.Constants.PRODUCT_DIC_PRICE;
import static com.quicart.quicart.Helper.Constants.PRODUCT_ID;
import static com.quicart.quicart.Helper.Constants.PRODUCT_IMAGE;
import static com.quicart.quicart.Helper.Constants.PRODUCT_NAME;
import static com.quicart.quicart.Helper.Constants.PRODUCT_PRICE;
import static com.quicart.quicart.Helper.Constants.PROFILE_IMG_URL;
import static com.quicart.quicart.Helper.Constants.QUANTITY;
import static com.quicart.quicart.Helper.Constants.SELLER_COMPANY;
import static com.quicart.quicart.Helper.Constants.SELLER_ID;
import static com.quicart.quicart.Helper.Constants.SELLER_IMG;
import static com.quicart.quicart.Helper.Constants.SELLER_NAME;

public class CartAdapter extends RecyclerView.Adapter<CartAdapter.ViewHolder> {
    private Activity activity;
    private ArrayList<HashMap<String,String>> map_list;
    private ArrayList<HashMap<String,String>> cart_sub_map_list;

    public CartAdapter(Activity activity, ArrayList<HashMap<String, String>> map_list) {
        this.activity = activity;
        this.map_list = map_list;

    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.cart_item, parent, false);

        return new ViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        final HashMap<String,String> map =map_list.get(position);
        holder.seller.setText("Seller: "+map.get(SELLER_COMPANY));
        holder.seller.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent I = new Intent(activity,SellerPublicProfileActivity.class);
                I.putExtra("store_id",map.get(SELLER_ID));
                activity.startActivity(I);
            }
        });
        get_product_list(holder.rv_cart_item,map.get(DETAILS_ARRAY));
        //holder.edt_price.setId(Integer.parseInt((map.get(BUSINESS_SERVICE_ID))));

        Picasso.with(activity).load(PROFILE_IMG_URL+map.get(SELLER_IMG)).placeholder(R.drawable.ic_seller).error(R.drawable.ic_seller).into(holder.img);
    }

    @Override
    public int getItemCount() {
        return map_list == null ? 0 : map_list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private RecyclerView rv_cart_item;
        TextView seller;
        CircleImageView img;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            rv_cart_item=(RecyclerView)itemView.findViewById(R.id.rv_cart_item);
            seller=(TextView) itemView.findViewById(R.id.seller);
            img=(CircleImageView) itemView.findViewById(R.id.img);

        }
    }
    private void get_product_list(RecyclerView recyclerView ,String detArrs){
        try {
            if (!detArrs.equals(null) && !detArrs.equals("null") && !detArrs.equals("")) {
                JSONArray detArr = new JSONArray(detArrs);
                cart_sub_map_list = new ArrayList<HashMap<String, String>>();
                for (int i = 0; i < detArr.length(); i++) {
                    Log.e("ISD", detArr.getJSONObject(i).getString("id"));
                    String id = detArr.getJSONObject(i).getString("id");
                    String product_id = detArr.getJSONObject(i).getString("product_id");
                    String attribute = detArr.getJSONObject(i).getString("attribute");
                    String qty = detArr.getJSONObject(i).getString("qty");
                    String price = detArr.getJSONObject(i).getString("price");
                    String description = detArr.getJSONObject(i).getString("description");
                    String discount_price = detArr.getJSONObject(i).getString("discount_price");
                    String product_name = detArr.getJSONObject(i).getString("product_name");
                    String note = detArr.getJSONObject(i).getString("note");

                    String product_default_image = detArr.getJSONObject(i).getString("image");
                    HashMap<String, String> map_list5 = new HashMap<String, String>();
                    map_list5.put(PRODUCT_NAME, product_name);
                    map_list5.put(PRODUCT_ID, product_id);
                    map_list5.put(DETAILS_ID, id);
                    map_list5.put(PRODUCT_PRICE, price);
                    map_list5.put(PRODUCT_IMAGE, product_default_image);
                    map_list5.put(PRODUCT_DESC, description);
                    map_list5.put(PRODUCT_DIC_PRICE, discount_price);
                    map_list5.put(NOTE, note);
                    map_list5.put(QUANTITY, qty);
                    map_list5.put(ATTR_NAME, attribute);
                    cart_sub_map_list.add(map_list5);
                }
                CartSubAdapter cartSubAdapter = new CartSubAdapter(activity, cart_sub_map_list);
                recyclerView.setAdapter(cartSubAdapter);
            }
        }catch (Exception e){
            e.printStackTrace();
            Log.e("ERR", e.toString() );
        }

    }


}
