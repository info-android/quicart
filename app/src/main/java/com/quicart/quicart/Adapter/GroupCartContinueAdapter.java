package com.quicart.quicart.Adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.quicart.quicart.Activity.SellerPublicProfileActivity;
import com.quicart.quicart.Helper.MethodClass;
import com.quicart.quicart.R;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.HashMap;

import static com.quicart.quicart.Helper.Constants.ATTR_NAME;
import static com.quicart.quicart.Helper.Constants.CART_ID;
import static com.quicart.quicart.Helper.Constants.DETAILS_ARRAY;
import static com.quicart.quicart.Helper.Constants.DETAILS_ID;
import static com.quicart.quicart.Helper.Constants.GRP_USR;
import static com.quicart.quicart.Helper.Constants.GRP_USR_ID;
import static com.quicart.quicart.Helper.Constants.NOTE;
import static com.quicart.quicart.Helper.Constants.OWNER_ID;
import static com.quicart.quicart.Helper.Constants.PRODUCT_DESC;
import static com.quicart.quicart.Helper.Constants.PRODUCT_DIC_PRICE;
import static com.quicart.quicart.Helper.Constants.PRODUCT_ID;
import static com.quicart.quicart.Helper.Constants.PRODUCT_IMAGE;
import static com.quicart.quicart.Helper.Constants.PRODUCT_NAME;
import static com.quicart.quicart.Helper.Constants.PRODUCT_PRICE;
import static com.quicart.quicart.Helper.Constants.QUANTITY;
import static com.quicart.quicart.Helper.Constants.SELLER_COMPANY;
import static com.quicart.quicart.Helper.Constants.SELLER_ID;
import static com.quicart.quicart.Helper.Constants.SELLER_IMG;
import static com.quicart.quicart.Helper.Constants.SELLER_NAME;

public class GroupCartContinueAdapter extends RecyclerView.Adapter<GroupCartContinueAdapter.ViewHolder> {
    private Activity activity;
    private ArrayList<HashMap<String,String>> map_list;
    private ArrayList<HashMap<String,String>> cart_sub_map_list;
    private String cart_id = "";
    private String owner_id = "";
    public GroupCartContinueAdapter(Activity activity, ArrayList<HashMap<String, String>> map_list) {
        this.activity = activity;
        this.map_list = map_list;

    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.groupcart_countinue_item, parent, false);

        return new ViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        final HashMap<String,String> map =map_list.get(position);

        cart_id = map.get(CART_ID);

        if(!map.get(SELLER_COMPANY).equals("null") && ! map.get(SELLER_COMPANY).equals("")){
            holder.seller.setText("Seller: "+map.get(SELLER_COMPANY));
        }else {
            holder.seller.setText("Seller: "+map.get(SELLER_NAME));
        }

        owner_id = map.get(OWNER_ID);
        holder.seller.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MethodClass.go_to_next_activity(activity, SellerPublicProfileActivity.class);
                Intent I = new Intent(activity,SellerPublicProfileActivity.class);
                I.putExtra("store_id",map.get(SELLER_ID));
                activity.startActivity(I);
            }
        });
        get_product_list(holder.rv_cart_item,map.get(DETAILS_ARRAY));
        //holder.edt_price.setId(Integer.parseInt((map.get(BUSINESS_SERVICE_ID))));
    }

    @Override
    public int getItemCount() {
        return map_list == null ? 0 : map_list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private RecyclerView rv_cart_item;
        TextView seller;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            rv_cart_item=(RecyclerView)itemView.findViewById(R.id.rv_cart_item);
            seller=(TextView)itemView.findViewById(R.id.seller);

        }
    }
    private void get_product_list(RecyclerView recyclerView, String detArrs ){

        try {
            JSONArray detArr = new JSONArray(detArrs);
            cart_sub_map_list=new ArrayList<HashMap<String, String>>();
            for (int i = 0; i <detArr.length() ; i++) {
                Log.e("ISD", detArr.getJSONObject(i).getString("id") );
                String id = detArr.getJSONObject(i).getString("id");
                String product_id = detArr.getJSONObject(i).getString("product_id");
                String attribute = detArr.getJSONObject(i).getString("attribute");
                String user_id = detArr.getJSONObject(i).getString("user_id");
                String qty = detArr.getJSONObject(i).getString("qty");
                String price = detArr.getJSONObject(i).getString("price");
                String description = detArr.getJSONObject(i).getString("description");
                String discount_price = detArr.getJSONObject(i).getString("discount_price");
                String product_name = detArr.getJSONObject(i).getString("product_name");

                String product_default_image = detArr.getJSONObject(i).getString("image");
                String first_name = detArr.getJSONObject(i).getString("first_name");
                String last_name = detArr.getJSONObject(i).getString("last_name");
                String note = detArr.getJSONObject(i).getString("note");
                String customer_image = detArr.getJSONObject(i).getString("customer_image");
                HashMap<String,String> map_list5=new HashMap<String, String>();
                map_list5.put(PRODUCT_NAME,product_name);
                map_list5.put(PRODUCT_ID,product_id);
                map_list5.put(DETAILS_ID,id);
                map_list5.put(PRODUCT_PRICE,price);
                map_list5.put(PRODUCT_IMAGE,product_default_image);
                map_list5.put(PRODUCT_DESC,description);
                map_list5.put(PRODUCT_DIC_PRICE,discount_price);
                map_list5.put(QUANTITY,qty);
                map_list5.put(NOTE,note);
                map_list5.put(SELLER_IMG,customer_image);
                map_list5.put(GRP_USR,first_name+" "+last_name);
                map_list5.put(GRP_USR_ID,user_id);
                map_list5.put(CART_ID, cart_id);
                map_list5.put(ATTR_NAME, attribute);
                map_list5.put(OWNER_ID, owner_id);
                cart_sub_map_list.add(map_list5);
            }
            GroupCartCounSubAdapter cartSubAdapter=new GroupCartCounSubAdapter(activity,cart_sub_map_list);
            recyclerView.setAdapter(cartSubAdapter);
        }catch (Exception e){
            e.printStackTrace();
            Log.e("ERR", e.toString() );
        }
    }


}
