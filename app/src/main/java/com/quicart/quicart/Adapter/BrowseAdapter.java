package com.quicart.quicart.Adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.quicart.quicart.Activity.SearchActivity;
import com.quicart.quicart.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;

import static com.quicart.quicart.Helper.Constants.CATEGORY_IMG_URL;
import static com.quicart.quicart.Helper.Constants.PRODUCT_ID;
import static com.quicart.quicart.Helper.Constants.PRODUCT_IMAGE;
import static com.quicart.quicart.Helper.Constants.PRODUCT_NAME;

public class BrowseAdapter extends RecyclerView.Adapter<BrowseAdapter.ViewHolder> {
    private Activity activity;
    private ArrayList<HashMap<String,String>> map_list;

    public BrowseAdapter(Activity activity, ArrayList<HashMap<String, String>> map_list) {
        this.activity = activity;
        this.map_list = map_list;

    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.browse_item, parent, false);
        return new ViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        final HashMap<String,String> map =map_list.get(position);

        //holder.edt_price.setId(Integer.parseInt((map.get(BUSINESS_SERVICE_ID))));
        holder.container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //MethodClass.go_to_next_activity(activity, ProductDetailsActivity.class);
                Intent intent=new Intent(activity, SearchActivity.class);
                intent.putExtra("cat_id",map.get(PRODUCT_ID));
                intent.putExtra("cat_name",map.get(PRODUCT_NAME));
                activity.startActivity(intent);
            }
        });
        holder.nameTV.setText(map.get(PRODUCT_NAME));
        Picasso.with(activity)
                .load(CATEGORY_IMG_URL+map.get(PRODUCT_IMAGE))
                .placeholder(R.drawable.ic_spinner_of_dots) //this is optional the image to display while the url image is downloading
                .error(R.drawable.ic_spinner_of_dots)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                .into(holder.imageV);
    }

    @Override
    public int getItemCount() {
        return map_list == null ? 0 : map_list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        CardView container;
        ImageView imageV;
        TextView nameTV;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            container = (CardView) itemView.findViewById(R.id.container);
            imageV = (ImageView) itemView.findViewById(R.id.img);
            nameTV = (TextView) itemView.findViewById(R.id.name);

        }
    }


}
