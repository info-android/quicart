package com.quicart.quicart.Activity;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.quicart.quicart.Activity.Shopper.MyOrderActivity;
import com.quicart.quicart.Helper.MethodClass;
import com.quicart.quicart.Helper.MySingleton;
import com.quicart.quicart.R;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.quicart.quicart.Helper.Constants.DEVICE_ID;
import static com.quicart.quicart.Helper.Constants.SHARED_PREF;
import static com.quicart.quicart.Helper.Constants.USER_LANG;
import static com.quicart.quicart.Helper.Constants.USER_LAT;


public class LoginActivity extends AppCompatActivity {
    private Button login;
    private TextView tv_sign_up;
    private TextView sign_up;
    private EditText et_email;
    private EditText et_password;
    private String address,city,state,country,postalCode;
    private String from = "";
    private String  cart_id = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);
        login=(Button) findViewById(R.id.login);
        tv_sign_up=(TextView) findViewById(R.id.tv_sign_up);
        sign_up=(TextView) findViewById(R.id.sign_up);
        et_email=(EditText) findViewById(R.id.et_email);
        et_password=(EditText) findViewById(R.id.et_password);

        if(getIntent().getExtras() !=null){
            from = getIntent().getStringExtra("from");
            cart_id = getIntent().getStringExtra("cart_id");
        }
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if(et_email.getText().toString().length() == 0){
//                    et_email.setError("Enter Seller for seller section/Customer for customer section");
//                    et_email.requestFocus();
//                }
//                if(et_email.getText().toString().trim().equals("Shopper")){
//                    MethodClass.go_to_next_activity(LoginActivity.this, MyOrderActivity.class);
//                }if(et_email.getText().toString().trim().equals("Customer")){
//                    MethodClass.go_to_next_activity(LoginActivity.this, HomeActivity.class);
//                }
                login();

            }
        });
        tv_sign_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MethodClass.go_to_next_activity(LoginActivity.this,ForgotPasswordActivity.class);
            }
        });
        sign_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent I = new Intent(LoginActivity.this, SignUpActivity.class);
                I.putExtra("from","cart");
                startActivity(I);
            }
        });

    }

    public void back(View view){
        super.onBackPressed();
    }
    public void login(){
        if(!emailValidator(et_email.getText().toString().trim())){
            et_email.setError("Enter your valid email");
            et_email.requestFocus();
            return;
        }
        if(et_password.getText().toString().trim().length() == 0){
            et_password.setError("Enter your password");
            et_password.requestFocus();
            return;
        }
        SharedPreferences pref = LoginActivity.this.getSharedPreferences(SHARED_PREF, 0);
        String regId = pref.getString("regId", null);
        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(this, Locale.getDefault());

        try {
            Log.e("address", String.valueOf(USER_LAT)+" "+USER_LANG);
            addresses = geocoder.getFromLocation(USER_LAT, USER_LANG, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            Log.e("ADDRESS", addresses.toString() );
            address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            city = addresses.get(0).getLocality();
            state = addresses.get(0).getAdminArea();
            country = addresses.get(0).getCountryName();
            postalCode = addresses.get(0).getPostalCode();
        } catch (Exception e) {
            e.printStackTrace();
        }


        String server_url = getString(R.string.SERVER_URL)+"customer-login";
        final String token = getString(R.string.AUTH_KEY);
        HashMap<String, String> params1 = new HashMap<String, String>();
        params1.put("secret_key", token);
        params1.put("email", et_email.getText().toString().trim());
        params1.put("password", et_password.getText().toString().trim());
        params1.put("reg_no", regId);
        params1.put("lat", String.valueOf(USER_LAT));
        params1.put("lng", String.valueOf(USER_LANG));
        params1.put("address", address);
        params1.put("device_id", DEVICE_ID);
        params1.put("reg_no", regId);
        if(getIntent().getExtras() !=null){
            params1.put("invitation_code", getIntent().getStringExtra("referral_id"));
        }
        params1.put("reg_no", regId);

        Log.e("Send", new JSONObject(params1).toString() );
        showProgressDialog();
        JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, server_url, new JSONObject(params1), new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.e("ERROR", response.toString());
                //Toast.makeText(BussinessSignupActivity.this, response.toString(), Toast.LENGTH_LONG).show();
                try {
                    hideProgressDialog();
                    String status = response.getString("status");
                    //Toast.makeText(BussinessSignupActivity.this, status, Toast.LENGTH_LONG).show();
                    if (status.equals("SUCCESS")) {
                        String user_data = response.getString("data");

                        JSONObject userObj = new JSONObject(user_data);
                        String user_id = userObj.getString("id");
                        String fname = userObj.getString("first_name");
                        String lname = userObj.getString("last_name");
                        String email = userObj.getString("email");
                        String user_type = userObj.getString("user_type");
                        String cart = userObj.getString("cart");
                        String image = userObj.getString("image");
                        String notification = userObj.getString("notification");
                        String radius = userObj.getString("radius");
                        String wallet_bal = userObj.getString("total_earning");
                        PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit().putString("new_request_count",notification).commit();

                        PreferenceManager.getDefaultSharedPreferences(LoginActivity.this).edit().putString("user_id",user_id).commit();
                        PreferenceManager.getDefaultSharedPreferences(LoginActivity.this).edit().putString("fname",fname).commit();

                        if(!lname.equals("null") && !lname.equals(null)){
                            PreferenceManager.getDefaultSharedPreferences(LoginActivity.this).edit().putString("lname",lname).commit();
                        }


                        PreferenceManager.getDefaultSharedPreferences(LoginActivity.this).edit().putString("email",email).commit();
                        PreferenceManager.getDefaultSharedPreferences(LoginActivity.this).edit().putString("cart",cart).commit();
                        PreferenceManager.getDefaultSharedPreferences(LoginActivity.this).edit().putString("profile_image",image).commit();
                        PreferenceManager.getDefaultSharedPreferences(LoginActivity.this).edit().putString("user_type",user_type).commit();
                        PreferenceManager.getDefaultSharedPreferences(LoginActivity.this).edit().putBoolean("is_logged_in",true).commit();
                        PreferenceManager.getDefaultSharedPreferences(LoginActivity.this).edit().putString("wallet_bal",wallet_bal).commit();

                        if(radius.equals(null) || radius.equals("null")){
                            PreferenceManager.getDefaultSharedPreferences(LoginActivity.this).edit().putString("radius","5").commit();
                        }else{
                            PreferenceManager.getDefaultSharedPreferences(LoginActivity.this).edit().putString("radius",radius).commit();
                        }

                        if(from.equals("")){
                            if(user_type.equals("C")){
                                Intent i = new Intent(LoginActivity.this,HomeActivity.class);
                                i.setFlags(i.FLAG_ACTIVITY_CLEAR_TASK|i.FLAG_ACTIVITY_CLEAR_TOP);
                                finish();
                                startActivity(i);
                            }else if(user_type.equals("D")){
                                String name = userObj.getString("name");
                                String is_available = userObj.getString("is_available");

                                PreferenceManager.getDefaultSharedPreferences(LoginActivity.this).edit().putString("fname",name).commit();
                                PreferenceManager.getDefaultSharedPreferences(LoginActivity.this).edit().putString("is_available",is_available).commit();
                                Intent i = new Intent(LoginActivity.this,MyOrderActivity.class);
                                i.putExtra("is_available",is_available);
                                i.setFlags(i.FLAG_ACTIVITY_CLEAR_TASK|i.FLAG_ACTIVITY_CLEAR_TOP);
                                finish();
                                startActivity(i);
                            }
                        }else{
                            if(user_type.equals("C")){
                                Intent i = new Intent(LoginActivity.this,CheckoutActivity.class);
                                i.putExtra("from", "personal");
                                i.putExtra("cart_id",userObj.getString("cart_id"));
                                i.setFlags(i.FLAG_ACTIVITY_CLEAR_TASK|i.FLAG_ACTIVITY_CLEAR_TOP);
                                finish();
                                startActivity(i);
                            }
                        }




                    }else if (status.equals("EMAIL_EXIST")) {
                        String message = "Your entered email is already exist. Please try with another email or login ";
                        new SweetAlertDialog(LoginActivity.this, SweetAlertDialog.WARNING_TYPE)
                                .setTitleText("Email Exist")
                                .setContentText(message)
                                .setConfirmText("Ok")
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                                        sweetAlertDialog.dismissWithAnimation();
                                    }
                                })
                                .show();

                    }else if (status.equals("INVITATION_OVER")) {
                        String message = "Your partner invitation limit is over. You are not added to his group cart";
                        new SweetAlertDialog(LoginActivity.this, SweetAlertDialog.WARNING_TYPE)
                                .setTitleText("Invitation Error")
                                .setContentText(message)
                                .setConfirmText("Ok")
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                                        sweetAlertDialog.dismissWithAnimation();
                                    }
                                })
                                .show();

                    }else if (status.equals("MOBILE_EXIST")) {
                        String message = "Your entered mobile number is already exist. Please try with another mobile number ";
                        new SweetAlertDialog(LoginActivity.this, SweetAlertDialog.WARNING_TYPE)
                                .setTitleText("Mobile Number Exist")
                                .setContentText(message)
                                .setConfirmText("Ok")
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                                        sweetAlertDialog.dismissWithAnimation();
                                    }
                                })
                                .show();

                    }else if (status.equals("INVALID_USERNAME_PASSWORD")) {
                        String message = "Your entered login credential is incorrect. Please try with valid login credential";
                        new SweetAlertDialog(LoginActivity.this, SweetAlertDialog.WARNING_TYPE)
                                .setTitleText("Invalid email or password")
                                .setContentText(message)
                                .setConfirmText("Ok")
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                                        sweetAlertDialog.dismissWithAnimation();
                                    }
                                })
                                .show();

                    }else if (status.equals("EMAIL_NOT_VERIFIED")) {
                        String message = "Your account is unverified please verify now.";
                        new SweetAlertDialog(LoginActivity.this, SweetAlertDialog.WARNING_TYPE)
                                .setTitleText("Account Unverified")
                                .setContentText(message)
                                .setConfirmText("Ok")
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                                        sweetAlertDialog.dismissWithAnimation();
                                        Intent I = new Intent(LoginActivity.this,VerificationActivity.class);
                                        I.putExtra("email_id",et_email.getText().toString().trim());
                                        startActivity(I);
                                    }
                                })
                                .show();

                    }else if(status.equals("ACCOUNT_INACTIVE")){
                        new SweetAlertDialog(LoginActivity.this, SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("Inactive Account")
                                .setContentText("Your account is currently disabled. Please contact our admin for enabling")
                                .setConfirmText("Okay")
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sDialog) {
                                        sDialog.dismissWithAnimation();
                                    }
                                })
                                .show();
                    }else if(status.equals("AWATING_APPROVAL")){
                        new SweetAlertDialog(LoginActivity.this, SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("Inactive Account")
                                .setContentText("Your account is currently disabled. Please contact our admin for enabling")
                                .setConfirmText("Okay")
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sDialog) {
                                        sDialog.dismissWithAnimation();
                                    }
                                })
                                .show();
                    }else {
                        new SweetAlertDialog(LoginActivity.this, SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("Network Error")
                                .setContentText("Please check your internet connection.")
                                .setConfirmText("Settings")
                                .setCancelText("Okay")
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sDialog) {
                                        sDialog.dismissWithAnimation();
                                        startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 1);
                                    }
                                })
                                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sDialog) {
                                        sDialog.dismissWithAnimation();
                                    }
                                })
                                .show();
                    }
                } catch (JSONException e) {
                    Log.e("ERROR", e.toString());
                    hideProgressDialog();
                    new SweetAlertDialog(LoginActivity.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Network Error")
                            .setContentText("Please check your internet connection.")
                            .setConfirmText("Settings")
                            .setCancelText("Okay")
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();
                                    startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 1);
                                }
                            })
                            .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();
                                }
                            })
                            .show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("ERROR", error.toString());
                hideProgressDialog();
                new SweetAlertDialog(LoginActivity.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Network Error")
                        .setContentText("Please check your internet connection.")
                        .setConfirmText("Settings")
                        .setCancelText("Okay")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();
                                startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 1);
                            }
                        })
                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();
                            }
                        })
                        .show();
            }
        });
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(50 * 1000, 3,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        // Access the RequestQueue through your singleton class.
        MySingleton.getInstance(this).addToRequestQueue(jsObjRequest);

    }
    Dialog mDialog;
    private void showProgressDialog(){
        mDialog = new Dialog(this);
        mDialog.setCancelable(false);
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        mDialog.setContentView(R.layout.custom_progress_dialog);
        mDialog.show();
    }
    private void hideProgressDialog(){
        mDialog.dismiss();
    }
    public boolean emailValidator(String email) {
        Pattern pattern;
        Matcher matcher;
        final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);
        return matcher.matches();
    }




}
