package com.quicart.quicart.Activity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.quicart.quicart.Adapter.CardAdapter2;
import com.quicart.quicart.Helper.MySingleton;
import com.quicart.quicart.R;


import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import static com.quicart.quicart.Helper.Constants.DATE;
import static com.quicart.quicart.Helper.Constants.PRODUCT_DESC;
import static com.quicart.quicart.Helper.Constants.PRODUCT_ID;
import static com.quicart.quicart.Helper.Constants.PRODUCT_NAME;

public class PaymentMethodsActivity extends AppCompatActivity {
    private ImageView back_btn;
    private RecyclerView rv_Mygroup;
    private ArrayList<HashMap<String, String>> orderlist;

    private String user_id = "";
    private LinearLayout chose;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_payment_methods);
        back_btn=(ImageView)findViewById(R.id.back_btn);
        rv_Mygroup=(RecyclerView) findViewById(R.id.rv_Mygroup);
        chose=(LinearLayout) findViewById(R.id.chose);

        user_id = PreferenceManager.getDefaultSharedPreferences(PaymentMethodsActivity.this).getString("user_id","");
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                finish();
            }
        });
        getCard();

    }
    public void getCard(){
        String server_url = getString(R.string.SERVER_URL) + "card-details";
        final String token = getString(R.string.AUTH_KEY);
        HashMap<String, String> params1 = new HashMap<String, String>();
        params1.put("secret_key", token);
        params1.put("id", user_id);
        //Log.e("avv", new JSONObject(params1).toString());
        showProgressDialog();
        JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, server_url, new JSONObject(params1), new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.e("ERROR", response.toString());
                //Toast.makeText(BussinessSignupActivity.this, response.toString(), Toast.LENGTH_LONG).show();
                try {
                    hideProgressDialog();
                    String status = response.getString("status");
                    //Toast.makeText(BussinessSignupActivity.this, status, Toast.LENGTH_LONG).show();
                    if (status.equals("SUCCESS")) {
                        String data = response.getString("data");

                        JSONArray datObj = new JSONArray(data);
                        if(datObj.length()>0){
                            rv_Mygroup.setVisibility(View.VISIBLE);
                            chose.setVisibility(View.GONE);
                            orderlist=new ArrayList<HashMap<String, String>>();
                            for (int i = 0; i <datObj.length() ; i++) {
                                String id = datObj.getJSONObject(i).getString("id");
                                String last4 = datObj.getJSONObject(i).getString("last4");
                                String exp_month = datObj.getJSONObject(i).getString("exp_month");
                                String exp_year = datObj.getJSONObject(i).getString("exp_year");
                                String brand = datObj.getJSONObject(i).getString("brand");


                                HashMap<String,String> map=new HashMap<String, String>();
                                map.put(PRODUCT_NAME,last4);
                                map.put(PRODUCT_ID,id);
                                map.put(DATE,exp_month+"/"+exp_year);
                                map.put(PRODUCT_DESC,brand);
                                orderlist.add(map);
                            }
                            CardAdapter2 adapter = new CardAdapter2(PaymentMethodsActivity.this, orderlist);
                            rv_Mygroup.setAdapter(adapter);

                        }else{
                            rv_Mygroup.setVisibility(View.GONE);
                            chose.setVisibility(View.VISIBLE);
                        }
                    } else {
                        rv_Mygroup.setVisibility(View.GONE);
                        chose.setVisibility(View.VISIBLE);
                        new SweetAlertDialog(PaymentMethodsActivity.this, SweetAlertDialog.ERROR_TYPE).setTitleText("Network Error").setContentText("Please check your internet connection.").setConfirmText("Settings").setCancelText("Okay").setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();
                                startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 1);
                            }
                        }).setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();
                            }
                        }).show();
                    }
                } catch (JSONException e) {
                    rv_Mygroup.setVisibility(View.GONE);
                    chose.setVisibility(View.VISIBLE);
                    Log.e("ERROR", e.toString());
                    hideProgressDialog();
                    new SweetAlertDialog(PaymentMethodsActivity.this, SweetAlertDialog.ERROR_TYPE).setTitleText("Network Error").setContentText("Please check your internet connection.").setConfirmText("Settings").setCancelText("Okay").setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sDialog) {
                            sDialog.dismissWithAnimation();
                            startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 1);
                        }
                    }).setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sDialog) {
                            sDialog.dismissWithAnimation();
                        }
                    }).show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                rv_Mygroup.setVisibility(View.GONE);
                chose.setVisibility(View.VISIBLE);
                Log.e("ERROR", error.toString());
                hideProgressDialog();
                new SweetAlertDialog(PaymentMethodsActivity.this, SweetAlertDialog.ERROR_TYPE).setTitleText("Network Error").setContentText("Please check your internet connection.").setConfirmText("Settings").setCancelText("Okay").setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();
                        startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 1);
                    }
                }).setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();
                    }
                }).show();
            }
        });
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(50 * 1000, 3, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        // Access the RequestQueue through your singleton class.
        MySingleton.getInstance(PaymentMethodsActivity.this).addToRequestQueue(jsObjRequest);
    }
    Dialog mDialog;

    private void showProgressDialog() {
        mDialog = new Dialog(this);
        mDialog.setCancelable(false);
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        mDialog.setContentView(R.layout.custom_progress_dialog);
        mDialog.show();
    }
    private void hideProgressDialog() {
        mDialog.dismiss();
    }
}
