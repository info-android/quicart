package com.quicart.quicart.Activity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.quicart.quicart.Helper.MethodClass;
import com.quicart.quicart.Helper.MySingleton;
import com.quicart.quicart.R;




import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.quicart.quicart.Helper.Constants.DEVICE_ID;

public class ForgotPasswordActivity extends AppCompatActivity {
    private TextView toolbar_title;
    private Button sbmt_btn;
    private EditText et_email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_forgot_password);
        MethodClass.hide_keyboard(ForgotPasswordActivity.this);
        toolbar_title = (TextView) findViewById(R.id.toolbar_title);
        sbmt_btn = (Button) findViewById(R.id.sbmt_btn);
        et_email = (EditText) findViewById(R.id.et_email);
        toolbar_title.setText("Forgot Password");

        sbmt_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!emailValidator(et_email.getText().toString().trim())){
                    et_email.setError("Please enter valid email");
                    et_email.requestFocus();
                    return;
                }
                forgotPassword();
            }
        });
    }

    public void back(View view) {
        super.onBackPressed();
    }

    public boolean emailValidator(String email) {
        Pattern pattern;
        Matcher matcher;
        final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);
        return matcher.matches();
    }
    public void forgotPassword(){
        String server_url = getString(R.string.SERVER_URL)+"forgot-password";
        String token = getString(R.string.AUTH_KEY);
        HashMap<String, String> params1 = new HashMap<String, String>();
        params1.put("secret_key", token);
        params1.put("email", et_email.getText().toString().trim());
        params1.put("device_id", DEVICE_ID);
        Log.e("Param", new JSONObject(params1).toString() );
        showProgressDialog();
        JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, server_url, new JSONObject(params1), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("ERROR", response.toString());
                try {
                    hideProgressDialog();
                    String status = response.getString("status");
                    if (status.equals("SUCCESS")) {
                        final String data = response.getString("data");
                        String message = "We’ve sent a verification code to your email. Please use it to reset your password.";
                        new SweetAlertDialog(ForgotPasswordActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                                .setTitleText("Successful")
                                .setContentText(message)
                                .setConfirmText("Ok")
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                                        sweetAlertDialog.dismissWithAnimation();
                                        Intent I = new Intent(ForgotPasswordActivity.this,Verification2Activity.class);
                                        I.putExtra("email_id",et_email.getText().toString().trim());
                                        I.putExtra("vcode",data);
                                        startActivity(I);
                                    }
                                })
                                .show();
                    }else if (status.equals("INVALID_EMAIL")) {
                        String message = "The email you entered has been not registered with us. Please use registered email.";
                        new SweetAlertDialog(ForgotPasswordActivity.this, SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("Not Registered")
                                .setContentText(message)
                                .setConfirmText("Ok")
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                                        sweetAlertDialog.dismissWithAnimation();
                                    }
                                })
                                .show();
                    }else {
                        new SweetAlertDialog(ForgotPasswordActivity.this, SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("UNAUTHORIZED_ACCESS ")
                                .setContentText("Wrong secret key provided")
                                .setConfirmText("Settings")
                                .setCancelText("Okay")
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sDialog) {
                                        sDialog.dismissWithAnimation();
                                        startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 1);
                                    }
                                })
                                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sDialog) {
                                        sDialog.dismissWithAnimation();
                                    }
                                })
                                .show();
                    }
                } catch (JSONException e) {
                    hideProgressDialog();
                    new SweetAlertDialog(ForgotPasswordActivity.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Network Error")
                            .setContentText(e.toString())
                            .setConfirmText("Settings")
                            .setCancelText("Okay")
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();
                                    startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 1);
                                }
                            })
                            .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();
                                }
                            })
                            .show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                hideProgressDialog();
                new SweetAlertDialog(ForgotPasswordActivity.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Network Error")
                        .setContentText(error.toString())
                        .setConfirmText("Settings")
                        .setCancelText("Okay")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();
                                startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 1);
                            }
                        })
                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();
                            }
                        })
                        .show();
            }
        });
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(50 * 1000, 3,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        // Access the RequestQueue through your singleton class.
        MySingleton.getInstance(this).addToRequestQueue(jsObjRequest);
    }

    Dialog mDialog;
    private void showProgressDialog(){
        mDialog = new Dialog(this);
        mDialog.setCancelable(false);
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        mDialog.setContentView(R.layout.custom_progress_dialog);
        mDialog.show();
    }
    private void hideProgressDialog(){
        mDialog.dismiss();
    }
}
