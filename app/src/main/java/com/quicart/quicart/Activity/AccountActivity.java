package com.quicart.quicart.Activity;

import android.content.Intent;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.quicart.quicart.Helper.MethodClass;
import com.quicart.quicart.R;


import java.util.Map;




public class AccountActivity extends AppCompatActivity {
    private ImageView back_btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_account);
        back_btn=(ImageView)findViewById(R.id.back_btn);
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                finish();
            }
        });
    }

    public void my_account(View view){
        MethodClass.go_to_next_activity(this, CustomerEditProfileActivity.class);
    }
    public void address(View view){
        MethodClass.go_to_next_activity(this, AddressBookActivity.class);
    }

    public void paymnt_method(View view){
        MethodClass.go_to_next_activity(this, PaymentMethodsActivity.class);
    }
    public void subscrip(View view){
        MethodClass.go_to_next_activity(this, SubscriptionActivity.class);
//        new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
//                .setTitleText("Coming Soon")
//                .setContentText("This feature is not available right now")
//                .setConfirmText("Ok")
//                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
//                    @Override
//                    public void onClick(SweetAlertDialog sDialog) {
//                        sDialog.dismissWithAnimation();
//                    }
//                })
//                .show();
    }
    public void noti(View view){
        MethodClass.go_to_next_activity(this, NotificationSettingsActivity.class);
    }
    public void radius(View view){
        MethodClass.go_to_next_activity(this, RadiusSetActivity.class);
    }
    public void logout(View view){
        Intent intent=new Intent(AccountActivity.this,SplashActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
        Map<String,?> prefs = PreferenceManager.getDefaultSharedPreferences(this).getAll();
        for(Map.Entry<String,?> prefToReset : prefs.entrySet()){

            if(!prefToReset.getKey().equals("first_time")){
                PreferenceManager.getDefaultSharedPreferences(this).edit().remove(prefToReset.getKey()).commit();
            }
        }
        startActivity(intent);
        startActivity(intent);
        finish();
    }
}
