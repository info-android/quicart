package com.quicart.quicart.Activity.Shopper;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.quicart.quicart.R;


public class ShopperProfileShowActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_shopper_profile_show);
    }
    public void back(View view){
        super.onBackPressed();
    }
    public void edit(View view) {
        Intent I = new Intent(ShopperProfileShowActivity.this,EditProfileActivity.class);
        //I.putExtra("edit_profile","edit");
        startActivity(I);
    }
}
