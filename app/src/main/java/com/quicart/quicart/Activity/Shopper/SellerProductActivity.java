package com.quicart.quicart.Activity.Shopper;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.quicart.quicart.Adapter.ProductAdapter;
import com.quicart.quicart.Helper.MySingleton;
import com.quicart.quicart.R;

import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;



import static com.quicart.quicart.Helper.Constants.ATTR_NAME;
import static com.quicart.quicart.Helper.Constants.DISC_DATE;
import static com.quicart.quicart.Helper.Constants.ORD_MASTER_ID;
import static com.quicart.quicart.Helper.Constants.PRODUCT_DESC;
import static com.quicart.quicart.Helper.Constants.PRODUCT_DIC_PRICE;
import static com.quicart.quicart.Helper.Constants.PRODUCT_ID;
import static com.quicart.quicart.Helper.Constants.PRODUCT_IMAGE;
import static com.quicart.quicart.Helper.Constants.PRODUCT_NAME;
import static com.quicart.quicart.Helper.Constants.PRODUCT_PRICE;

public class SellerProductActivity extends AppCompatActivity {
    private RecyclerView listviewsearch;
    private ArrayList<HashMap<String, String>> orderlist;
    private LinearLayout noContent;
    private RelativeLayout cartLayout;
    //private NavigationView navigationView;
    Boolean is_logged_in;
    /* private TextView nav_name_tv,email_head;
     private ImageView imgPro;
     private View headerView;*/
    private String seller_id = "", order_id="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_seller_product);
        listviewsearch = (RecyclerView) findViewById(R.id.listviewsearch);
        is_logged_in = PreferenceManager.getDefaultSharedPreferences(SellerProductActivity.this).getBoolean("is_logged_in",false);
      /*  navigationView = (NavigationView) findViewById(R.id.nav_view);
        headerView = navigationView.getHeaderView(0);
        nav_name_tv=(TextView) headerView.findViewById(R.id.nav_name_tv);
        email_head=(TextView) headerView.findViewById(R.id.email);
        imgPro=(ImageView) headerView.findViewById(R.id.imgPro);
        updatepro=(LinearLayout) headerView.findViewById(R.id.updatepro);*/
        seller_id = getIntent().getStringExtra("store_id");
        order_id = getIntent().getStringExtra("order_id");
        getOrder();
        noContent=(LinearLayout) findViewById(R.id.noContent);
    }
    public void getOrder(){
        String server_url = getString(R.string.SERVER_URL)+"seller-product-list";
        final String token = getString(R.string.AUTH_KEY);
        HashMap<String, String> params1 = new HashMap<String, String>();
        params1.put("secret_key", token);
        params1.put("seller_id", seller_id);
        Log.e("avv", new JSONObject(params1).toString());
        showProgressDialog();
        JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, server_url, new JSONObject(params1), new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.e("ERROR", response.toString());
                //Toast.makeText(BussinessSignupActivity.this, response.toString(), Toast.LENGTH_LONG).show();
                try {
                    hideProgressDialog();
                    String status = response.getString("status");
                    //Toast.makeText(BussinessSignupActivity.this, status, Toast.LENGTH_LONG).show();
                    if (status.equals("SUCCESS")) {

                        String user_data = response.getString("data");
                        JSONObject datObj = new JSONObject(user_data);
                        String premium_product = datObj.getString("products");
                        if(!premium_product.equals("null") && !premium_product.equals(null)){
                            JSONArray prmARR = new JSONArray(premium_product);
                            if(prmARR.length()>0){
                                orderlist=new ArrayList<HashMap<String, String>>();
                                for (int i = 0; i <prmARR.length() ; i++) {

                                    String product_name = prmARR.getJSONObject(i).getString("product_name");
                                    JSONObject pprod = new JSONObject(product_name);
                                    String product_default_image = pprod.getString("product_default_image");
                                    String name = pprod.getString("name");
                                    String description = pprod.getString("description");
                                    String id = prmARR.getJSONObject(i).getString("id");
                                    String price = prmARR.getJSONObject(i).getString("price");
                                    String discounted_price = prmARR.getJSONObject(i).getString("discounted_price");
                                    String disc_to_date = prmARR.getJSONObject(i).getString("disc_to_date");
                                    JSONObject imgOBJ = new JSONObject(product_default_image);

                                    HashMap<String,String> map=new HashMap<String, String>();
                                    map.put(PRODUCT_NAME,name);
                                    map.put(PRODUCT_ID,id);
                                    map.put(PRODUCT_PRICE,price);
                                    map.put(PRODUCT_IMAGE,imgOBJ.getString("image"));
                                    map.put(PRODUCT_DESC,description);
                                    map.put(PRODUCT_DIC_PRICE,discounted_price);
                                    map.put(ORD_MASTER_ID,order_id);
                                    map.put(DISC_DATE,disc_to_date);
                                    String attribute_value_id = prmARR.getJSONObject(i).getString("attribute_value_id");
                                    if(!attribute_value_id.equals("null") && !attribute_value_id.equals(null)  && !prmARR.getJSONObject(i).getString("attribute_name").equals("null")){
                                        JSONObject attOBJ = new JSONObject(prmARR.getJSONObject(i).getString("attribute_name"));
                                        String attrName = attOBJ.getString("name");
                                        map.put(ATTR_NAME,attrName);
                                    }else {
                                        map.put(ATTR_NAME,"");
                                    }
                                    orderlist.add(map);
                                }
                                listviewsearch.setLayoutManager(new GridLayoutManager(SellerProductActivity.this,2));
                                ProductAdapter adapter = new ProductAdapter(SellerProductActivity.this, orderlist);
                                listviewsearch.setAdapter(adapter);
                                listviewsearch.setVisibility(View.VISIBLE);
                                noContent.setVisibility(View.GONE);
                            }else {
                                listviewsearch.setVisibility(View.GONE);
                                noContent.setVisibility(View.VISIBLE);
                            }
                        }else {
                            listviewsearch.setVisibility(View.GONE);
                            noContent.setVisibility(View.VISIBLE);
                        }



                    }else {
                        listviewsearch.setVisibility(View.GONE);
                        noContent.setVisibility(View.VISIBLE);
                        new SweetAlertDialog(SellerProductActivity.this, SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("Network Error")
                                .setContentText("Please check your internet connection.")
                                .setConfirmText("Settings")
                                .setCancelText("Okay")
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sDialog) {
                                        sDialog.dismissWithAnimation();
                                        startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 1);
                                    }
                                })
                                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sDialog) {
                                        sDialog.dismissWithAnimation();
                                    }
                                })
                                .show();
                    }
                } catch (JSONException e) {
                    listviewsearch.setVisibility(View.GONE);
                    noContent.setVisibility(View.VISIBLE);
                    Log.e("ERROR", e.toString());
                    hideProgressDialog();
                    new SweetAlertDialog(SellerProductActivity.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Network Error")
                            .setContentText("Please check your internet connection.")
                            .setConfirmText("Settings")
                            .setCancelText("Okay")
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();
                                    startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 1);
                                }
                            })
                            .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();
                                }
                            })
                            .show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                listviewsearch.setVisibility(View.GONE);
                noContent.setVisibility(View.VISIBLE);
                Log.e("ERROR", error.toString());
                hideProgressDialog();
                new SweetAlertDialog(SellerProductActivity.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Network Error")
                        .setContentText("Please check your internet connection.")
                        .setConfirmText("Settings")
                        .setCancelText("Okay")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();
                                startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 1);
                            }
                        })
                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();
                            }
                        })
                        .show();
            }
        });
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(50 * 1000, 3,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        // Access the RequestQueue through your singleton class.
        MySingleton.getInstance(this).addToRequestQueue(jsObjRequest);
    }
    Dialog mDialog;
    private void showProgressDialog(){
        mDialog = new Dialog(this);
        mDialog.setCancelable(false);
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        mDialog.setContentView(R.layout.custom_progress_dialog);
        mDialog.show();
    }
    private void hideProgressDialog(){
        mDialog.dismiss();
    }
    public void back(View view){
        super.onBackPressed();
    }
}
