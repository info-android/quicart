package com.quicart.quicart.Activity;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.quicart.quicart.Adapter.HomeAdapter5;
import com.quicart.quicart.Helper.Common;
import com.quicart.quicart.Helper.MethodClass;
import com.quicart.quicart.Helper.MySingleton;
import com.quicart.quicart.R;
import com.google.android.gms.common.api.Status;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;




import static com.quicart.quicart.Helper.Constants.ATTR_NAME;
import static com.quicart.quicart.Helper.Constants.BANNER_IMG_URL;
import static com.quicart.quicart.Helper.Constants.DISC_DATE;
import static com.quicart.quicart.Helper.Constants.PRODUCT_DESC;
import static com.quicart.quicart.Helper.Constants.PRODUCT_DIC_PRICE;
import static com.quicart.quicart.Helper.Constants.PRODUCT_ID;
import static com.quicart.quicart.Helper.Constants.PRODUCT_IMAGE;
import static com.quicart.quicart.Helper.Constants.PRODUCT_NAME;
import static com.quicart.quicart.Helper.Constants.PRODUCT_PRICE;
import static com.quicart.quicart.Helper.Constants.PROFILE_IMG_URL;
import static com.quicart.quicart.Helper.Constants.COUNTRIES;
import static com.quicart.quicart.Helper.Constants.SRCH_KEY;
import static com.quicart.quicart.Helper.Constants.USER_LANG;
import static com.quicart.quicart.Helper.Constants.USER_LAT;


public class HomeActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private NavigationView navigationView;
    private DrawerLayout drawer;
    private ImageView menu_iv;
    private ImageView noti;
    private CircleImageView storeImg;
    private TextView view1, view2, view3, view4, view5;
    private boolean doubleBackToExitPressedOnce = false;
    private RecyclerView rv_1, rv_2, rv_3, rv_4, rv_5;
    private LinearLayout browse_layout, cart_layout, home_layout;
    private ArrayList<HashMap<String, String>> map_list1, map_list2, map_list3, map_list4, map_list5;
    private String store_id = "";
    private String store_img = "";
    private String[] store_data;
    private TextView store_tv;
    private TextView sign_up;
    private TextView cart_count;
    private View headerView;
    TextInputEditText location;
    private LinearLayout head_lay, head_lay2, updatepro;
    private TextView dummyhead, dummyhead2, cntrctr_name_tv, cntrctr_name_tv2, nav_name_tv, email_head, cAdd;
    private EditText store, store2;
    private EditText store_toolbar;
    private EditText edt_search;
    private CircleImageView imgPro;
    int PLACE_AUTOCOMPLETE_REQUEST_CODE = 4;
    private AppBarLayout appBarLayout;
    private CollapsingToolbarLayout collapsingToolbar;
    private TextView item_title_1, item_title_2, item_title_3, item_title_4, item_title_5, openTIme, openTIme2, noti_count;
    private CoordinatorLayout coordinateLay;
    String email, fname, lname, user_id = "", address_id = "";
    Boolean is_logged_in;
    private String lat = "", lng = "", address, stores = "",sellername="";
    private LayoutInflater linf;
    private LinearLayout main_contain;
    private RelativeLayout notiLayout;
    private CircleImageView logoIcon;
    private CardView itemCardView;
    private CardView itemCardView2;
    private EditText store22;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_home);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        MethodClass.hide_keyboard(HomeActivity.this);
        is_logged_in = PreferenceManager.getDefaultSharedPreferences(HomeActivity.this).getBoolean("is_logged_in", false);
        user_id = PreferenceManager.getDefaultSharedPreferences(HomeActivity.this).getString("user_id", "");
        bottumLaySetColor("home");
        home_layout = (LinearLayout) findViewById(R.id.home_layout);
        logoIcon = (CircleImageView) findViewById(R.id.logoIcon);

        head_lay = (LinearLayout) findViewById(R.id.head_lay);
        head_lay2 = (LinearLayout) findViewById(R.id.head_lay2);
        main_contain = (LinearLayout) findViewById(R.id.main_contain);
        notiLayout = (RelativeLayout) findViewById(R.id.notiLayout);
        store = (EditText) findViewById(R.id.store);
        dummyhead = (TextView) findViewById(R.id.dummyhead);
        dummyhead2 = (TextView) findViewById(R.id.dummyhead2);
        openTIme = (TextView) findViewById(R.id.openTIme);
        openTIme2 = (TextView) findViewById(R.id.openTIme2);
        store_tv = (TextView) findViewById(R.id.store_tv);
        noti_count = (TextView) findViewById(R.id.noti_count);
        itemCardView = (CardView) findViewById(R.id.itemCardView);
        itemCardView2 = (CardView) findViewById(R.id.itemCardView2);


        cAdd = (TextView) findViewById(R.id.cAdd);
        cntrctr_name_tv = (TextView) findViewById(R.id.cntrctr_name_tv);
        edt_search = (EditText) findViewById(R.id.edt_search);
        store22 = (EditText) findViewById(R.id.store22);
        storeImg = (CircleImageView) findViewById(R.id.storeImg);

        noti = (ImageView) findViewById(R.id.noti);

        appBarLayout = (AppBarLayout) findViewById(R.id.appbar);
        collapsingToolbar = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        coordinateLay = (CoordinatorLayout) findViewById(R.id.coordinateLay);


        store_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MethodClass.go_to_next_activity(HomeActivity.this, ChooseSellerActivity.class);
            }
        });
        store22.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MethodClass.go_to_next_activity(HomeActivity.this, ChooseSellerActivity.class);
            }
        });


        if (getIntent().getExtras() != null) {
            cntrctr_name_tv.setVisibility(View.VISIBLE);
            head_lay.setVisibility(View.GONE);
            head_lay2.setVisibility(View.VISIBLE);

            store.setVisibility(View.VISIBLE);
            sellername = getIntent().getStringExtra("seller");
            stores = getIntent().getStringExtra("store");
            store_id = getIntent().getStringExtra("seller_ID");
            store_img = getIntent().getStringExtra("seller_img");

            Picasso.with(this).load(PROFILE_IMG_URL + store_img).placeholder(R.drawable.grocery_icon).error(R.drawable.grocery_icon).into(storeImg);
            store_tv.setText(stores);
            if (sellername.equals("") && sellername.equals("null") && sellername.equals(null)) {
                cntrctr_name_tv.setText("Not Applicable");
                edt_search.setHint("Search " + stores);
                store.setHint("Search " + stores);
                store.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent I = new Intent(HomeActivity.this, SearchActivity2.class);
                        if (!store_id.equals("")) {
                            I.putExtra("stores", stores);
                        }
                        startActivityForResult(I, 1);
                    }
                });
                edt_search.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent I = new Intent(HomeActivity.this, SearchActivity2.class);
                        if (!store_id.equals("")) {
                            I.putExtra("stores", stores);
                        }

                        startActivityForResult(I, 1);
                    }
                });
            } else {
                cntrctr_name_tv.setText(sellername);
                edt_search.setHint("Search " + sellername);
                store.setHint("Search " + sellername);
                store.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent I = new Intent(HomeActivity.this, SearchActivity2.class);
                        if (!store_id.equals("")) {
                            I.putExtra("stores", sellername);
                        }
                        startActivityForResult(I, 1);
                    }
                });
                edt_search.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent I = new Intent(HomeActivity.this, SearchActivity2.class);
                        if (!store_id.equals("")) {
                            I.putExtra("stores", sellername);
                        }

                        startActivityForResult(I, 1);
                    }
                });
            }

            cntrctr_name_tv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(HomeActivity.this, SellerPublicProfileActivity.class);
                    intent.putExtra("store_id", store_id);
                    startActivity(intent);
                }
            });
        }else {
            edt_search.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent I = new Intent(HomeActivity.this, SearchActivity2.class);
                    I.putExtra("stores", "");
                    startActivityForResult(I, 1);
                }
            });
        }
        browse_layout = (LinearLayout) findViewById(R.id.browse_layout);
        cart_layout = (LinearLayout) findViewById(R.id.cart_layout);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        /*ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();*/

        get_product_list();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        headerView = navigationView.getHeaderView(0);

        nav_name_tv = (TextView) headerView.findViewById(R.id.nav_name_tv);
        email_head = (TextView) headerView.findViewById(R.id.email);
        imgPro = (CircleImageView) headerView.findViewById(R.id.imgPro);
        updatepro = (LinearLayout) headerView.findViewById(R.id.updatepro);
        if (is_logged_in) {
            updatepro.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    MethodClass.go_to_next_activity(HomeActivity.this, CustomerEditProfileActivity.class);
                }
            });
        }

        navigationView.setNavigationItemSelectedListener(this);
        menu_iv = (ImageView) findViewById(R.id.img_navbtn);
        menu_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer.openDrawer(Gravity.START);
            }
        });

       /* edt_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MethodClass.go_to_next_activity(HomeActivity.this,SearchActivity2.class);

            }
        });*/


        notiLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (is_logged_in) {
                    Intent I = new Intent(HomeActivity.this, CustomerNotificationActivity.class);
                    startActivityForResult(I, 1);
                } else {

                }

            }
        });

        cAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getAddress();

            }
        });

        final Toolbar toolbar = (Toolbar) findViewById(R.id.anim_toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        head_lay = (LinearLayout) findViewById(R.id.head_lay);
       /* store_toolbar = (EditText) findViewById(R.id.store_toolbar);
        store_toolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });*/

        collapsingToolbar.setTitle("");


        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {

                if (Math.abs(verticalOffset)-appBarLayout.getTotalScrollRange() == 0)
                {
                    //  Collapsed
                    edt_search.setVisibility(View.VISIBLE);
                    store_tv.setVisibility(View.GONE);itemCardView2.setVisibility(View.GONE);
                    notiLayout.setVisibility(View.GONE);
                    itemCardView.setVisibility(View.GONE);
                    openTIme2.setVisibility(View.GONE);
                    dummyhead2.setVisibility(View.GONE);
                    storeImg.setVisibility(View.GONE);
                    if (store.getText().toString().length() != 0) {
                        edt_search.setText(store.getText().toString());
                    }
                    toolbar.setBackground(getDrawable(R.color.white));
                    menu_iv.setColorFilter(ContextCompat.getColor(HomeActivity.this, R.color.green), android.graphics.PorterDuff.Mode.MULTIPLY);


                }
                else
                {
                    edt_search.setVisibility(View.GONE);
                    store_tv.setVisibility(View.GONE);
                    itemCardView.setVisibility(View.VISIBLE);
                    itemCardView2.setVisibility(View.VISIBLE);
                    notiLayout.setVisibility(View.VISIBLE);
                    openTIme2.setVisibility(View.VISIBLE);
                    dummyhead2.setVisibility(View.VISIBLE);
                    storeImg.setVisibility(View.VISIBLE);
                    Log.e("verticalOffset", String.valueOf(verticalOffset));
                    Log.e("toolbar.getHeight() ", String.valueOf(toolbar.getHeight()));
                    toolbar.setBackground(getDrawable(R.color.transparent));
                    menu_iv.setColorFilter(ContextCompat.getColor(HomeActivity.this, R.color.white), android.graphics.PorterDuff.Mode.MULTIPLY);


                }
            }
        });

    }

    public class StringWithTag {
        public String string;
        public String string2;
        public String string3;
        public Object tag;

        public StringWithTag(String stringPart, String stringPart2, String stringPart3, Object tagPart) {
            string = stringPart;
            string2 = stringPart2;
            string3 = stringPart3;
            tag = tagPart;
        }

        @Override
        public String toString() {
            return string;
        }
    }


    public void getAddress() {
        if (is_logged_in) {
            String server_url = getString(R.string.SERVER_URL) + "address-list";
            final String token = getString(R.string.AUTH_KEY);
            HashMap<String, String> params1 = new HashMap<String, String>();
            params1.put("secret_key", token);
            params1.put("user_id", user_id);

            Log.e("avv", new JSONObject(params1).toString());
            showProgressDialog();
            JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, server_url, new JSONObject(params1), new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    Log.e("ERROR", response.toString());
                    //Toast.makeText(BussinessSignupActivity.this, response.toString(), Toast.LENGTH_LONG).show();
                    try {
                        hideProgressDialog();
                        String status = response.getString("status");
                        //Toast.makeText(BussinessSignupActivity.this, status, Toast.LENGTH_LONG).show();
                        if (status.equals("SUCCESS")) {
                            String user_data = response.getString("data");
                            JSONArray usrARR = new JSONArray(user_data);
                            if (usrARR.length() > 0) {

                                final Dialog dialog = new Dialog(HomeActivity.this);
                                dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                dialog.setContentView(R.layout.home_addess);
                                ImageView close = (ImageView) dialog.findViewById(R.id.close);
                                Spinner address_spinner = (Spinner) dialog.findViewById(R.id.address_spinner);
                                LinearLayout logAdd = (LinearLayout) dialog.findViewById(R.id.logAdd);
                                TextInputLayout gAdd = (TextInputLayout) dialog.findViewById(R.id.gAdd);
                                logAdd.setVisibility(View.VISIBLE);
                                gAdd.setVisibility(View.GONE);
                                Button create = (Button) dialog.findViewById(R.id.create);
                                Button Done = (Button) dialog.findViewById(R.id.Done);
                                List<StringWithTag> spinnerArray = new ArrayList<StringWithTag>();
                                for (int i = 0; i < usrARR.length(); i++) {

                                    String fname = usrARR.getJSONObject(i).getString("fname");
                                    String lngg = usrARR.getJSONObject(i).getString("lng");
                                    String id = usrARR.getJSONObject(i).getString("id");
                                    String latt = usrARR.getJSONObject(i).getString("lat");
                                    String address = usrARR.getJSONObject(i).getString("address");

                                    spinnerArray.add(new StringWithTag(address, latt, lngg, id));
                                }

                                ArrayAdapter adapter3 = new ArrayAdapter(getApplicationContext(), R.layout.multiline_spinner_row, spinnerArray) {
                                };
                                address_spinner.setAdapter(adapter3);
                                address_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                    @Override
                                    //for user salutation select
                                    public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                                        // An item was selected. You can retrieve the selected item using
                                        //StringWithTag s
                                        StringWithTag s = (StringWithTag) parent.getItemAtPosition(pos);
                                        address_id = String.valueOf(s.tag);
                                        lat = String.valueOf(s.string2);
                                        lng = String.valueOf(s.string3);
                                        address = String.valueOf(s.string);
                                    }

                                    @Override
                                    public void onNothingSelected(AdapterView<?> parent) {
                                        // Another interface callback
                                        return;
                                    }
                                });

                                create.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        Intent I = new Intent(HomeActivity.this, AddAddressActivity.class);
                                        startActivity(I);
                                    }
                                });
                                Done.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        dialog.dismiss();
                                        cAdd.setText(address);
                                        get_product_list();
                                    }
                                });


                                close.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        dialog.dismiss();
                                    }
                                });
                                dialog.show();


                            } else {
                                final Dialog dialog = new Dialog(HomeActivity.this);
                                dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                dialog.setContentView(R.layout.home_addess);
                                ImageView close = (ImageView) dialog.findViewById(R.id.close);
                                Button create = (Button) dialog.findViewById(R.id.create);
                                Button Done = (Button) dialog.findViewById(R.id.Done);
                                Spinner address_spinner = (Spinner) dialog.findViewById(R.id.address_spinner);
                                LinearLayout or = (LinearLayout) dialog.findViewById(R.id.or);
                                LinearLayout logAdd = (LinearLayout) dialog.findViewById(R.id.logAdd);
                                TextInputLayout gAdd = (TextInputLayout) dialog.findViewById(R.id.gAdd);
                                logAdd.setVisibility(View.VISIBLE);
                                gAdd.setVisibility(View.GONE);
                                address_spinner.setVisibility(View.GONE);
                                or.setVisibility(View.GONE);
                                close.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        dialog.dismiss();
                                    }
                                });
                                create.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        Intent I = new Intent(HomeActivity.this, AddAddressActivity.class);
                                        startActivity(I);
                                    }
                                });
                                Done.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        dialog.dismiss();
                                        get_product_list();
                                    }
                                });
                                dialog.show();

                            }


                        } else {

                            new SweetAlertDialog(HomeActivity.this, SweetAlertDialog.ERROR_TYPE).setTitleText("Network Error").setContentText("Please check your internet connection.").setConfirmText("Settings").setCancelText("Okay").setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();
                                    startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 1);
                                }
                            }).setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();
                                }
                            }).show();
                        }
                    } catch (JSONException e) {

                        Log.e("ERROR", e.toString());
                        hideProgressDialog();
                        new SweetAlertDialog(HomeActivity.this, SweetAlertDialog.ERROR_TYPE).setTitleText("Network Error").setContentText("Please check your internet connection.").setConfirmText("Settings").setCancelText("Okay").setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();
                                startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 1);
                            }
                        }).setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();
                            }
                        }).show();
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {

                    Log.e("ERROR", error.toString());
                    hideProgressDialog();
                    new SweetAlertDialog(HomeActivity.this, SweetAlertDialog.ERROR_TYPE).setTitleText("Network Error").setContentText("Please check your internet connection.").setConfirmText("Settings").setCancelText("Okay").setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sDialog) {
                            sDialog.dismissWithAnimation();
                            startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 1);
                        }
                    }).setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sDialog) {
                            sDialog.dismissWithAnimation();
                        }
                    }).show();
                }
            });
            jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(50 * 1000, 3, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            // Access the RequestQueue through your singleton class.
            MySingleton.getInstance(this).addToRequestQueue(jsObjRequest);


        } else {

            final Dialog dialog = new Dialog(HomeActivity.this);
            dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.home_addess);
            ImageView close = (ImageView) dialog.findViewById(R.id.close);
            Button Done = (Button) dialog.findViewById(R.id.Done);
            LinearLayout logAdd = (LinearLayout) dialog.findViewById(R.id.logAdd);
            TextInputLayout gAdd = (TextInputLayout) dialog.findViewById(R.id.gAdd);
            location = (TextInputEditText) dialog.findViewById(R.id.location);
            logAdd.setVisibility(View.GONE);
            gAdd.setVisibility(View.VISIBLE);
            // Initialize Places.
            Places.initialize(getApplicationContext(), "AIzaSyC1A0Zjdpb5eWY6MCTp_8ZOVAlDkUB4MTY");
            // Create a new Places client instance.
            PlacesClient placesClient = Places.createClient(this);
// Set the fields to specify which types of place data to return.
            final List<Place.Field> fields = Arrays.asList(Place.Field.LAT_LNG, Place.Field.NAME, Place.Field.ADDRESS);

            location.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.FULLSCREEN, fields).build(HomeActivity.this);
                    startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
                }
            });


            close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });
            Done.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                    get_product_list();
                }
            });
            dialog.show();


        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = Autocomplete.getPlaceFromIntent(data);
                location.setText(place.getAddress());
                cAdd.setText(place.getAddress());
                lat = String.valueOf(place.getLatLng().latitude);
                lng = String.valueOf(place.getLatLng().longitude);
            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                // TODO: Handle the error.
                Status status = Autocomplete.getStatusFromIntent(data);
                Log.i("PLACESS", status.getStatusMessage());
            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {

                edt_search.setText(SRCH_KEY);
                store.setText(SRCH_KEY);
                get_product_list();
            }
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        return false;
    }

    private void get_product_list() {
        String user_id=PreferenceManager.getDefaultSharedPreferences(HomeActivity.this).getString("user_id","");
        String server_url = getString(R.string.SERVER_URL) + "home";
        final String token = getString(R.string.AUTH_KEY);
        HashMap<String, String> params1 = new HashMap<String, String>();
        params1.put("secret_key", token);
        if (!store_id.equals("")) {
            params1.put("store_id", store_id);
        }
        params1.put("keyword", SRCH_KEY);
        if (!lat.equals("")) {
            params1.put("lat", String.valueOf(USER_LAT));
        }
        if (!lng.equals("")) {
            params1.put("lng", String.valueOf(USER_LANG));
        }
        params1.put("user_id",user_id);
        Log.e("Tst", new JSONObject(params1).toString());
        showProgressDialog();
        JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, server_url, new JSONObject(params1), new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.e("ERROR", response.toString());
                //Toast.makeText(BussinessSignupActivity.this, response.toString(), Toast.LENGTH_LONG).show();
                try {

                    String status = response.getString("status");
                    //Toast.makeText(BussinessSignupActivity.this, status, Toast.LENGTH_LONG).show();
                    if (status.equals("SUCCESS")) {
                        SRCH_KEY = "";
                        main_contain.removeAllViews();
                        String user_data = response.getString("data");
                        JSONObject datObj = new JSONObject(user_data);
                        String premium_product = datObj.getString("premium_product");
                        String cat_product = datObj.getString("cat_product");
                        //String all_product = datObj.getString("all_product");
                        String home_content = datObj.getString("home_content");
                        String total_earning=datObj.getString("total_earning");
                        PreferenceManager.getDefaultSharedPreferences(HomeActivity.this).edit().putString("wallet_bal",total_earning).commit();
                        JSONArray prmARR = new JSONArray(premium_product);


                        JSONObject hmContent = new JSONObject(home_content);

                        String banner_image = hmContent.getString("banner_image");
                        String urls = BANNER_IMG_URL + banner_image;

                        Picasso.with(HomeActivity.this).load(urls).into(new Target() {
                            @Override
                            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                                head_lay.setBackground(new BitmapDrawable(bitmap));
                                head_lay2.setBackground(new BitmapDrawable(bitmap));
                            }

                            @Override
                            public void onBitmapFailed(Drawable errorDrawable) { }

                            @Override
                            public void onPrepareLoad(Drawable placeHolderDrawable) {}
                        });
                        String logo = hmContent.getString("logo");
                        if (!store_id.equals("")) {
                            String seller = datObj.getString("seller");
                            final JSONObject selOBJ = new JSONObject(seller);
                            final String pricing = selOBJ.getString("pricing_policy");
                            final String today = selOBJ.getString("today");
                            if (!pricing.equals("null") && !pricing.equals(null)) {
                                dummyhead2.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        final Dialog dialog = new Dialog(HomeActivity.this);
                                        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                        dialog.setContentView(R.layout.privacy_popup);
                                        ImageView close = (ImageView) dialog.findViewById(R.id.close);
                                        TextView priCText = (TextView) dialog.findViewById(R.id.priCText);
                                        priCText.setText(pricing);
                                        close.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {
                                                dialog.dismiss();
                                            }
                                        });
                                        dialog.show();
                                    }
                                });
                            }
                            if (!today.equals("null") && !today.equals(null)) {
                                JSONObject todayObj = new JSONObject(today);
                                String from_time = todayObj.getString("from_time");
                                String to_time = todayObj.getString("to_time");

                                SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss");
                                SimpleDateFormat sdfs = new SimpleDateFormat("hh:mm a");
                                Date dt;
                                Date dt2;
                                try {
                                    dt = sdf.parse(from_time);
                                    dt2 = sdf.parse(to_time);
                                    openTIme.setText("Today, " + sdfs.format(dt) + " - " + sdfs.format(dt2));
                                    openTIme2.setText("Today, " + sdfs.format(dt) + " - " + sdfs.format(dt2));
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                openTIme.setText("Not availble");
                                openTIme2.setText("Not availble");
                            }
                        } else {
                            Picasso.with(HomeActivity.this).load(BANNER_IMG_URL + logo).placeholder(R.drawable.grocery_icon).error(R.drawable.grocery_icon).into(logoIcon);
                        }


                        linf = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        linf = LayoutInflater.from(HomeActivity.this);
                        JSONArray catArr = new JSONArray(cat_product);

                        if (prmARR.length() > 0) {
                            final View v = linf.inflate(R.layout.home_recycle_layout, null);
                            RecyclerView rv_5 = (RecyclerView) v.findViewById(R.id.rv_5);
                            TextView view5 = (TextView) v.findViewById(R.id.view5);
                            view5.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent I = new Intent(HomeActivity.this, SearchActivity.class);
                                    startActivity(I);
                                }
                            });

                            map_list5 = new ArrayList<HashMap<String, String>>();
                            for (int i = 0; i < prmARR.length(); i++) {

                                String id = prmARR.getJSONObject(i).getString("id");
                                String attribute_value_id = prmARR.getJSONObject(i).getString("attribute_value_id");

                                String price = prmARR.getJSONObject(i).getString("price");
                                String product_name = prmARR.getJSONObject(i).getString("product_name");

                                JSONObject pprod = new JSONObject(product_name);
                                String product_default_image = pprod.getString("product_default_image");
                                String name = pprod.getString("name");

                                String description = pprod.getString("description");
                                String discounted_price = prmARR.getJSONObject(i).getString("discounted_price");
                                String disc_to_date = prmARR.getJSONObject(i).getString("disc_to_date");
                                JSONObject imgOBJ = new JSONObject(product_default_image);

                                HashMap<String, String> map = new HashMap<String, String>();
                                map.put(PRODUCT_NAME, name);
                                map.put(PRODUCT_ID, id);
                                map.put(PRODUCT_PRICE, price);
                                map.put(PRODUCT_IMAGE, imgOBJ.getString("image"));
                                map.put(PRODUCT_DESC, description);
                                map.put(PRODUCT_DIC_PRICE, discounted_price);
                                map.put(DISC_DATE, disc_to_date);
                                if (!attribute_value_id.equals("null") && !attribute_value_id.equals(null) && !prmARR.getJSONObject(i).getString("attribute_name").equals("null")) {
                                    JSONObject attOBJ = new JSONObject(prmARR.getJSONObject(i).getString("attribute_name"));
                                    String attrName = attOBJ.getString("name");
                                    map.put(ATTR_NAME, attrName);
                                } else {
                                    map.put(ATTR_NAME, "");
                                }
                                map_list5.add(map);
                            }
                            HomeAdapter5 homeAdapter5 = new HomeAdapter5(HomeActivity.this, map_list5);
                            rv_5.setAdapter(homeAdapter5);
                            main_contain.addView(v);
                        }

                        if (catArr.length() > 0) {
                            for (int j = 0; j < catArr.length(); j++) {
                                final JSONObject catOBJB = catArr.getJSONObject(j);
                                String products = catOBJB.getString("products");
                                JSONArray proARRR = new JSONArray(products);
                                if (proARRR.length() > 0) {
                                    final View v = linf.inflate(R.layout.home_recycle_layout, null);
                                    RecyclerView rv_5 = (RecyclerView) v.findViewById(R.id.rv_5);
                                    TextView item_title_5 = (TextView) v.findViewById(R.id.item_title_5);
                                    TextView view5 = (TextView) v.findViewById(R.id.view5);

                                    item_title_5.setText(catOBJB.getString("category_name"));
                                    final String catee_id = catOBJB.getString("category_id");
                                    view5.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            Intent I = new Intent(HomeActivity.this, SearchActivity.class);
                                            I.putExtra("cat_id", catee_id);
                                            startActivity(I);
                                        }
                                    });


                                    Log.e("Lenght", String.valueOf(proARRR.length()));
                                    map_list5 = new ArrayList<HashMap<String, String>>();
                                    for (int l = 0; l < proARRR.length(); l++) {

                                        String id = proARRR.getJSONObject(l).getString("id");
                                        String attribute_value_ids = proARRR.getJSONObject(l).getString("attribute_value_id");
                                        String price = proARRR.getJSONObject(l).getString("price");
                                        String product_name = proARRR.getJSONObject(l).getString("product_name");

                                        JSONObject pprod = new JSONObject(product_name);
                                        String product_default_image = pprod.getString("product_default_image");
                                        String name = pprod.getString("name");

                                        String description = pprod.getString("description");
                                        String discounted_price = proARRR.getJSONObject(l).getString("discounted_price");
                                        String disc_to_date = proARRR.getJSONObject(l).getString("disc_to_date");
                                        JSONObject imgOBJ = new JSONObject(product_default_image);

                                        HashMap<String, String> map = new HashMap<String, String>();
                                        map.put(PRODUCT_NAME, name);
                                        map.put(PRODUCT_ID, id);
                                        map.put(PRODUCT_PRICE, price);
                                        map.put(PRODUCT_IMAGE, imgOBJ.getString("image"));
                                        map.put(PRODUCT_DESC, description);
                                        map.put(PRODUCT_DIC_PRICE, discounted_price);
                                        map.put(DISC_DATE, disc_to_date);
                                        if (!attribute_value_ids.equals("null") && !attribute_value_ids.equals(null) && !proARRR.getJSONObject(l).getString("attribute_name").equals("null")) {

                                            JSONObject attOBJ = new JSONObject(proARRR.getJSONObject(l).getString("attribute_name"));
                                            String attrName = attOBJ.getString("name");
                                            map.put(ATTR_NAME, attrName);
                                        } else {
                                            map.put(ATTR_NAME, "");
                                        }
                                        map_list5.add(map);
                                    }
                                    HomeAdapter5 homeAdapter5 = new HomeAdapter5(HomeActivity.this, map_list5);
                                    rv_5.setAdapter(homeAdapter5);
                                    main_contain.addView(v);
                                }
                            }
                        }
                        Common.set_nav_wallet_bal(HomeActivity.this);
                    } else {

                        new SweetAlertDialog(HomeActivity.this, SweetAlertDialog.ERROR_TYPE).setTitleText("Network Error").setContentText("Please check your internet connection.").setConfirmText("Settings").setCancelText("Okay").setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();
                                startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 1);
                            }
                        }).setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();
                            }
                        }).show();
                    }
                    hideProgressDialog();
                } catch (JSONException e) {
                    Log.e("ERROR", e.toString());
                    hideProgressDialog();
                    new SweetAlertDialog(HomeActivity.this, SweetAlertDialog.ERROR_TYPE).setTitleText("Network Error").setContentText("Please check your internet connection.").setConfirmText("Settings").setCancelText("Okay").setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sDialog) {
                            sDialog.dismissWithAnimation();
                            startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 1);
                        }
                    }).setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sDialog) {
                            sDialog.dismissWithAnimation();
                        }
                    }).show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("ERROR", error.toString());
                hideProgressDialog();
                new SweetAlertDialog(HomeActivity.this, SweetAlertDialog.ERROR_TYPE).setTitleText("Network Error").setContentText("Please check your internet connection.").setConfirmText("Settings").setCancelText("Okay").setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();
                        startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 1);
                    }
                }).setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();
                    }
                }).show();
            }
        });
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(50 * 1000, 3, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        // Access the RequestQueue through your singleton class.
        MySingleton.getInstance(this).addToRequestQueue(jsObjRequest);


    }

    public Bitmap getBitmapFromURL(String imageUrl) {
        try {
            URL url = new URL(imageUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }


    Dialog mDialog;

    private void showProgressDialog() {
        mDialog = new Dialog(this);
        mDialog.setCancelable(false);
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        mDialog.setContentView(R.layout.custom_progress_dialog);
        mDialog.show();
    }

    private void hideProgressDialog() {
        if(mDialog !=null){
            mDialog.dismiss();
        }

    }

    public void openmenu(View view) {
        drawer.openDrawer(Gravity.START);
    }

    public void order(View view) {
        if (is_logged_in) {
            MethodClass.go_to_next_activity(this, CustomerOrderActivity.class);
        } else {
            new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE).setTitleText("Login required").setContentText("Please login to continue").setConfirmText("Login").setCancelText("No").setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sDialog) {
                    sDialog.dismissWithAnimation();
                    Intent I = new Intent(HomeActivity.this, LoginActivity.class);
                    startActivity(I);
                }
            }).setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sDialog) {
                    sDialog.dismissWithAnimation();
                }
            }).show();
        }
    }

    public void edit(View view) {
        if (is_logged_in) {
            MethodClass.go_to_next_activity(this, CustomerEditProfileActivity.class);
        } else {
            new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE).setTitleText("Login required").setContentText("Please login to continue").setConfirmText("Login").setCancelText("No").setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sDialog) {
                    sDialog.dismissWithAnimation();
                    Intent I = new Intent(HomeActivity.this, LoginActivity.class);
                    startActivity(I);
                }
            }).setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sDialog) {
                    sDialog.dismissWithAnimation();
                }
            }).show();
        }
    }

    public void refer(View view) {
        if (is_logged_in) {
            MethodClass.go_to_next_activity(this, ReferralActivity.class);

        } else {
            new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE).setTitleText("Login required").setContentText("Please login to continue").setConfirmText("Login").setCancelText("No").setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sDialog) {
                    sDialog.dismissWithAnimation();
                    Intent I = new Intent(HomeActivity.this, LoginActivity.class);
                    startActivity(I);
                }
            }).setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sDialog) {
                    sDialog.dismissWithAnimation();
                }
            }).show();
        }

    }

    public void cartshop(View view) {
        if (is_logged_in) {
            MethodClass.go_to_next_activity(this, CartActivity.class);
        } else {
            new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE).setTitleText("Login required").setContentText("Please login to continue").setConfirmText("Login").setCancelText("No").setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sDialog) {
                    sDialog.dismissWithAnimation();
                    Intent I = new Intent(HomeActivity.this, LoginActivity.class);
                    startActivity(I);
                }
            }).setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sDialog) {
                    sDialog.dismissWithAnimation();
                }
            }).show();
        }
    }

    public void signup(View view) {
        MethodClass.go_to_next_activity(this, SignUpActivity.class);
    }

    public void login(View view) {
        MethodClass.go_to_next_activity(this, LoginActivity.class);
    }

    public void btmLogin(View view) {
        MethodClass.go_to_next_activity(this, LoginActivity.class);
    }

    public void browse(View view) {
        MethodClass.go_to_next_activity(this, BrowseActivity.class);
    }

    public void home(View view) {
        MethodClass.go_to_next_activity(this, HomeActivity.class);
    }

    public void shoppingcart(View view) {
        MethodClass.go_to_next_activity(this, ShoppingCartActivity.class);
    }

    public void help(View view) {
        MethodClass.go_to_next_activity(this, HelperActivity.class);
    }

    public void account(View view) {
        if (is_logged_in) {
            MethodClass.go_to_next_activity(this, AccountActivity.class);
        } else {
            new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE).setTitleText("Login required").setContentText("Please login to continue").setConfirmText("Login").setCancelText("No").setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sDialog) {
                    sDialog.dismissWithAnimation();
                    Intent I = new Intent(HomeActivity.this, LoginActivity.class);
                    startActivity(I);
                }
            }).setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sDialog) {
                    sDialog.dismissWithAnimation();
                }
            }).show();
        }
    }

    public void btmLogout(View view) {
        Intent intent = new Intent(this, SplashActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        Map<String, ?> prefs = PreferenceManager.getDefaultSharedPreferences(this).getAll();
        for (Map.Entry<String, ?> prefToReset : prefs.entrySet()) {

            if (!prefToReset.getKey().equals("first_time")) {
                PreferenceManager.getDefaultSharedPreferences(this).edit().remove(prefToReset.getKey()).commit();
            }
        }
        startActivity(intent);
    }

    public void logout(View view) {
        Intent intent = new Intent(this, SplashActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        Map<String, ?> prefs = PreferenceManager.getDefaultSharedPreferences(this).getAll();
        for (Map.Entry<String, ?> prefToReset : prefs.entrySet()) {

            if (!prefToReset.getKey().equals("first_time")) {
                PreferenceManager.getDefaultSharedPreferences(this).edit().remove(prefToReset.getKey()).commit();
            }
        }
        startActivity(intent);
    }

    public void addressbook(View view) {
        if (is_logged_in) {
            MethodClass.go_to_next_activity(this, AddressBookActivity.class);
        } else {
            new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE).setTitleText("Login required").setContentText("Please login to continue").setConfirmText("Login").setCancelText("No").setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sDialog) {
                    sDialog.dismissWithAnimation();
                    Intent I = new Intent(HomeActivity.this, LoginActivity.class);
                    startActivity(I);
                }
            }).setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sDialog) {
                    sDialog.dismissWithAnimation();
                }
            }).show();
        }
    }

    public void yourItem(View view) {
        if (is_logged_in) {
            MethodClass.go_to_next_activity(this, YourItemsActivity.class);
        } else {
            new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE).setTitleText("Login required").setContentText("Please login to continue").setConfirmText("Login").setCancelText("No").setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sDialog) {
                    sDialog.dismissWithAnimation();
                    Intent I = new Intent(HomeActivity.this, LoginActivity.class);
                    startActivity(I);
                }
            }).setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sDialog) {
                    sDialog.dismissWithAnimation();
                }
            }).show();
        }
    }

    public void show_edit(View view) {
        if (is_logged_in) {
            MethodClass.go_to_next_activity(this, CustomerProfileShowActivity.class);
        } else {
            new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE).setTitleText("Login required").setContentText("Please login to continue").setConfirmText("Login").setCancelText("No").setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sDialog) {
                    sDialog.dismissWithAnimation();
                    Intent I = new Intent(HomeActivity.this, LoginActivity.class);
                    startActivity(I);
                }
            }).setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sDialog) {
                    sDialog.dismissWithAnimation();
                }
            }).show();
        }
    }

    private void bottumLaySetColor(String s) {
        TextView home_tv, browse_tv, cart_tv, login_tv, itemText;
        ImageView home_img, browse_img, cart_img, login_img, itemImg;
        home_tv = (TextView) findViewById(R.id.home_tv);
        browse_tv = (TextView) findViewById(R.id.browse_tv);
        cart_tv = (TextView) findViewById(R.id.cart_tv);
        login_tv = (TextView) findViewById(R.id.login_tv);
        itemText = (TextView) findViewById(R.id.itemText);
        home_img = (ImageView) findViewById(R.id.home_img);
        browse_img = (ImageView) findViewById(R.id.browse_img);
        cart_img = (ImageView) findViewById(R.id.cart_img);
        login_img = (ImageView) findViewById(R.id.login_img);
        itemImg = (ImageView) findViewById(R.id.itemImg);


        LinearLayout login_layout = (LinearLayout) findViewById(R.id.login_layout);
        LinearLayout logout_layout = (LinearLayout) findViewById(R.id.logout_layout);

        DrawableCompat.setTint(home_img.getDrawable(), ContextCompat.getColor(this, R.color.black));
        home_tv.setTextColor(getResources().getColor(R.color.black));

        DrawableCompat.setTint(browse_img.getDrawable(), ContextCompat.getColor(this, R.color.black));
        browse_tv.setTextColor(getResources().getColor(R.color.black));

        DrawableCompat.setTint(cart_img.getDrawable(), ContextCompat.getColor(this, R.color.black));
        cart_tv.setTextColor(getResources().getColor(R.color.black));

        DrawableCompat.setTint(login_img.getDrawable(), ContextCompat.getColor(this, R.color.black));
        login_tv.setTextColor(getResources().getColor(R.color.black));

        DrawableCompat.setTint(itemImg.getDrawable(), ContextCompat.getColor(this, R.color.black));
        itemText.setTextColor(getResources().getColor(R.color.black));

        if (s.equals("home")) {
            home_tv.setTextColor(getResources().getColor(R.color.colorPrimary));
            DrawableCompat.setTint(home_img.getDrawable(), ContextCompat.getColor(this, R.color.colorPrimary));
        }
        if (s.equals("browse")) {
            DrawableCompat.setTint(browse_img.getDrawable(), ContextCompat.getColor(this, R.color.colorPrimary));
            browse_tv.setTextColor(getResources().getColor(R.color.colorPrimary));
        }
        if (s.equals("cart")) {
            DrawableCompat.setTint(cart_img.getDrawable(), ContextCompat.getColor(this, R.color.colorPrimary));
            cart_tv.setTextColor(getResources().getColor(R.color.colorPrimary));
        }
        if (s.equals("login")) {
            DrawableCompat.setTint(login_img.getDrawable(), ContextCompat.getColor(this, R.color.colorPrimary));
            login_tv.setTextColor(getResources().getColor(R.color.colorPrimary));
        }
        if (s.equals("items")) {
            DrawableCompat.setTint(itemImg.getDrawable(), ContextCompat.getColor(this, R.color.colorPrimary));
            itemText.setTextColor(getResources().getColor(R.color.colorPrimary));
        }

        if (is_logged_in) {
            login_layout.setVisibility(View.GONE);
            logout_layout.setVisibility(View.VISIBLE);
        } else {
            login_layout.setVisibility(View.VISIBLE);
            logout_layout.setVisibility(View.GONE);
        }

    }

    private void view_more_on_click(String title_name) {

        Intent intent = new Intent(HomeActivity.this, SearchActivity.class);
        intent.putExtra("title", title_name);
        startActivity(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();

        Common.set_nav_wallet_bal(HomeActivity.this);
        LinearLayout signin = (LinearLayout) headerView.findViewById(R.id.signin);
        LinearLayout signup = (LinearLayout) headerView.findViewById(R.id.signup);
        LinearLayout signout = (LinearLayout) headerView.findViewById(R.id.signout);
        if (is_logged_in) {
            signin.setVisibility(View.GONE);
            signup.setVisibility(View.GONE);
            signout.setVisibility(View.VISIBLE);
            nav_name_tv.setText(PreferenceManager.getDefaultSharedPreferences(this).getString("fname", "") + " " + PreferenceManager.getDefaultSharedPreferences(this).getString("lname", ""));
            email_head.setText(PreferenceManager.getDefaultSharedPreferences(this).getString("email", ""));
            Picasso.with(HomeActivity.this).load(PROFILE_IMG_URL + PreferenceManager.getDefaultSharedPreferences(this).getString("profile_image", "")).placeholder(R.drawable.ic_user) //this is optional the image to display while the url image is downloading
                    .error(R.drawable.ic_user)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                    .into(imgPro);

            String cus_req_count = PreferenceManager.getDefaultSharedPreferences(HomeActivity.this).getString("new_request_count", "0");
            if (!cus_req_count.equals("0")) {
                noti_count.setText(cus_req_count);
                noti_count.setVisibility(View.VISIBLE);
            } else {
                noti_count.setVisibility(View.GONE);
            }
            LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver, new IntentFilter("updateNoti"));

        } else {
            signin.setVisibility(View.VISIBLE);
            signup.setVisibility(View.VISIBLE);
            signout.setVisibility(View.GONE);
            nav_name_tv.setText("Hi, user");
            email_head.setText("");
        }
    }

    @Override
    protected void onPause() {
        // Unregister since the activity is not visible
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
        super.onPause();
    }

    // handler for received Intents for the "my-event" event
    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Extract data included in the Intent
            String users_id = getIntent().getStringExtra("user_id");
            String cus_req_count = PreferenceManager.getDefaultSharedPreferences(HomeActivity.this).getString("new_request_count", "0");
            if (user_id.equals(users_id)) {
                if (!cus_req_count.equals("0")) {
                    noti_count.setText(cus_req_count);
                    noti_count.setVisibility(View.VISIBLE);
                } else {
                    noti_count.setVisibility(View.GONE);
                }
            }

        }
    };
}
