package com.quicart.quicart.Activity;

import android.content.Intent;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.quicart.quicart.Helper.MethodClass;
import com.quicart.quicart.R;

import java.util.Map;



public class HelperDetailsActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener  {

   /* private LinearLayout updatepro;
    private NavigationView navigationView;
    private DrawerLayout drawer;*/

    Boolean is_logged_in;
   /* private TextView nav_name_tv,email_head;
    private ImageView imgPro;
    private View headerView;*/
    private String user_id="";


    private TextView title;
    private TextView description;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.content_helper_details);
        MethodClass.hide_keyboard(HelperDetailsActivity.this);

        //drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        title = (TextView) findViewById(R.id.title);
        description = (TextView) findViewById(R.id.description);

      /*  navigationView = (NavigationView) findViewById(R.id.nav_view);
        headerView = navigationView.getHeaderView(0);
        nav_name_tv=(TextView) headerView.findViewById(R.id.nav_name_tv);
        email_head=(TextView) headerView.findViewById(R.id.email);
        imgPro=(ImageView) headerView.findViewById(R.id.imgPro);
        updatepro=(LinearLayout) headerView.findViewById(R.id.updatepro);*/
        is_logged_in = PreferenceManager.getDefaultSharedPreferences(HelperDetailsActivity.this).getBoolean("is_logged_in",false);
        user_id = PreferenceManager.getDefaultSharedPreferences(HelperDetailsActivity.this).getString("user_id","");

       /* if(is_logged_in){
            updatepro.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    MethodClass.go_to_next_activity(HelperDetailsActivity.this,CustomerEditProfileActivity.class);
                }
            });
        }
        navigationView.setNavigationItemSelectedListener(this);*/

        title.setText(getIntent().getStringExtra("name"));
        description.setText(Html.fromHtml(getIntent().getStringExtra("detail")));
    }



    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        return false;
    }
/*
    public void openmenu(View view) {
        drawer.openDrawer(Gravity.START);
    }
*/
    public void order(View view){
        if(is_logged_in){
            MethodClass.go_to_next_activity(this, CustomerOrderActivity.class);
        }else{
            MethodClass.go_to_next_activity(this, LoginActivity.class);
        }
    }
    public void edit(View view){
        if(is_logged_in){
            MethodClass.go_to_next_activity(this, CustomerEditProfileActivity.class);
        }else{
            MethodClass.go_to_next_activity(this, LoginActivity.class);
        }
    }
    public void refer(View view){
        if(is_logged_in){
            MethodClass.go_to_next_activity(this, ReferralActivity.class);
        }else{
            MethodClass.go_to_next_activity(this, LoginActivity.class);
        }

    }
    public void cartshop(View view){
        if(is_logged_in){
            MethodClass.go_to_next_activity(this, CartActivity.class);
        }else{
            MethodClass.go_to_next_activity(this, LoginActivity.class);
        }


    }
    public void signup(View view){
        MethodClass.go_to_next_activity(this, SignUpActivity.class);
    }
    public void login(View view){
        MethodClass.go_to_next_activity(this, LoginActivity.class);
    }
    public void btmLogin(View view){
        MethodClass.go_to_next_activity(this, LoginActivity.class);
    }
    public void browse(View view){
        MethodClass.go_to_next_activity(this, BrowseActivity.class);
    }
    public void home(View view){
        MethodClass.go_to_next_activity(this, HomeActivity.class);
    }
    public void shoppingcart(View view){
        MethodClass.go_to_next_activity(this, ShoppingCartActivity.class);
    }
    public void help(View view){
        MethodClass.go_to_next_activity(this, HelperActivity.class);
    }
    public void account(View view){
        if(is_logged_in){
            MethodClass.go_to_next_activity(this, AccountActivity.class);
        }else{
            MethodClass.go_to_next_activity(this, LoginActivity.class);
        }
    }
    public void btmLogout(View view){
        Intent intent=new Intent(this, SplashActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
        Map<String,?> prefs = PreferenceManager.getDefaultSharedPreferences(this).getAll();
        for(Map.Entry<String,?> prefToReset : prefs.entrySet()){

            if(!prefToReset.getKey().equals("first_time")){
                PreferenceManager.getDefaultSharedPreferences(this).edit().remove(prefToReset.getKey()).commit();
            }
        }
        startActivity(intent);
    }
    public void logout(View view){
        Intent intent=new Intent(this, SplashActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
        Map<String,?> prefs = PreferenceManager.getDefaultSharedPreferences(this).getAll();
        for(Map.Entry<String,?> prefToReset : prefs.entrySet()){

            if(!prefToReset.getKey().equals("first_time")){
                PreferenceManager.getDefaultSharedPreferences(this).edit().remove(prefToReset.getKey()).commit();
            }
        }
        startActivity(intent);
    }
    public void addressbook(View view){
        if(is_logged_in){
            MethodClass.go_to_next_activity(this, AddressBookActivity.class);
        }else{
            MethodClass.go_to_next_activity(this, LoginActivity.class);
        }
    }
    public void yourItem(View view){
        if(is_logged_in){
            MethodClass.go_to_next_activity(this, YourItemsActivity.class);
        }else{
            MethodClass.go_to_next_activity(this, LoginActivity.class);
        }
    }
    public void show_edit(View view){
        if(is_logged_in){
            MethodClass.go_to_next_activity(this, CustomerProfileShowActivity.class);
        }else{
            MethodClass.go_to_next_activity(this, LoginActivity.class);
        }
    }
    @Override
    protected void onResume() {
        super.onResume();
       /* LinearLayout signin = (LinearLayout) headerView.findViewById(R.id.signin);
        LinearLayout signup = (LinearLayout) headerView.findViewById(R.id.signup);
        LinearLayout signout = (LinearLayout) headerView.findViewById(R.id.signout);
        LinearLayout login_layout=(LinearLayout)findViewById(R.id.login_layout);
        LinearLayout logout_layout=(LinearLayout)findViewById(R.id.logout_layout);
        if(is_logged_in){
            login_layout.setVisibility(View.GONE);
            logout_layout.setVisibility(View.VISIBLE);
            signin.setVisibility(View.GONE);
            signup.setVisibility(View.GONE);
            signout.setVisibility(View.VISIBLE);
            nav_name_tv.setText(PreferenceManager.getDefaultSharedPreferences(this).getString("fname","")+" "+PreferenceManager.getDefaultSharedPreferences(this).getString("lname",""));
            email_head.setText(PreferenceManager.getDefaultSharedPreferences(this).getString("email",""));
            Picasso.with(this)
                    .load(PROFILE_IMG_URL+PreferenceManager.getDefaultSharedPreferences(this).getString("profile_image",""))
                    .placeholder(R.drawable.ic_user) //this is optional the image to display while the url image is downloading
                    .error(R.drawable.ic_user)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                    .into(imgPro);
        }else{
            login_layout.setVisibility(View.VISIBLE);
            logout_layout.setVisibility(View.GONE);
            signin.setVisibility(View.VISIBLE);
            signup.setVisibility(View.VISIBLE);
            signout.setVisibility(View.GONE);
            nav_name_tv.setText("Hi, user");
            email_head.setText("");
        }*/
    }

    public void back(View view){
        super.onBackPressed();
    }
}
