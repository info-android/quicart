package com.quicart.quicart.Activity.Shopper;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.quicart.quicart.Adapter.ShopEditAdapter;
import com.quicart.quicart.Helper.MethodClass;
import com.quicart.quicart.Helper.MySingleton;
import com.quicart.quicart.R;

import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;



import static com.quicart.quicart.Helper.Constants.DETAILS_ARRAY;
import static com.quicart.quicart.Helper.Constants.ORD_MASTER_ID;
import static com.quicart.quicart.Helper.Constants.SELLER_COMPANY;
import static com.quicart.quicart.Helper.Constants.SELLER_ID;
import static com.quicart.quicart.Helper.Constants.SELLER_IMG;
import static com.quicart.quicart.Helper.Constants.SELLER_NAME;

public class ShopperEditorderActivity extends AppCompatActivity {
    private LinearLayout checkout;
    private LinearLayout cartContainer;
    private LinearLayout noContent;
    Boolean is_logged_in;
    private boolean doubleBackToExitPressedOnce=false;
    private RecyclerView cart_rv;
    private ArrayList<HashMap<String,String>> map_list1,map_list2;
    private String user_id = "";
    private String order_id = "";
    private TextView total;
    private TextView cart_count;
    private CardView itemCardView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_shopper_editorder);
        MethodClass.hide_keyboard(ShopperEditorderActivity.this);
        is_logged_in = PreferenceManager.getDefaultSharedPreferences(ShopperEditorderActivity.this).getBoolean("is_logged_in", false);
        user_id = PreferenceManager.getDefaultSharedPreferences(ShopperEditorderActivity.this).getString("user_id", "");
        cart_rv = (RecyclerView) findViewById(R.id.cart_rv);
        checkout = (LinearLayout) findViewById(R.id.checkout);
        cart_count = (TextView) findViewById(R.id.cart_count);
        itemCardView = (CardView) findViewById(R.id.itemCardView);
        noContent = (LinearLayout) findViewById(R.id.noContent);
        cartContainer = (LinearLayout) findViewById(R.id.cartContainer);
        total = findViewById(R.id.total);
        /*ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();*/
        order_id = getIntent().getStringExtra("order_id");

        checkout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent I = new Intent(ShopperEditorderActivity.this, OrderDetailsActivity.class);
                I.putExtra("order_id",order_id);
                startActivity(I);
                finish();
            }
        });
    }
        public void get_product_list(){
            String server_url = getString(R.string.SERVER_URL)+"shopper-order-edit";
            final String token = getString(R.string.AUTH_KEY);
            HashMap<String, String> params1 = new HashMap<String, String>();
            params1.put("secret_key", token);
            params1.put("user_id", user_id);
            params1.put("order_id", order_id);
            Log.e("avv", new JSONObject(params1).toString());
            showProgressDialog();
            JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, server_url, new JSONObject(params1), new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    Log.e("ERROR", response.toString());
                    //Toast.makeText(BussinessSignupActivity.this, response.toString(), Toast.LENGTH_LONG).show();
                    try {
                        hideProgressDialog();
                        String status = response.getString("status");
                        //Toast.makeText(BussinessSignupActivity.this, status, Toast.LENGTH_LONG).show();
                        if (status.equals("SUCCESS")) {

                            String cart_master = response.getString("order_master");

                            if (!cart_master.equals("") && !cart_master.equals("null") && !cart_master.equals(null)) {
                                final JSONObject cartMasterObj = new JSONObject(cart_master);
                                String total_price = cartMasterObj.getString("order_total");
                                String no_of_product = cartMasterObj.getString("order_qty");
                                final String cartId = cartMasterObj.getString("id");

                                total.setText("₦" + total_price);
                                String cart_details = response.getString("order_details");
                                JSONArray cartArr = new JSONArray(cart_details);
                                map_list1 = new ArrayList<HashMap<String, String>>();
                                for (int i = 0; i < cartArr.length(); i++) {
                                    String seller_name = cartArr.getJSONObject(i).getString("seller_name");
                                    String seller_id = cartArr.getJSONObject(i).getString("seller_id");
                                    String details_product = cartArr.getJSONObject(i).getString("order_detail");
                                    JSONObject setName = new JSONObject(seller_name);
                                    String sellerName = setName.getString("name");
                                    String sellerCompany = setName.getString("company_name");
                                    String image = setName.getString("image");
                                    HashMap<String, String> map = new HashMap<String, String>();
                                    map.put(SELLER_NAME, sellerName);
                                    map.put(SELLER_IMG, image);
                                    map.put(SELLER_ID, seller_id);
                                    map.put(SELLER_COMPANY, sellerCompany);
                                    map.put(ORD_MASTER_ID, order_id);
                                    map.put(DETAILS_ARRAY, details_product);
                                    map_list1.add(map);
                                }
                                ShopEditAdapter cartAdapter = new ShopEditAdapter(ShopperEditorderActivity.this, map_list1);
                                cart_rv.setFocusable(true);
                                cart_rv.setAdapter(cartAdapter);

                                if (cartArr.length() > 0) {
                                    checkout.setVisibility(View.VISIBLE);
                                    itemCardView.setVisibility(View.VISIBLE);
                                    cartContainer.setVisibility(View.VISIBLE);
                                    noContent.setVisibility(View.GONE);

                                } else {
                                    checkout.setVisibility(View.GONE);
                                    itemCardView.setVisibility(View.GONE);
                                    cartContainer.setVisibility(View.GONE);
                                    noContent.setVisibility(View.VISIBLE);
                                }

                            }else {
                                itemCardView.setVisibility(View.GONE);
                                checkout.setVisibility(View.GONE);
                                cartContainer.setVisibility(View.GONE);
                                noContent.setVisibility(View.VISIBLE);
                            }


                        }else {
                            cartContainer.setVisibility(View.GONE);
                            noContent.setVisibility(View.VISIBLE);
                            new SweetAlertDialog(ShopperEditorderActivity.this, SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText("Network Error")
                                    .setContentText("Please check your internet connection.")
                                    .setConfirmText("Settings")
                                    .setCancelText("Okay")
                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sDialog) {
                                            sDialog.dismissWithAnimation();
                                            startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 1);
                                        }
                                    })
                                    .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sDialog) {
                                            sDialog.dismissWithAnimation();
                                        }
                                    })
                                    .show();
                        }
                    } catch (JSONException e) {
                        cartContainer.setVisibility(View.GONE);
                        noContent.setVisibility(View.VISIBLE);
                        Log.e("ERROR", e.toString());
                        hideProgressDialog();
                        new SweetAlertDialog(ShopperEditorderActivity.this, SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("Network Error")
                                .setContentText("Please check your internet connection.")
                                .setConfirmText("Settings")
                                .setCancelText("Okay")
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sDialog) {
                                        sDialog.dismissWithAnimation();
                                        startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 1);
                                    }
                                })
                                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sDialog) {
                                        sDialog.dismissWithAnimation();
                                    }
                                })
                                .show();
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    cartContainer.setVisibility(View.GONE);
                    noContent.setVisibility(View.VISIBLE);
                    Log.e("ERROR", error.toString());
                    hideProgressDialog();
                    new SweetAlertDialog(ShopperEditorderActivity.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Network Error")
                            .setContentText("Please check your internet connection.")
                            .setConfirmText("Settings")
                            .setCancelText("Okay")
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();
                                    startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 1);
                                }
                            })
                            .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();
                                }
                            })
                            .show();
                }
            });
            jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(50 * 1000, 3,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            // Access the RequestQueue through your singleton class.
            MySingleton.getInstance(this).addToRequestQueue(jsObjRequest);
        }
        Dialog mDialog;
        private void showProgressDialog(){
            mDialog = new Dialog(this);
            mDialog.setCancelable(false);
            mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            mDialog.setContentView(R.layout.custom_progress_dialog);
            mDialog.show();
        }
        private void hideProgressDialog(){
            mDialog.dismiss();
        }
        public void back(View view){
            super.onBackPressed();
        }

    @Override
    protected void onResume() {
        super.onResume();
        get_product_list();
    }
}
