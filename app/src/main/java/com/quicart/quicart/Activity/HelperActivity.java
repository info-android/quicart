package com.quicart.quicart.Activity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.quicart.quicart.Adapter.HelperAdapter;
import com.quicart.quicart.Helper.MethodClass;
import com.quicart.quicart.Helper.MySingleton;
import com.quicart.quicart.R;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.quicart.quicart.Helper.Constants.ORD_ID;
import static com.quicart.quicart.Helper.Constants.ORD_NUMBER;



public class HelperActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private RecyclerView rv_Myorder;
    private ArrayList<HashMap<String, String>> orderlist;
   /* private LinearLayout updatepro;
    private NavigationView navigationView;
    private DrawerLayout drawer;*/
    private CardView help1;
    Boolean is_logged_in;
    /*private TextView nav_name_tv,email_head;
    private ImageView imgPro;
    private View headerView;*/
    private String user_id="";

    private TextView payment2;
    private TextView payment;
    private TextView price;
    private TextView delivery;
    private TextView order;
    private TextView works;

    private LinearLayout container;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.content_helper);
        MethodClass.hide_keyboard(HelperActivity.this);

       // drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        rv_Myorder = (RecyclerView) findViewById(R.id.rv_Myorder);
        bottumLaySetColor("homessdsd");
//        navigationView = (NavigationView) findViewById(R.id.nav_view);
//        headerView = navigationView.getHeaderView(0);
//        nav_name_tv=(TextView) headerView.findViewById(R.id.nav_name_tv);
//        email_head=(TextView) headerView.findViewById(R.id.email);
//        imgPro=(ImageView) headerView.findViewById(R.id.imgPro);
//        updatepro=(LinearLayout) headerView.findViewById(R.id.updatepro);
        is_logged_in = PreferenceManager.getDefaultSharedPreferences(HelperActivity.this).getBoolean("is_logged_in",false);
        user_id = PreferenceManager.getDefaultSharedPreferences(HelperActivity.this).getString("user_id","");

       /* if(is_logged_in){
            updatepro.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    MethodClass.go_to_next_activity(HelperActivity.this,CustomerEditProfileActivity.class);
                }
            });
        }
        navigationView.setNavigationItemSelectedListener(this);
*/
        getHelp();
    }

    public void getHelp(){
        String server_url = getString(R.string.SERVER_URL)+"help";
        final String token = getString(R.string.AUTH_KEY);
        HashMap<String, String> params1 = new HashMap<String, String>();
        params1.put("secret_key", token);
        Log.e("avv", new JSONObject(params1).toString());
        showProgressDialog();
        JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, server_url, new JSONObject(params1), new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.e("ERROR", response.toString());
                //Toast.makeText(BussinessSignupActivity.this, response.toString(), Toast.LENGTH_LONG).show();
                try {
                    hideProgressDialog();
                    String status = response.getString("status");
                    //Toast.makeText(BussinessSignupActivity.this, status, Toast.LENGTH_LONG).show();
                    if (status.equals("SUCCESS")) {
                        final String data = response.getString("data");

                        final JSONObject datObj = new JSONObject(data);

                        String help = datObj.getString("help");

                        JSONArray helpArr = new JSONArray(help);
                        orderlist = new ArrayList<HashMap<String, String>>();
                        for (int i = 0; i <helpArr.length() ; i++) {
                            final String category = helpArr.getJSONObject(i).getString("category");
                            final String description = helpArr.getJSONObject(i).getString("description");
                            HashMap<String,String> list = new HashMap<String, String>();
                            list.put(ORD_ID,category);
                            list.put(ORD_NUMBER,description);
                            orderlist.add(list);
                        }
                        HelperAdapter adapter = new HelperAdapter(HelperActivity.this, orderlist);
                        rv_Myorder.setAdapter(adapter);
                    }else {
                        new SweetAlertDialog(HelperActivity.this, SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("Network Error")
                                .setContentText("Please check your internet connection.")
                                .setConfirmText("Settings")
                                .setCancelText("Okay")
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sDialog) {
                                        sDialog.dismissWithAnimation();
                                        startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 1);
                                    }
                                })
                                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sDialog) {
                                        sDialog.dismissWithAnimation();
                                    }
                                })
                                .show();
                    }
                } catch (JSONException e) {
                    Log.e("ERROR", e.toString());
                    hideProgressDialog();
                    new SweetAlertDialog(HelperActivity.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Network Error")
                            .setContentText("Please check your internet connection.")
                            .setConfirmText("Settings")
                            .setCancelText("Okay")
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();
                                    startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 1);
                                }
                            })
                            .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();
                                }
                            })
                            .show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("ERROR", error.toString());
                hideProgressDialog();
                new SweetAlertDialog(HelperActivity.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Network Error")
                        .setContentText("Please check your internet connection.")
                        .setConfirmText("Settings")
                        .setCancelText("Okay")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();
                                startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 1);
                            }
                        })
                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();
                            }
                        })
                        .show();
            }
        });
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(50 * 1000, 3,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        // Access the RequestQueue through your singleton class.
        MySingleton.getInstance(HelperActivity.this).addToRequestQueue(jsObjRequest);
    }
    Dialog mDialog;
    private void showProgressDialog(){
        mDialog = new Dialog(this);
        mDialog.setCancelable(false);
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        mDialog.setContentView(R.layout.custom_progress_dialog);
        mDialog.show();
    }
    private void hideProgressDialog(){
        mDialog.dismiss();
    }
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        return false;
    }
/*
    public void openmenu(View view) {
        drawer.openDrawer(Gravity.START);
    }
*/
    public void order(View view){
        if(is_logged_in){
            MethodClass.go_to_next_activity(this, CustomerOrderActivity.class);
        }else{
            MethodClass.go_to_next_activity(this, LoginActivity.class);
        }
    }
    public void edit(View view){
        if(is_logged_in){
            MethodClass.go_to_next_activity(this, CustomerEditProfileActivity.class);
        }else{
            MethodClass.go_to_next_activity(this, LoginActivity.class);
        }
    }
    public void refer(View view){
        if(is_logged_in){
            MethodClass.go_to_next_activity(this, ReferralActivity.class);
        }else{
            MethodClass.go_to_next_activity(this, LoginActivity.class);
        }

    }
    public void cartshop(View view){
        if(is_logged_in){
            MethodClass.go_to_next_activity(this, CartActivity.class);
        }else{
            MethodClass.go_to_next_activity(this, LoginActivity.class);
        }


    }
    public void signup(View view){
        MethodClass.go_to_next_activity(this, SignUpActivity.class);
    }
    public void login(View view){
        MethodClass.go_to_next_activity(this, LoginActivity.class);
    }
    public void btmLogin(View view){
        MethodClass.go_to_next_activity(this, LoginActivity.class);
    }
    public void browse(View view){
        MethodClass.go_to_next_activity(this, BrowseActivity.class);
    }
    public void home(View view){
        MethodClass.go_to_next_activity(this, HomeActivity.class);
    }
    public void shoppingcart(View view){
        MethodClass.go_to_next_activity(this, ShoppingCartActivity.class);
    }
    public void help(View view){
        MethodClass.go_to_next_activity(this, HelperActivity.class);
    }
    public void account(View view){
        if(is_logged_in){
            MethodClass.go_to_next_activity(this, AccountActivity.class);
        }else{
            MethodClass.go_to_next_activity(this, LoginActivity.class);
        }
    }
    public void btmLogout(View view){
        Intent intent=new Intent(this, SplashActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
        Map<String,?> prefs = PreferenceManager.getDefaultSharedPreferences(this).getAll();
        for(Map.Entry<String,?> prefToReset : prefs.entrySet()){

            if(!prefToReset.getKey().equals("first_time")){
                PreferenceManager.getDefaultSharedPreferences(this).edit().remove(prefToReset.getKey()).commit();
            }
        }
        startActivity(intent);
    }
    public void logout(View view){
        Intent intent=new Intent(this, SplashActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
        Map<String,?> prefs = PreferenceManager.getDefaultSharedPreferences(this).getAll();
        for(Map.Entry<String,?> prefToReset : prefs.entrySet()){

            if(!prefToReset.getKey().equals("first_time")){
                PreferenceManager.getDefaultSharedPreferences(this).edit().remove(prefToReset.getKey()).commit();
            }
        }
        startActivity(intent);
    }
    public void addressbook(View view){
        if(is_logged_in){
            MethodClass.go_to_next_activity(this, AddressBookActivity.class);
        }else{
            MethodClass.go_to_next_activity(this, LoginActivity.class);
        }
    }
    public void yourItem(View view){
        if(is_logged_in){
            MethodClass.go_to_next_activity(this, YourItemsActivity.class);
        }else{
            MethodClass.go_to_next_activity(this, LoginActivity.class);
        }
    }
    public void show_edit(View view){
        if(is_logged_in){
            MethodClass.go_to_next_activity(this, CustomerProfileShowActivity.class);
        }else{
            MethodClass.go_to_next_activity(this, LoginActivity.class);
        }
    }
    @Override
    protected void onResume() {
        super.onResume();
       /* LinearLayout signin = (LinearLayout) headerView.findViewById(R.id.signin);
        LinearLayout signup = (LinearLayout) headerView.findViewById(R.id.signup);
        LinearLayout signout = (LinearLayout) headerView.findViewById(R.id.signout);
        LinearLayout login_layout=(LinearLayout)findViewById(R.id.login_layout);
        LinearLayout logout_layout=(LinearLayout)findViewById(R.id.logout_layout);
        if(is_logged_in){
            login_layout.setVisibility(View.GONE);
            logout_layout.setVisibility(View.VISIBLE);
            signin.setVisibility(View.GONE);
            signup.setVisibility(View.GONE);
            signout.setVisibility(View.VISIBLE);
            nav_name_tv.setText(PreferenceManager.getDefaultSharedPreferences(this).getString("fname","")+" "+PreferenceManager.getDefaultSharedPreferences(this).getString("lname",""));
            email_head.setText(PreferenceManager.getDefaultSharedPreferences(this).getString("email",""));
            Picasso.with(this)
                    .load(PROFILE_IMG_URL+PreferenceManager.getDefaultSharedPreferences(this).getString("profile_image",""))
                    .placeholder(R.drawable.ic_user) //this is optional the image to display while the url image is downloading
                    .error(R.drawable.ic_user)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                    .into(imgPro);
        }else{
            login_layout.setVisibility(View.VISIBLE);
            logout_layout.setVisibility(View.GONE);
            signin.setVisibility(View.VISIBLE);
            signup.setVisibility(View.VISIBLE);
            signout.setVisibility(View.GONE);
            nav_name_tv.setText("Hi, user");
            email_head.setText("");
        }*/
    }
    private void bottumLaySetColor(String s){
        TextView home_tv,browse_tv,cart_tv,login_tv,itemText;
        ImageView home_img,browse_img,cart_img,login_img,itemImg;
        home_tv=(TextView)findViewById(R.id.home_tv);
        browse_tv=(TextView)findViewById(R.id.browse_tv);
        cart_tv=(TextView)findViewById(R.id.cart_tv);
        login_tv=(TextView)findViewById(R.id.login_tv);
        itemText=(TextView)findViewById(R.id.itemText);
        home_img=(ImageView)findViewById(R.id.home_img);
        browse_img=(ImageView)findViewById(R.id.browse_img);
        cart_img=(ImageView)findViewById(R.id.cart_img);
        login_img=(ImageView)findViewById(R.id.login_img);
        itemImg=(ImageView)findViewById(R.id.itemImg);

        DrawableCompat.setTint(home_img.getDrawable(), ContextCompat.getColor(this, R.color.black));
        home_tv.setTextColor(getResources().getColor(R.color.black));

        DrawableCompat.setTint(browse_img.getDrawable(), ContextCompat.getColor(this, R.color.black));
        browse_tv.setTextColor(getResources().getColor(R.color.black));

        DrawableCompat.setTint(cart_img.getDrawable(), ContextCompat.getColor(this, R.color.black));
        cart_tv.setTextColor(getResources().getColor(R.color.black));

        DrawableCompat.setTint(login_img.getDrawable(), ContextCompat.getColor(this, R.color.black));
        login_tv.setTextColor(getResources().getColor(R.color.black));

        DrawableCompat.setTint(itemImg.getDrawable(), ContextCompat.getColor(this, R.color.black));
        itemText.setTextColor(getResources().getColor(R.color.black));

        if (s.equals("home")){
            home_tv.setTextColor(getResources().getColor(R.color.colorPrimary));
            DrawableCompat.setTint(home_img.getDrawable(), ContextCompat.getColor(this, R.color.colorPrimary));
        }
        if (s.equals("browse")){
            DrawableCompat.setTint(browse_img.getDrawable(), ContextCompat.getColor(this, R.color.colorPrimary));
            browse_tv.setTextColor(getResources().getColor(R.color.colorPrimary));
        }
        if (s.equals("cart")){
            DrawableCompat.setTint(cart_img.getDrawable(), ContextCompat.getColor(this, R.color.colorPrimary));
            cart_tv.setTextColor(getResources().getColor(R.color.colorPrimary));
        }
        if (s.equals("login")){
            DrawableCompat.setTint(login_img.getDrawable(), ContextCompat.getColor(this, R.color.colorPrimary));
            login_tv.setTextColor(getResources().getColor(R.color.colorPrimary));
        }
        if (s.equals("items")){
            DrawableCompat.setTint(itemImg.getDrawable(), ContextCompat.getColor(this, R.color.colorPrimary));
            itemText.setTextColor(getResources().getColor(R.color.colorPrimary));
        }

    }

    public void back(View view){
        super.onBackPressed();
    }
}
