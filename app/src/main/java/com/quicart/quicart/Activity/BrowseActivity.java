package com.quicart.quicart.Activity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.quicart.quicart.Adapter.BrowseAdapter;
import com.quicart.quicart.Helper.Common;
import com.quicart.quicart.Helper.MethodClass;
import com.quicart.quicart.Helper.MySingleton;
import com.quicart.quicart.R;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.quicart.quicart.Helper.Constants.PRODUCT_ID;
import static com.quicart.quicart.Helper.Constants.PRODUCT_IMAGE;
import static com.quicart.quicart.Helper.Constants.PRODUCT_NAME;
import static com.quicart.quicart.Helper.Constants.PROFILE_IMG_URL;



public class BrowseActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private LinearLayout updatepro;
    private NavigationView navigationView;
    private DrawerLayout drawer;
    private ImageView menu_iv;
    private boolean doubleBackToExitPressedOnce=false;
    private RecyclerView browse_rv;
    private ArrayList<HashMap<String,String>> map_list1,map_list2;
    private ImageView cart;
    private ImageView search_iv;
    private TextView cart_count;
    private int[] images;
    Boolean is_logged_in;
    private TextView nav_name_tv,email_head,toolbar_title;
    private ImageView imgPro;
    private View headerView;
    private EditText edt_search;
    private LinearLayout noContent;
    private RelativeLayout cartLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_browse);
        MethodClass.hide_keyboard(BrowseActivity.this);

        browse_rv=(RecyclerView)findViewById(R.id.browse_rv);
        search_iv=(ImageView) findViewById(R.id.search_iv);
        edt_search=(EditText) findViewById(R.id.edt_search);
        toolbar_title=(TextView) findViewById(R.id.toolbar_title);
        is_logged_in = PreferenceManager.getDefaultSharedPreferences(BrowseActivity.this).getBoolean("is_logged_in",false);
        bottumLaySetColor("browse");
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        cart=(ImageView)findViewById(R.id.cart);
        cartLayout=(RelativeLayout) findViewById(R.id.cartLayout);


        cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MethodClass.go_to_next_activity(BrowseActivity.this,ShoppingCartActivity.class);
            }
        });
        /*ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();*/

        get_product_list();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        headerView = navigationView.getHeaderView(0);
        nav_name_tv=(TextView) headerView.findViewById(R.id.nav_name_tv);
        email_head=(TextView) headerView.findViewById(R.id.email);
        imgPro=(ImageView) headerView.findViewById(R.id.imgPro);
        updatepro=(LinearLayout) headerView.findViewById(R.id.updatepro);
        noContent=(LinearLayout) findViewById(R.id.noContent);
        if(is_logged_in){
            updatepro.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    MethodClass.go_to_next_activity(BrowseActivity.this,CustomerEditProfileActivity.class);
                }
            });
        }

        TextView nav_name_tv=(TextView) headerView.findViewById(R.id.nav_name_tv);
        navigationView.setNavigationItemSelectedListener(this);
        menu_iv=(ImageView)findViewById(R.id.img_navbtn);
        menu_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer.openDrawer(Gravity.START);
            }
        });

        search_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toolbar_title.setVisibility(View.GONE);
                cart.setVisibility(View.GONE);
                cartLayout.setVisibility(View.GONE);
                cart_count.setVisibility(View.GONE);
                search_iv.setVisibility(View.GONE);
                edt_search.setVisibility(View.VISIBLE);
            }
        });
        edt_search.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_ACTION_SEARCH) {
                    Intent intent=new Intent(BrowseActivity.this, SearchActivity.class);
                    intent.putExtra("cat_id","");
                    intent.putExtra("keywords",edt_search.getText().toString().trim());
                    startActivity(intent);
                    return true;
                }
                return false;
            }
        });

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        return false;
    }

    private void get_product_list(){
        String server_url = getString(R.string.SERVER_URL)+"category";
        final String token = getString(R.string.AUTH_KEY);
        HashMap<String, String> params1 = new HashMap<String, String>();
        params1.put("secret_key", token);
        params1.put("keyword", edt_search.getText().toString().trim());
        Log.e("keyword",edt_search.getText().toString().trim());
        Log.e("params",new JSONObject(params1).toString());
        showProgressDialog();
        JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, server_url, new JSONObject(params1), new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.e("ERROR", response.toString());
                //Toast.makeText(BussinessSignupActivity.this, response.toString(), Toast.LENGTH_LONG).show();
                try {
                    hideProgressDialog();
                    String status = response.getString("status");
                    //Toast.makeText(BussinessSignupActivity.this, status, Toast.LENGTH_LONG).show();
                    if (status.equals("SUCCESS")) {
                        toolbar_title.setVisibility(View.VISIBLE);
                        cart.setVisibility(View.VISIBLE);
                        cartLayout.setVisibility(View.VISIBLE);
                        if(!PreferenceManager.getDefaultSharedPreferences(BrowseActivity.this).getString("cart","").equals("") && !PreferenceManager.getDefaultSharedPreferences(BrowseActivity.this).getString("cart","").equals("null") && !PreferenceManager.getDefaultSharedPreferences(BrowseActivity.this).getString("cart","").equals(null)){
                            cart_count.setText(PreferenceManager.getDefaultSharedPreferences(BrowseActivity.this).getString("cart",""));
                            cart_count.setVisibility(View.VISIBLE);
                        }else {
                            cart_count.setVisibility(View.GONE);
                        }
                        search_iv.setVisibility(View.VISIBLE);
                        edt_search.setVisibility(View.GONE);
                        String user_data = response.getString("data");
                        JSONObject datObj = new JSONObject(user_data);
                        String premium_product = datObj.getString("category");
                        JSONArray prmARR = new JSONArray(premium_product);
                        if(prmARR.length()>0){
                            map_list1=new ArrayList<HashMap<String, String>>();
                            for (int i = 0; i <prmARR.length() ; i++) {

                                String name = prmARR.getJSONObject(i).getString("name");
                                String id = prmARR.getJSONObject(i).getString("id");
                                String image = prmARR.getJSONObject(i).getString("image");

                                HashMap<String,String> map=new HashMap<String, String>();
                                map.put(PRODUCT_NAME,name);
                                map.put(PRODUCT_ID,id);
                                map.put(PRODUCT_IMAGE,image);
                                map_list1.add(map);
                            }
                            browse_rv.setLayoutManager(new GridLayoutManager(BrowseActivity.this,2));
                            BrowseAdapter browseAdapter=new BrowseAdapter(BrowseActivity.this,map_list1);
                            browse_rv.setAdapter(browseAdapter);
                        }else{
                            browse_rv.setVisibility(View.GONE);
                            noContent.setVisibility(View.VISIBLE);
                        }

                    }else {
                        browse_rv.setVisibility(View.GONE);
                        noContent.setVisibility(View.VISIBLE);
                        new SweetAlertDialog(BrowseActivity.this, SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("Network Error")
                                .setContentText("Please check your internet connection.")
                                .setConfirmText("Settings")
                                .setCancelText("Okay")
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sDialog) {
                                        sDialog.dismissWithAnimation();
                                        startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 1);
                                    }
                                })
                                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sDialog) {
                                        sDialog.dismissWithAnimation();
                                    }
                                })
                                .show();
                    }
                } catch (JSONException e) {
                    browse_rv.setVisibility(View.GONE);
                    noContent.setVisibility(View.VISIBLE);
                    Log.e("ERROR", e.toString());
                    hideProgressDialog();
                    new SweetAlertDialog(BrowseActivity.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Network Error")
                            .setContentText("Please check your internet connection.")
                            .setConfirmText("Settings")
                            .setCancelText("Okay")
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();
                                    startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 1);
                                }
                            })
                            .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();
                                }
                            })
                            .show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                browse_rv.setVisibility(View.GONE);
                noContent.setVisibility(View.VISIBLE);
                Log.e("ERROR", error.toString());
                hideProgressDialog();
                new SweetAlertDialog(BrowseActivity.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Network Error")
                        .setContentText("Please check your internet connection.")
                        .setConfirmText("Settings")
                        .setCancelText("Okay")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();
                                startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 1);
                            }
                        })
                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();
                            }
                        })
                        .show();
            }
        });
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(50 * 1000, 3,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        // Access the RequestQueue through your singleton class.
        MySingleton.getInstance(this).addToRequestQueue(jsObjRequest);

    }
    Dialog mDialog;
    private void showProgressDialog(){
        mDialog = new Dialog(this);
        mDialog.setCancelable(false);
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        mDialog.setContentView(R.layout.custom_progress_dialog);
        mDialog.show();
    }
    private void hideProgressDialog(){
        mDialog.dismiss();
    }
    public void openmenu(View view) {
        drawer.openDrawer(Gravity.START);
    }
    public void order(View view){
        if(is_logged_in){
            MethodClass.go_to_next_activity(this, CustomerOrderActivity.class);
        }else{
            MethodClass.go_to_next_activity(this, LoginActivity.class);
        }
    }
    public void edit(View view){
        if(is_logged_in){
            MethodClass.go_to_next_activity(this, CustomerEditProfileActivity.class);
        }else{
            MethodClass.go_to_next_activity(this, LoginActivity.class);
        }
    }
    public void refer(View view){
        if(is_logged_in){
            MethodClass.go_to_next_activity(this, ReferralActivity.class);
        }else{
            MethodClass.go_to_next_activity(this, LoginActivity.class);
        }

    }
    public void cartshop(View view){
        if(is_logged_in){
            MethodClass.go_to_next_activity(this, CartActivity.class);
        }else{
            MethodClass.go_to_next_activity(this, LoginActivity.class);
        }


    }
    public void signup(View view){
        MethodClass.go_to_next_activity(this, SignUpActivity.class);
    }
    public void login(View view){
        MethodClass.go_to_next_activity(this, LoginActivity.class);
    }
    public void btmLogin(View view){
        MethodClass.go_to_next_activity(this, LoginActivity.class);
    }
    public void browse(View view){
        MethodClass.go_to_next_activity(this, BrowseActivity.class);
    }
    public void home(View view){
        MethodClass.go_to_next_activity(this, HomeActivity.class);
    }
    public void shoppingcart(View view){
        MethodClass.go_to_next_activity(this, ShoppingCartActivity.class);
    }
    public void help(View view){
        MethodClass.go_to_next_activity(this, HelperActivity.class);
    }
    public void account(View view){
        if(is_logged_in){
            MethodClass.go_to_next_activity(this, AccountActivity.class);
        }else{
            MethodClass.go_to_next_activity(this, LoginActivity.class);
        }
    }
    public void btmLogout(View view){
        Intent intent=new Intent(this, SplashActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
        Map<String,?> prefs = PreferenceManager.getDefaultSharedPreferences(this).getAll();
        for(Map.Entry<String,?> prefToReset : prefs.entrySet()){

            if(!prefToReset.getKey().equals("first_time")){
                PreferenceManager.getDefaultSharedPreferences(this).edit().remove(prefToReset.getKey()).commit();
            }
        }
        startActivity(intent);
    }
    public void logout(View view){
        Intent intent=new Intent(this, SplashActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
        Map<String,?> prefs = PreferenceManager.getDefaultSharedPreferences(this).getAll();
        for(Map.Entry<String,?> prefToReset : prefs.entrySet()){

            if(!prefToReset.getKey().equals("first_time")){
                PreferenceManager.getDefaultSharedPreferences(this).edit().remove(prefToReset.getKey()).commit();
            }
        }
        startActivity(intent);
    }
    public void addressbook(View view){
        if(is_logged_in){
            MethodClass.go_to_next_activity(this, AddressBookActivity.class);
        }else{
            MethodClass.go_to_next_activity(this, LoginActivity.class);
        }
    }
    public void yourItem(View view){
        if(is_logged_in){
            MethodClass.go_to_next_activity(this, YourItemsActivity.class);
        }else{
            MethodClass.go_to_next_activity(this, LoginActivity.class);
        }
    }
    public void show_edit(View view){
        if(is_logged_in){
            MethodClass.go_to_next_activity(this, CustomerProfileShowActivity.class);
        }else{
            MethodClass.go_to_next_activity(this, LoginActivity.class);
        }
    }
    private void bottumLaySetColor(String s){
        TextView home_tv,browse_tv,cart_tv,login_tv,itemText;
        ImageView home_img,browse_img,cart_img,login_img,itemImg;
        home_tv=(TextView)findViewById(R.id.home_tv);
        browse_tv=(TextView)findViewById(R.id.browse_tv);
        cart_tv=(TextView)findViewById(R.id.cart_tv);
        login_tv=(TextView)findViewById(R.id.login_tv);
        itemText=(TextView)findViewById(R.id.itemText);
        home_img=(ImageView)findViewById(R.id.home_img);
        browse_img=(ImageView)findViewById(R.id.browse_img);
        cart_img=(ImageView)findViewById(R.id.cart_img);
        login_img=(ImageView)findViewById(R.id.login_img);
        itemImg=(ImageView)findViewById(R.id.itemImg);


        LinearLayout login_layout=(LinearLayout)findViewById(R.id.login_layout);
        LinearLayout logout_layout=(LinearLayout)findViewById(R.id.logout_layout);

        DrawableCompat.setTint(home_img.getDrawable(), ContextCompat.getColor(this, R.color.black));
        home_tv.setTextColor(getResources().getColor(R.color.black));

        DrawableCompat.setTint(browse_img.getDrawable(), ContextCompat.getColor(this, R.color.black));
        browse_tv.setTextColor(getResources().getColor(R.color.black));

        DrawableCompat.setTint(cart_img.getDrawable(), ContextCompat.getColor(this, R.color.black));
        cart_tv.setTextColor(getResources().getColor(R.color.black));

        DrawableCompat.setTint(login_img.getDrawable(), ContextCompat.getColor(this, R.color.black));
        login_tv.setTextColor(getResources().getColor(R.color.black));

        DrawableCompat.setTint(itemImg.getDrawable(), ContextCompat.getColor(this, R.color.black));
        itemText.setTextColor(getResources().getColor(R.color.black));
        if (s.equals("home")){
            home_tv.setTextColor(getResources().getColor(R.color.colorPrimary));
            DrawableCompat.setTint(home_img.getDrawable(), ContextCompat.getColor(this, R.color.colorPrimary));
        }
        if (s.equals("browse")){
            DrawableCompat.setTint(browse_img.getDrawable(), ContextCompat.getColor(this, R.color.colorPrimary));
            browse_tv.setTextColor(getResources().getColor(R.color.colorPrimary));
        }
        if (s.equals("cart")){
            DrawableCompat.setTint(cart_img.getDrawable(), ContextCompat.getColor(this, R.color.colorPrimary));
            cart_tv.setTextColor(getResources().getColor(R.color.colorPrimary));
        }
        if (s.equals("login")){
            DrawableCompat.setTint(login_img.getDrawable(), ContextCompat.getColor(this, R.color.colorPrimary));
            login_tv.setTextColor(getResources().getColor(R.color.colorPrimary));
        }
        if (s.equals("items")){
            DrawableCompat.setTint(itemImg.getDrawable(), ContextCompat.getColor(this, R.color.colorPrimary));
            itemText.setTextColor(getResources().getColor(R.color.colorPrimary));
        }

        if(is_logged_in){
            login_layout.setVisibility(View.GONE);
            logout_layout.setVisibility(View.VISIBLE);
        }else{
            login_layout.setVisibility(View.VISIBLE);
            logout_layout.setVisibility(View.GONE);
        }


    }
    @Override
    protected void onResume() {
        super.onResume();
        Common.set_nav_wallet_bal(BrowseActivity.this);
        LinearLayout signin = (LinearLayout) headerView.findViewById(R.id.signin);
        LinearLayout signup = (LinearLayout) headerView.findViewById(R.id.signup);
        LinearLayout signout = (LinearLayout) headerView.findViewById(R.id.signout);
        LinearLayout login_layout=(LinearLayout)findViewById(R.id.login_layout);
        LinearLayout logout_layout=(LinearLayout)findViewById(R.id.logout_layout);

        cart_count = (TextView) findViewById(R.id.cart_count);

        if(is_logged_in){
            login_layout.setVisibility(View.GONE);
            logout_layout.setVisibility(View.VISIBLE);
            signin.setVisibility(View.GONE);
            signup.setVisibility(View.GONE);
            signout.setVisibility(View.VISIBLE);
            nav_name_tv.setText(PreferenceManager.getDefaultSharedPreferences(this).getString("fname","")+" "+PreferenceManager.getDefaultSharedPreferences(this).getString("lname",""));
            email_head.setText(PreferenceManager.getDefaultSharedPreferences(this).getString("email",""));
            Picasso.with(this)
                    .load(PROFILE_IMG_URL+PreferenceManager.getDefaultSharedPreferences(this).getString("profile_image",""))
                    .placeholder(R.drawable.ic_user) //this is optional the image to display while the url image is downloading
                    .error(R.drawable.ic_user)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                    .into(imgPro);
            if(!PreferenceManager.getDefaultSharedPreferences(this).getString("cart","").equals("") && !PreferenceManager.getDefaultSharedPreferences(this).getString("cart","").equals("null") && !PreferenceManager.getDefaultSharedPreferences(this).getString("cart","").equals(null)){
                cart_count.setText(PreferenceManager.getDefaultSharedPreferences(this).getString("cart",""));
                cart_count.setVisibility(View.VISIBLE);
            }else {
                cart_count.setVisibility(View.GONE);
            }
        }else{
            login_layout.setVisibility(View.VISIBLE);
            logout_layout.setVisibility(View.GONE);
            signin.setVisibility(View.VISIBLE);
            signup.setVisibility(View.VISIBLE);
            signout.setVisibility(View.GONE);
            nav_name_tv.setText("Hi, user");
            email_head.setText("");
        }


    }
}
