package com.quicart.quicart.Activity.Shopper;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.quicart.quicart.Helper.MySingleton;
import com.quicart.quicart.R;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.quicart.quicart.Helper.Constants.PROFILE_IMG_URL;


public class YourStatusActivity extends AppCompatActivity {
    private RadioButton available_rb;
    private RadioButton unavailable_rb;
    private RadioGroup avail;
    private TextView email;
    private CircleImageView image;


    private String user_id = "";
    private String is_avail = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_your_status);
        user_id = PreferenceManager.getDefaultSharedPreferences(YourStatusActivity.this).getString("user_id","");
        is_avail = PreferenceManager.getDefaultSharedPreferences(YourStatusActivity.this).getString("is_available","N");

        Log.e("is_avail", is_avail );


        image = (CircleImageView) findViewById(R.id.image);
        email = (TextView) findViewById(R.id.email);
        avail = (RadioGroup) findViewById(R.id.avail);
        available_rb = (RadioButton) findViewById(R.id.available_rb);
        unavailable_rb = (RadioButton) findViewById(R.id.unavailable_rb);
        email.setText(PreferenceManager.getDefaultSharedPreferences(this).getString("email",""));
        Picasso.with(this)
                .load(PROFILE_IMG_URL+PreferenceManager.getDefaultSharedPreferences(this).getString("profile_image",""))
                .placeholder(R.drawable.ic_user) //this is optional the image to display while the url image is downloading
                .error(R.drawable.ic_user)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                .into(image);
        if(is_avail.equals("Y")){
            available_rb.setChecked(true);
            unavailable_rb.setChecked(false);
        }else {
            available_rb.setChecked(false);
            unavailable_rb.setChecked(true);
        }
        avail.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // checkedId is the RadioButton selected
                if(checkedId == R.id.available_rb){
                    String server_url = getString(R.string.SERVER_URL)+"shopper-avaliable-status";
                    final String token = getString(R.string.AUTH_KEY);
                    HashMap<String, String> params1 = new HashMap<String, String>();
                    params1.put("secret_key", token);
                    params1.put("user_id", user_id);
                    params1.put("is_available", "Y");

                    Log.e("avv", new JSONObject(params1).toString());
                    showProgressDialog();
                    JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, server_url, new JSONObject(params1), new Response.Listener<JSONObject>() {

                        @Override
                        public void onResponse(JSONObject response) {
                            Log.e("ERROR", response.toString());
                            //Toast.makeText(BussinessSignupActivity.this, response.toString(), Toast.LENGTH_LONG).show();
                            try {
                                hideProgressDialog();
                                String status = response.getString("status");
                                //Toast.makeText(BussinessSignupActivity.this, status, Toast.LENGTH_LONG).show();
                                if (status.equals("SUCCESS")) {
                                    PreferenceManager.getDefaultSharedPreferences(YourStatusActivity.this).edit().putString("is_available","Y").commit();

                                }else {

                                    new SweetAlertDialog(YourStatusActivity.this, SweetAlertDialog.ERROR_TYPE)
                                            .setTitleText("Network Error")
                                            .setContentText("Please check your internet connection.")
                                            .setConfirmText("Settings")
                                            .setCancelText("Okay")
                                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                @Override
                                                public void onClick(SweetAlertDialog sDialog) {
                                                    sDialog.dismissWithAnimation();
                                                    startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 1);
                                                }
                                            })
                                            .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                @Override
                                                public void onClick(SweetAlertDialog sDialog) {
                                                    sDialog.dismissWithAnimation();
                                                }
                                            })
                                            .show();
                                }
                            } catch (JSONException e) {

                                Log.e("ERROR", e.toString());
                                hideProgressDialog();
                                new SweetAlertDialog(YourStatusActivity.this, SweetAlertDialog.ERROR_TYPE)
                                        .setTitleText("Network Error")
                                        .setContentText("Please check your internet connection.")
                                        .setConfirmText("Settings")
                                        .setCancelText("Okay")
                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sDialog) {
                                                sDialog.dismissWithAnimation();
                                                startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 1);
                                            }
                                        })
                                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sDialog) {
                                                sDialog.dismissWithAnimation();
                                            }
                                        })
                                        .show();
                            }
                        }
                    }, new Response.ErrorListener() {

                        @Override
                        public void onErrorResponse(VolleyError error) {

                            Log.e("ERROR", error.toString());
                            hideProgressDialog();
                            new SweetAlertDialog(YourStatusActivity.this, SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText("Network Error")
                                    .setContentText("Please check your internet connection.")
                                    .setConfirmText("Settings")
                                    .setCancelText("Okay")
                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sDialog) {
                                            sDialog.dismissWithAnimation();
                                            startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 1);
                                        }
                                    })
                                    .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sDialog) {
                                            sDialog.dismissWithAnimation();
                                        }
                                    })
                                    .show();
                        }
                    });
                    jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(50 * 1000, 3,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    // Access the RequestQueue through your singleton class.
                    MySingleton.getInstance(YourStatusActivity.this).addToRequestQueue(jsObjRequest);
                }
                if(checkedId == R.id.unavailable_rb){
                    String server_url = getString(R.string.SERVER_URL)+"shopper-avaliable-status";
                    final String token = getString(R.string.AUTH_KEY);
                    HashMap<String, String> params1 = new HashMap<String, String>();
                    params1.put("secret_key", token);
                    params1.put("user_id", user_id);
                    params1.put("is_available", "N");

                    Log.e("avv", new JSONObject(params1).toString());
                    showProgressDialog();
                    JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, server_url, new JSONObject(params1), new Response.Listener<JSONObject>() {

                        @Override
                        public void onResponse(JSONObject response) {
                            Log.e("ERROR", response.toString());
                            //Toast.makeText(BussinessSignupActivity.this, response.toString(), Toast.LENGTH_LONG).show();
                            try {
                                hideProgressDialog();
                                String status = response.getString("status");
                                //Toast.makeText(BussinessSignupActivity.this, status, Toast.LENGTH_LONG).show();
                                if (status.equals("SUCCESS")) {
                                    PreferenceManager.getDefaultSharedPreferences(YourStatusActivity.this).edit().putString("is_available","N").commit();
                                }else {

                                    new SweetAlertDialog(YourStatusActivity.this, SweetAlertDialog.ERROR_TYPE)
                                            .setTitleText("Network Error")
                                            .setContentText("Please check your internet connection.")
                                            .setConfirmText("Settings")
                                            .setCancelText("Okay")
                                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                @Override
                                                public void onClick(SweetAlertDialog sDialog) {
                                                    sDialog.dismissWithAnimation();
                                                    startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 1);
                                                }
                                            })
                                            .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                @Override
                                                public void onClick(SweetAlertDialog sDialog) {
                                                    sDialog.dismissWithAnimation();
                                                }
                                            })
                                            .show();
                                }
                            } catch (JSONException e) {

                                Log.e("ERROR", e.toString());
                                hideProgressDialog();
                                new SweetAlertDialog(YourStatusActivity.this, SweetAlertDialog.ERROR_TYPE)
                                        .setTitleText("Network Error")
                                        .setContentText("Please check your internet connection.")
                                        .setConfirmText("Settings")
                                        .setCancelText("Okay")
                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sDialog) {
                                                sDialog.dismissWithAnimation();
                                                startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 1);
                                            }
                                        })
                                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sDialog) {
                                                sDialog.dismissWithAnimation();
                                            }
                                        })
                                        .show();
                            }
                        }
                    }, new Response.ErrorListener() {

                        @Override
                        public void onErrorResponse(VolleyError error) {

                            Log.e("ERROR", error.toString());
                            hideProgressDialog();
                            new SweetAlertDialog(YourStatusActivity.this, SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText("Network Error")
                                    .setContentText("Please check your internet connection.")
                                    .setConfirmText("Settings")
                                    .setCancelText("Okay")
                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sDialog) {
                                            sDialog.dismissWithAnimation();
                                            startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 1);
                                        }
                                    })
                                    .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sDialog) {
                                            sDialog.dismissWithAnimation();
                                        }
                                    })
                                    .show();
                        }
                    });
                    jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(50 * 1000, 3,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    // Access the RequestQueue through your singleton class.
                    MySingleton.getInstance(YourStatusActivity.this).addToRequestQueue(jsObjRequest);
                }
            }
        });
    }
    public void back(View view){
        super.onBackPressed();
    }
    Dialog mDialog;
    private void showProgressDialog(){
        mDialog = new Dialog(this);
        mDialog.setCancelable(false);
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        mDialog.setContentView(R.layout.custom_progress_dialog);
        mDialog.show();
    }
    private void hideProgressDialog(){
        mDialog.dismiss();
    }
    public void notification(View view) {
        Intent intent=new Intent(YourStatusActivity.this,NotificationActivity.class);
        startActivity(intent);
    }
}
