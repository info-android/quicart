package com.quicart.quicart.Activity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.quicart.quicart.Helper.MethodClass;
import com.quicart.quicart.Helper.MySingleton;
import com.quicart.quicart.R;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.quicart.quicart.Helper.Constants.DEVICE_ID;


public class SignUpActivity extends AppCompatActivity {
    private TextView tv_login,toolbar_title;
    private Button sign_up_btn;
    private EditText et_f_name,et_l_name,et_email,et_password,et_cnfrm_password,et_ref_code,et_mob;

    String refferal_id ="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_sign_up);
        MethodClass.hide_keyboard(SignUpActivity.this);
        tv_login=(TextView)findViewById(R.id.tv_login);
        toolbar_title=(TextView)findViewById(R.id.toolbar_title);
        sign_up_btn=(Button) findViewById(R.id.sign_up_btn);

        et_f_name=(EditText) findViewById(R.id.et_f_name);
        et_l_name=(EditText) findViewById(R.id.et_l_name);
        et_email=(EditText) findViewById(R.id.et_email);
        et_password=(EditText) findViewById(R.id.et_password);
        et_cnfrm_password=(EditText) findViewById(R.id.et_cnfrm_password);
        et_ref_code=(EditText) findViewById(R.id.et_ref_code);
        et_mob=(EditText) findViewById(R.id.et_mob);
        final Handler handler = new Handler();
        Uri data = this.getIntent().getData();
        if (data != null && data.isHierarchical()) {
            Uri uri = Uri.parse(data.toString());
            refferal_id = uri.getQueryParameter("content");
            Log.e("Refferal",refferal_id );
        }

        toolbar_title.setText("Sign up");

        tv_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent I  = new Intent(SignUpActivity.this,LoginActivity.class);
                if(!refferal_id.equals("")){
                    I.putExtra("referral_id",refferal_id);
                    I.putExtra("from","");
                }
                startActivity(I);
            }
        });
        sign_up_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Signup();
            }
        });



    }

    public void Signup(){
        if(et_f_name.getText().toString().length() == 0){
            et_f_name.setError("Please enter your first name");
            et_f_name.requestFocus();
            return;
        }if(et_l_name.getText().toString().length() == 0){
            et_l_name.setError("Please enter your last name");
            et_l_name.requestFocus();
            return;
        }
        if(!emailValidator(et_email.getText().toString().trim())){
            et_email.setError("Please enter valid email");
            et_email.requestFocus();
            return;
        }
        if(et_mob.getText().toString().length() == 0){
            et_mob.setError("Please enter your mobile number");
            et_mob.requestFocus();
            return;
        }
        if(et_password.getText().toString().length() == 0){
            et_password.setError("Please enter your password");
            et_password.requestFocus();
            return;
        }
        if(!(et_password.getText().toString()).equals(et_cnfrm_password.getText().toString())){
            et_cnfrm_password.setError("Please enter your password again");
            et_cnfrm_password.requestFocus();
            return;
        }

        String server_url = getString(R.string.SERVER_URL)+"customer-register";
        String token = getString(R.string.AUTH_KEY);
        HashMap<String, String> params1 = new HashMap<String, String>();
        params1.put("secret_key", token);
        params1.put("first_name", et_f_name.getText().toString().trim());
        params1.put("last_name", et_l_name.getText().toString().trim());
        params1.put("email", et_email.getText().toString().trim());
        params1.put("password", et_password.getText().toString().trim());
        params1.put("mobile", et_mob.getText().toString().trim());
        params1.put("device_id", DEVICE_ID);
        if(!refferal_id.equals("")){
            params1.put("invitation_code", refferal_id);
        }
        if(et_ref_code.getText().toString().trim().length() != 0){
            params1.put("referral_code",et_ref_code.getText().toString().trim());
        }
        Log.e("Param", new JSONObject(params1).toString() );
        showProgressDialog();
        JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, server_url, new JSONObject(params1), new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.e("ERROR", response.toString());
                //Toast.makeText(BussinessSignUpActivity.this, response.toString(), Toast.LENGTH_LONG).show();
                try {
                    hideProgressDialog();
                    String status = response.getString("status");
                    //Toast.makeText(BussinessSignUpActivity.this, status, Toast.LENGTH_LONG).show();
                    if (status.equals("SUCCESS")) {
                        final String data = response.getString("data");
                        String message = "Your registration is successful. Please verify your account in the next step to login.";
                        new SweetAlertDialog(SignUpActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                                .setTitleText("Registration Successful")
                                .setContentText(message)
                                .setConfirmText("Ok")
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                                        sweetAlertDialog.dismissWithAnimation();
                                        Intent I = new Intent(SignUpActivity.this,VerificationActivity.class);
                                        I.putExtra("email_id",et_email.getText().toString().trim());
                                        I.putExtra("vcode",data);
                                        startActivity(I);
                                    }
                                })
                                .show();

                    }else if (status.equals("EMAIL_ALREADY_EXISTS")) {
                        String message = "The email you entered has been already registered with us. Please use another email.";
                        new SweetAlertDialog(SignUpActivity.this, SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("Email Already Exists")
                                .setContentText(message)
                                .setConfirmText("Ok")
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                                        sweetAlertDialog.dismissWithAnimation();
                                    }
                                })
                                .show();

                    }else if (status.equals("INVITATION_OVER")) {
                        String message = "Your partner invitation limit is over. You are not added to his group cart";
                        new SweetAlertDialog(SignUpActivity.this, SweetAlertDialog.WARNING_TYPE)
                                .setTitleText("Invitation Error")
                                .setContentText(message)
                                .setConfirmText("Ok")
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                                        sweetAlertDialog.dismissWithAnimation();
                                    }
                                })
                                .show();

                    }else if (status.equals("MOBILE_NUMBER_ALREADY_EXISTS")) {
                        String message = "The mobile number you entered already exists. Please use another one.";
                        new SweetAlertDialog(SignUpActivity.this, SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("Number already exists")
                                .setContentText(message)
                                .setConfirmText("Ok")
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                                        sweetAlertDialog.dismissWithAnimation();
                                    }
                                })
                                .show();

                    }else {
                        new SweetAlertDialog(SignUpActivity.this, SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("UNAUTHORIZED_ACCESS ")
                                .setContentText("Wrong secret key provided")
                                .setConfirmText("Settings")
                                .setCancelText("Okay")
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sDialog) {
                                        sDialog.dismissWithAnimation();
                                        startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 1);
                                    }
                                })
                                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sDialog) {
                                        sDialog.dismissWithAnimation();
                                    }
                                })
                                .show();
                    }
                } catch (JSONException e) {

                    hideProgressDialog();
                    new SweetAlertDialog(SignUpActivity.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Network Error")
                            .setContentText(e.toString())
                            .setConfirmText("Settings")
                            .setCancelText("Okay")
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();
                                    startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 1);
                                }
                            })
                            .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();
                                }
                            })
                            .show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                hideProgressDialog();
                new SweetAlertDialog(SignUpActivity.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Network Error")
                        .setContentText(error.toString())
                        .setConfirmText("Settings")
                        .setCancelText("Okay")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();
                                startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 1);
                            }
                        })
                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();
                            }
                        })
                        .show();
            }
        });
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(50 * 1000, 3,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        // Access the RequestQueue through your singleton class.
        MySingleton.getInstance(this).addToRequestQueue(jsObjRequest);
    }
    public void back(View view){
        super.onBackPressed();
    }
    Dialog mDialog;
    private void showProgressDialog(){
        mDialog = new Dialog(this);
        mDialog.setCancelable(false);
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        mDialog.setContentView(R.layout.custom_progress_dialog);
        mDialog.show();
    }
    private void hideProgressDialog(){
        mDialog.dismiss();
    }
    public boolean emailValidator(String email) {
        Pattern pattern;
        Matcher matcher;
        final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);
        return matcher.matches();
    }
}
