package com.quicart.quicart.Activity;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;
import com.quicart.quicart.Helper.MySingleton;
import com.quicart.quicart.R;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import static com.quicart.quicart.Helper.Constants.BANNER_IMG_URL;
import static com.quicart.quicart.Helper.Constants.COUNTRIES;
import static com.quicart.quicart.Helper.Constants.SRCH_KEY;


public class SearchActivity2 extends AppCompatActivity {
    private AutoCompleteTextView search_edt;
    private ImageView back_btn,cross_btn;

    private static final String[] COUNTRIESs = new String[]{
            "Afghanistan", "Albania", "Algeria", "Andorra", "Angola"
    };
    private String stores = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_search2);
        search_edt=(AutoCompleteTextView)findViewById(R.id.search_edt);
        back_btn=(ImageView) findViewById(R.id.back_btn);
        cross_btn=(ImageView)findViewById(R.id.cross_btn);
        //Log.e("Country", TextUtils.join(",",COUNTRIES));
        cross_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                search_edt.setText("");
            }
        });
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SRCH_KEY = search_edt.getText().toString();
                Intent returnIntent = new Intent();
                setResult(Activity.RESULT_OK, returnIntent);
                finish();
            }
        });



        getAllList();
    }
    public void getAllList(){
        String server_url = getString(R.string.SERVER_URL) + "all-product";
        final String token = getString(R.string.AUTH_KEY);
        HashMap<String, String> params1 = new HashMap<String, String>();
        params1.put("secret_key", token);
        showProgressDialog();
        JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, server_url, new JSONObject(params1), new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.e("ERROR", response.toString());
                //Toast.makeText(BussinessSignupActivity.this, response.toString(), Toast.LENGTH_LONG).show();
                try {
                    hideProgressDialog();
                    String all_product = response.getJSONObject("data").getString("all_product");
                    JSONArray allARR = new JSONArray(all_product);

                    COUNTRIES = new String[allARR.length()];

                    for (int i = 0; i < allARR.length(); i++) {
                        String proName = allARR.getJSONObject(i).getString("name");
                        COUNTRIES[i] = proName;
                    }

                    if(getIntent().getExtras() !=null){
                        stores = getIntent().getStringExtra("stores");
                        search_edt.setHint("Search "+stores);
                    }
                    search_edt.setText(SRCH_KEY);

                    //initialise the adapter for give n array of strings to be searched with
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(SearchActivity2.this,
                            android.R.layout.simple_list_item_1, COUNTRIES);
                    //set the adapter for the Autocomplete TextView
                    search_edt.setAdapter(adapter);
                    search_edt.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            SRCH_KEY = parent.getItemAtPosition(position).toString();
                            Intent returnIntent = new Intent();
                            setResult(Activity.RESULT_OK, returnIntent);
                            finish();
                        }
                    });
                    search_edt.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                        @Override
                        public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                            if (i == EditorInfo.IME_ACTION_SEARCH) {
                                SRCH_KEY = search_edt.getText().toString();
                                Intent returnIntent = new Intent();
                                setResult(Activity.RESULT_OK, returnIntent);
                                finish();
                                return true;
                            }
                            return false;
                        }
                    });
                    search_edt.setOnFocusChangeListener(new View.OnFocusChangeListener() {

                        @Override
                        public void onFocusChange(View v, boolean hasFocus) {
                            search_edt.showDropDown();
                        }
                    });

                    search_edt.setOnTouchListener(new View.OnTouchListener() {

                        @Override
                        public boolean onTouch(View v, MotionEvent event) {
                            search_edt.showDropDown();
                            return false;
                        }
                    });

                } catch (JSONException e) {
                    Log.e("ERROR", e.toString());
                    hideProgressDialog();
                    new SweetAlertDialog(SearchActivity2.this, SweetAlertDialog.ERROR_TYPE).setTitleText("Network Error").setContentText("Please check your internet connection.").setConfirmText("Settings").setCancelText("Okay").setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sDialog) {
                            sDialog.dismissWithAnimation();
                            startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 1);
                        }
                    }).setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sDialog) {
                            sDialog.dismissWithAnimation();
                        }
                    }).show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("ERROR", error.toString());
                hideProgressDialog();
                new SweetAlertDialog(SearchActivity2.this, SweetAlertDialog.ERROR_TYPE).setTitleText("Network Error").setContentText("Please check your internet connection.").setConfirmText("Settings").setCancelText("Okay").setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();
                        startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 1);
                    }
                }).setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();
                    }
                }).show();
            }
        });
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(50 * 1000, 3, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        // Access the RequestQueue through your singleton class.
        MySingleton.getInstance(SearchActivity2.this).addToRequestQueue(jsObjRequest);
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        SRCH_KEY = search_edt.getText().toString();
        Intent returnIntent = new Intent();
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }
    Dialog mDialog;

    private void showProgressDialog() {
        mDialog = new Dialog(this);
        mDialog.setCancelable(false);
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        mDialog.setContentView(R.layout.custom_progress_dialog);
        mDialog.show();
    }

    private void hideProgressDialog() {
        mDialog.dismiss();
    }
}
