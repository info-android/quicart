package com.quicart.quicart.Activity;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.quicart.quicart.Activity.Shopper.MyOrderActivity;
import com.quicart.quicart.R;


import java.util.ArrayList;
import java.util.List;

import static com.quicart.quicart.Helper.Constants.DEVICE_ID;
import static com.quicart.quicart.Helper.Constants.RADIUS;
import static com.quicart.quicart.Helper.Constants.USER_LANG;
import static com.quicart.quicart.Helper.Constants.USER_LAT;

public class SplashActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        com.google.android.gms.location.LocationListener {
    private boolean first_time_sign_up;
    private Location mylocation;
    private GoogleApiClient googleApiClient;
    private final static int REQUEST_CHECK_SETTINGS_GPS=0x1;
    private final static int REQUEST_ID_MULTIPLE_PERMISSIONS=0x2;
    private AlertDialog ExitApplication;
    private String user_type;
    private String android_id;
    public LocationManager locationManager;
    protected LocationListener locationListener;
    public Criteria criteria;
    public String bestProvider;
    boolean gps_enabled = false;
    boolean network_enabled = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_splash);
        first_time_sign_up = PreferenceManager.getDefaultSharedPreferences(SplashActivity.this).getBoolean("first_time", false);
        user_type = PreferenceManager.getDefaultSharedPreferences(SplashActivity.this).getString("user_type", "");
        RADIUS = PreferenceManager.getDefaultSharedPreferences(SplashActivity.this).getString("radius", "5");
        //setUpGClient();
        android_id = Settings.Secure.getString(getApplicationContext().getContentResolver(),
                Settings.Secure.ANDROID_ID);
        setUpGClient();
        DEVICE_ID = android_id;

        //checkPermissions();
    }

    public void startMyActivity() {
        Log.e("COND", first_time_sign_up + " " + user_type);
        if (first_time_sign_up && user_type.equals("C")) {
            startActivity(new Intent(SplashActivity.this, HomeActivity.class));
            finish();
        } else if (first_time_sign_up && user_type.equals("D")) {
            startActivity(new Intent(SplashActivity.this, MyOrderActivity.class));
            finish();
        } else if (first_time_sign_up && user_type.equals("")) {
            startActivity(new Intent(SplashActivity.this, HomeActivity.class));
            finish();
        } else {
            PreferenceManager.getDefaultSharedPreferences(SplashActivity.this).edit().putBoolean("first_time", true).commit();
            startActivity(new Intent(SplashActivity.this, FirstSignUpActivity.class));
            finish();
        }
    }

    private synchronized void setUpGClient() {
        googleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, 0, this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        googleApiClient.connect();
    }
    @Override
    public void onLocationChanged(Location location) {
        mylocation = location;
        if (mylocation != null) {
            Double latitude=mylocation.getLatitude();
            Double longitude=mylocation.getLongitude();
            USER_LAT = latitude;

            USER_LANG = longitude;
            //startMyActivity();
            //Toast.makeText(OpeningActivity.this, "CHANGED "+USER_LAT.toString()+" // "+USER_LANG.toString(), Toast.LENGTH_SHORT).show();

        }
    }
    @Override
    public void onConnected(Bundle bundle)
    {
        checkPermissions();
    }
    @Override
    public void onConnectionSuspended(int i) {
        //Do whatever you need
        //You can display a message here
    }
    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        //You can display a message here
    }
    private void getMyLocation(){
        if(googleApiClient!=null) {
            if (googleApiClient.isConnected()) {
                int permissionLocation = ContextCompat.checkSelfPermission(SplashActivity.this,
                        Manifest.permission.ACCESS_FINE_LOCATION);
                if (permissionLocation == PackageManager.PERMISSION_GRANTED) {
                    LocationRequest locationRequest = new LocationRequest();
                    locationRequest.setInterval(3000);
                    locationRequest.setFastestInterval(3000);
                    locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
                    LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                            .addLocationRequest(locationRequest);
                    builder.setAlwaysShow(true);
                    LocationServices.FusedLocationApi
                            .requestLocationUpdates(googleApiClient, locationRequest, (com.google.android.gms.location.LocationListener) this);
                    PendingResult<LocationSettingsResult> result =
                            LocationServices.SettingsApi
                                    .checkLocationSettings(googleApiClient, builder.build());
                    result.setResultCallback(new ResultCallback<LocationSettingsResult>() {

                        @Override
                        public void onResult(LocationSettingsResult result) {
                            final Status status = result.getStatus();
                            switch (status.getStatusCode()) {
                                case LocationSettingsStatusCodes.SUCCESS:
                                    // All location settings are satisfied.
                                    // You can initialize location requests here.
                                    int permissionLocation = ContextCompat
                                            .checkSelfPermission(SplashActivity.this,
                                                    Manifest.permission.ACCESS_FINE_LOCATION);
                                    if (permissionLocation == PackageManager.PERMISSION_GRANTED) {
                                        mylocation = LocationServices.FusedLocationApi
                                                .getLastLocation(googleApiClient);
                                        if(null != mylocation){
                                            final Handler handler = new Handler();
                                            handler.postDelayed(new Runnable() {
                                                @Override
                                                public void run() {
                                                    //Do something after 100ms
                                                    //Toast.makeText(SplashActivity.this, "FINE2 "+mylocation.getLatitude(), Toast.LENGTH_SHORT).show();
                                                    USER_LAT=mylocation.getLatitude();
                                                    USER_LANG=mylocation.getLongitude();
                                                    //Toast.makeText(SplashActivity.this, "FINE2 "+String.valueOf(USER_LAT), Toast.LENGTH_SHORT).show();
                                                    if(USER_LAT!=null && USER_LANG!=null){
                                                        startMyActivity();
                                                    }
                                                }
                                            }, 500);
                                        }else {
                                            getMyLocation();
                                        }

                                        //Toast.makeText(OpeningActivity.this, "FINE2 "+USER_LAT.toString(), Toast.LENGTH_SHORT).show();
                                    }
                                    break;
                                case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                                    // Location settings are not satisfied.
                                    // But could be fixed by showing the user a dialog.

                                    try {
                                        // Show the dialog by calling startResolutionForResult(),
                                        // and check the result in onActivityResult().
                                        // Ask to turn on GPS automatically
                                        status.startResolutionForResult(SplashActivity.this,
                                                REQUEST_CHECK_SETTINGS_GPS);
                                    } catch (IntentSender.SendIntentException e) {
                                        // Ignore the error.
                                    }
                                    break;
                                case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                                    // Location settings are not satisfied.
                                    // However, we have no way
                                    // to fix the
                                    // settings so we won't show the dialog.
                                    // finish();
                                    break;
                            }
                        }
                    });


                }
            }
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_CHECK_SETTINGS_GPS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        getMyLocation();
                        break;
                    case Activity.RESULT_CANCELED:
                        shoExitDialog();
                        break;
                }
                break;
        }
    }
    private void checkPermissions(){
        int permissionLocation = ContextCompat.checkSelfPermission(SplashActivity.this,
                android.Manifest.permission.ACCESS_FINE_LOCATION);
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (permissionLocation != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.ACCESS_FINE_LOCATION);
            if (!listPermissionsNeeded.isEmpty()) {
                ActivityCompat.requestPermissions(this,
                        listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), REQUEST_ID_MULTIPLE_PERMISSIONS);
            }
        }else{
            getMyLocation();
        }

    }
    private void shoExitDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(SplashActivity.this);
        builder.setMessage("Without the location service you will not be able to use this application." +
                " Please give permission to this application to access your location and turn on your location service" +
                " to enjoy the exclusive features of this application.")
                .setTitle("Exit Application");
        builder.setCancelable(false);

        builder.setPositiveButton("Exit", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User clicked OK button
                finish();
            }
        });
        ExitApplication = builder.create();
        ExitApplication.show();
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        int permissionLocation = ContextCompat.checkSelfPermission(SplashActivity.this,
                Manifest.permission.ACCESS_FINE_LOCATION);
        if (permissionLocation == PackageManager.PERMISSION_GRANTED) {
            getMyLocation();
        }else{

            shoExitDialog();
        }
    }
}
