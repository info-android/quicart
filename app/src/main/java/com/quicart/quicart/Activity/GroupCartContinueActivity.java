package com.quicart.quicart.Activity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.quicart.quicart.Adapter.GroupCartContinueAdapter;
import com.quicart.quicart.Helper.Common;
import com.quicart.quicart.Helper.MethodClass;
import com.quicart.quicart.Helper.MySingleton;
import com.quicart.quicart.R;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.quicart.quicart.Helper.Constants.CART_ID;
import static com.quicart.quicart.Helper.Constants.DETAILS_ARRAY;
import static com.quicart.quicart.Helper.Constants.OWNER_ID;
import static com.quicart.quicart.Helper.Constants.PROFILE_IMG_URL;
import static com.quicart.quicart.Helper.Constants.SELLER_COMPANY;
import static com.quicart.quicart.Helper.Constants.SELLER_ID;
import static com.quicart.quicart.Helper.Constants.SELLER_NAME;



public class GroupCartContinueActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private LinearLayout updatepro;
    private NavigationView navigationView;
    private DrawerLayout drawer;
    private ImageView menu_iv;
    private LinearLayout checkout;
    private boolean doubleBackToExitPressedOnce=false;
    private RecyclerView cart_rv;
    private ArrayList<HashMap<String,String>> map_list1,map_list2;
    Boolean is_logged_in;
    private TextView cart_count;
    private LinearLayout cartContainer;
    private LinearLayout noContent;
    String cart_id = "";
    private String user_id = "";
    private TextView nav_name_tv,email_head;
    private ImageView imgPro;
    private View headerView;
    private TextView total;
    private CardView itemCardView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_group_cart_continue);
        MethodClass.hide_keyboard(GroupCartContinueActivity.this);
        is_logged_in = PreferenceManager.getDefaultSharedPreferences(GroupCartContinueActivity.this).getBoolean("is_logged_in",false);
        user_id = PreferenceManager.getDefaultSharedPreferences(GroupCartContinueActivity.this).getString("user_id","");
        cart_rv=(RecyclerView)findViewById(R.id.cart_rv);
        checkout=(LinearLayout)findViewById(R.id.checkout);
        itemCardView=(CardView) findViewById(R.id.itemCardView);
        noContent = (LinearLayout) findViewById(R.id.noContent);
        cartContainer = (LinearLayout) findViewById(R.id.cartContainer);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        /*ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();*/

        if(getIntent().getExtras() !=null){
            cart_id = getIntent().getStringExtra("id");
        }
        get_product_list();
        checkout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent I = new Intent(GroupCartContinueActivity.this,CheckoutActivity.class);
                I.putExtra("from","group");
                I.putExtra("cart_id",cart_id);
                startActivity(I);
            }
        });
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        headerView = navigationView.getHeaderView(0);
        nav_name_tv=(TextView) headerView.findViewById(R.id.nav_name_tv);
        email_head=(TextView) headerView.findViewById(R.id.email);
        imgPro=(ImageView) headerView.findViewById(R.id.imgPro);
        updatepro=(LinearLayout) headerView.findViewById(R.id.updatepro);
        if(is_logged_in){
            updatepro.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    MethodClass.go_to_next_activity(GroupCartContinueActivity.this,CustomerEditProfileActivity.class);
                }
            });
        }

        navigationView.setNavigationItemSelectedListener(this);
        menu_iv=(ImageView)findViewById(R.id.img_navbtn);
        menu_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer.openDrawer(Gravity.START);
            }
        });
        total = findViewById(R.id.total);
    }


    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.user_dashboard, menu);
        return true;
    }*/



    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        return false;
    }
    public void continueShop(View view){
        Intent I = new Intent(this,HomeActivity.class);
        I.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(I);
    }
    public void get_product_list(){
        String server_url = getString(R.string.SERVER_URL)+"group-cart";
        final String token = getString(R.string.AUTH_KEY);
        HashMap<String, String> params1 = new HashMap<String, String>();
        params1.put("secret_key", token);
        params1.put("customer_id", user_id);
        params1.put("cart_id", cart_id);
        Log.e("avv", new JSONObject(params1).toString());
        showProgressDialog();
        JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, server_url, new JSONObject(params1), new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.e("ERROR", response.toString());
                //Toast.makeText(BussinessSignupActivity.this, response.toString(), Toast.LENGTH_LONG).show();
                try {
                    hideProgressDialog();
                    String status = response.getString("status");
                    //Toast.makeText(BussinessSignupActivity.this, status, Toast.LENGTH_LONG).show();
                    if (status.equals("SUCCESS")) {

                        String data = response.getString("data");
                        JSONObject dataObj = new JSONObject(data);

                        String cart_master = dataObj.getString("cart_master");
                        JSONObject cartMasterObj = new JSONObject(cart_master);
                        String total_price = cartMasterObj.getString("total_price");
                        String owner_id = cartMasterObj.getString("owner_id");
                        total.setText("₦ "+total_price);
                        String cart_details = dataObj.getString("cart_details");
                        JSONArray cartArr = new JSONArray(cart_details);
                        map_list1=new ArrayList<HashMap<String, String>>();
                        for (int i = 0; i <cartArr.length() ; i++) {
                            String seller_name = cartArr.getJSONObject(i).getString("seller_name");
                            String seller_id = cartArr.getJSONObject(i).getString("seller_id");
                            String details_product = cartArr.getJSONObject(i).getString("cart_detl");
                            JSONObject setName = new JSONObject(seller_name);
                            String sellerName = setName.getString("name");
                            String company_name = setName.getString("company_name");
                            HashMap<String,String> map=new HashMap<String, String>();
                            map.put(SELLER_NAME,sellerName);
                            map.put(SELLER_COMPANY,company_name);
                            map.put(SELLER_ID,seller_id);
                            map.put(DETAILS_ARRAY,details_product);
                            map.put(OWNER_ID,owner_id);
                            map.put(CART_ID,cart_id);
                            map_list1.add(map);
                        }
                        GroupCartContinueAdapter cartAdapter=new GroupCartContinueAdapter(GroupCartContinueActivity.this,map_list1);
                        cart_rv.setFocusable(true);
                        cart_rv.setAdapter(cartAdapter);

                        if(cartArr.length()>0){
                            checkout.setVisibility(View.VISIBLE);
                            itemCardView.setVisibility(View.VISIBLE);
                            cartContainer.setVisibility(View.VISIBLE);
                            noContent.setVisibility(View.GONE);
                        }else {
                            checkout.setVisibility(View.GONE);
                            itemCardView.setVisibility(View.GONE);
                            cartContainer.setVisibility(View.GONE);
                            noContent.setVisibility(View.VISIBLE);
                        }

                    }else {
                        cartContainer.setVisibility(View.GONE);
                        noContent.setVisibility(View.VISIBLE);
                        new SweetAlertDialog(GroupCartContinueActivity.this, SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("Network Error")
                                .setContentText("Please check your internet connection.")
                                .setConfirmText("Settings")
                                .setCancelText("Okay")
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sDialog) {
                                        sDialog.dismissWithAnimation();
                                        startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 1);
                                    }
                                })
                                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sDialog) {
                                        sDialog.dismissWithAnimation();
                                    }
                                })
                                .show();
                    }
                } catch (JSONException e) {
                    cartContainer.setVisibility(View.GONE);
                    noContent.setVisibility(View.VISIBLE);
                    Log.e("ERROR", e.toString());
                    hideProgressDialog();
                    new SweetAlertDialog(GroupCartContinueActivity.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Network Error")
                            .setContentText("Please check your internet connection.")
                            .setConfirmText("Settings")
                            .setCancelText("Okay")
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();
                                    startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 1);
                                }
                            })
                            .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();
                                }
                            })
                            .show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                cartContainer.setVisibility(View.GONE);
                noContent.setVisibility(View.VISIBLE);
                Log.e("ERROR", error.toString());
                hideProgressDialog();
                new SweetAlertDialog(GroupCartContinueActivity.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Network Error")
                        .setContentText("Please check your internet connection.")
                        .setConfirmText("Settings")
                        .setCancelText("Okay")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();
                                startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 1);
                            }
                        })
                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();
                            }
                        })
                        .show();
            }
        });
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(50 * 1000, 3,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        // Access the RequestQueue through your singleton class.
        MySingleton.getInstance(this).addToRequestQueue(jsObjRequest);
    }
    Dialog mDialog;
    private void showProgressDialog(){
        mDialog = new Dialog(this);
        mDialog.setCancelable(false);
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        mDialog.setContentView(R.layout.custom_progress_dialog);
        mDialog.show();
    }
    private void hideProgressDialog(){
        mDialog.dismiss();
    }
    public void openmenu(View view) {
        drawer.openDrawer(Gravity.START);
    }
    public void order(View view){
        if(is_logged_in){
            MethodClass.go_to_next_activity(this, CustomerOrderActivity.class);
        }else{
            MethodClass.go_to_next_activity(this, LoginActivity.class);
        }
    }
    public void edit(View view){
        if(is_logged_in){
            MethodClass.go_to_next_activity(this, CustomerEditProfileActivity.class);
        }else{
            MethodClass.go_to_next_activity(this, LoginActivity.class);
        }
    }
    public void refer(View view){
        if(is_logged_in){
            MethodClass.go_to_next_activity(this, ReferralActivity.class);
        }else{
            MethodClass.go_to_next_activity(this, LoginActivity.class);
        }

    }
    public void cartshop(View view){
        if(is_logged_in){
            MethodClass.go_to_next_activity(this, CartActivity.class);
        }else{
            MethodClass.go_to_next_activity(this, LoginActivity.class);
        }


    }
    public void signup(View view){
        MethodClass.go_to_next_activity(this, SignUpActivity.class);
    }
    public void login(View view){
        MethodClass.go_to_next_activity(this, LoginActivity.class);
    }
    public void btmLogin(View view){
        MethodClass.go_to_next_activity(this, LoginActivity.class);
    }
    public void browse(View view){
        MethodClass.go_to_next_activity(this, BrowseActivity.class);
    }
    public void home(View view){
        MethodClass.go_to_next_activity(this, HomeActivity.class);
    }
    public void shoppingcart(View view){
        MethodClass.go_to_next_activity(this, ShoppingCartActivity.class);
    }
    public void help(View view){
        MethodClass.go_to_next_activity(this, HelperActivity.class);
    }
    public void account(View view){
        if(is_logged_in){
            MethodClass.go_to_next_activity(this, AccountActivity.class);
        }else{
            MethodClass.go_to_next_activity(this, LoginActivity.class);
        }
    }
    public void btmLogout(View view){
        Intent intent=new Intent(this, SplashActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
        Map<String,?> prefs = PreferenceManager.getDefaultSharedPreferences(this).getAll();
        for(Map.Entry<String,?> prefToReset : prefs.entrySet()){

            if(!prefToReset.getKey().equals("first_time")){
                PreferenceManager.getDefaultSharedPreferences(this).edit().remove(prefToReset.getKey()).commit();
            }
        }
        startActivity(intent);
    }
    public void logout(View view){
        Intent intent=new Intent(this, SplashActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
        Map<String,?> prefs = PreferenceManager.getDefaultSharedPreferences(this).getAll();
        for(Map.Entry<String,?> prefToReset : prefs.entrySet()){

            if(!prefToReset.getKey().equals("first_time")){
                PreferenceManager.getDefaultSharedPreferences(this).edit().remove(prefToReset.getKey()).commit();
            }
        }
        startActivity(intent);
    }
    public void addressbook(View view){
        if(is_logged_in){
            MethodClass.go_to_next_activity(this, AddressBookActivity.class);
        }else{
            MethodClass.go_to_next_activity(this, LoginActivity.class);
        }
    }
    public void yourItem(View view){
        if(is_logged_in){
            MethodClass.go_to_next_activity(this, YourItemsActivity.class);
        }else{
            MethodClass.go_to_next_activity(this, LoginActivity.class);
        }
    }
    public void show_edit(View view){
        if(is_logged_in){
            MethodClass.go_to_next_activity(this, CustomerProfileShowActivity.class);
        }else{
            MethodClass.go_to_next_activity(this, LoginActivity.class);
        }
    }
    @Override
    protected void onResume() {
        super.onResume();
        Common.set_nav_wallet_bal(GroupCartContinueActivity.this);
        LinearLayout signin = (LinearLayout) headerView.findViewById(R.id.signin);
        LinearLayout signup = (LinearLayout) headerView.findViewById(R.id.signup);
        LinearLayout signout = (LinearLayout) headerView.findViewById(R.id.signout);
        LinearLayout login_layout=(LinearLayout)findViewById(R.id.login_layout);
        LinearLayout logout_layout=(LinearLayout)findViewById(R.id.logout_layout);
        cart_count = (TextView) findViewById(R.id.cart_count);
        if(is_logged_in){
            login_layout.setVisibility(View.GONE);
            logout_layout.setVisibility(View.VISIBLE);
            signin.setVisibility(View.GONE);
            signup.setVisibility(View.GONE);
            signout.setVisibility(View.VISIBLE);
            nav_name_tv.setText(PreferenceManager.getDefaultSharedPreferences(this).getString("fname","")+" "+PreferenceManager.getDefaultSharedPreferences(this).getString("lname",""));
            email_head.setText(PreferenceManager.getDefaultSharedPreferences(this).getString("email",""));
            Picasso.with(this)
                    .load(PROFILE_IMG_URL+PreferenceManager.getDefaultSharedPreferences(this).getString("profile_image",""))
                    .placeholder(R.drawable.ic_user) //this is optional the image to display while the url image is downloading
                    .error(R.drawable.ic_user)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                    .into(imgPro);
            if(!PreferenceManager.getDefaultSharedPreferences(this).getString("cart","").equals("") && !PreferenceManager.getDefaultSharedPreferences(this).getString("cart","").equals("null") && !PreferenceManager.getDefaultSharedPreferences(this).getString("cart","").equals(null)){
                cart_count.setText(PreferenceManager.getDefaultSharedPreferences(this).getString("cart",""));
                cart_count.setVisibility(View.VISIBLE);
            }else {
                cart_count.setVisibility(View.GONE);
            }
        }else{
            login_layout.setVisibility(View.VISIBLE);
            logout_layout.setVisibility(View.GONE);
            signin.setVisibility(View.VISIBLE);
            signup.setVisibility(View.VISIBLE);
            signout.setVisibility(View.GONE);
            nav_name_tv.setText("Hi, user");
            email_head.setText("");
        }


    }
}
