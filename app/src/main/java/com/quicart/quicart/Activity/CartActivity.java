package com.quicart.quicart.Activity;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.ColorDrawable;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.quicart.quicart.Adapter.GroupCartListAdapter;
import com.quicart.quicart.Helper.Common;
import com.quicart.quicart.Helper.MethodClass;
import com.quicart.quicart.Helper.MySingleton;
import com.quicart.quicart.R;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.quicart.quicart.Helper.Constants.CART_ID;
import static com.quicart.quicart.Helper.Constants.CART_MEMBER;
import static com.quicart.quicart.Helper.Constants.CART_NAME;
import static com.quicart.quicart.Helper.Constants.OWNER_ID;
import static com.quicart.quicart.Helper.Constants.PROFILE_IMG_URL;


public class CartActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener  {

    private LinearLayout updatepro;
    private NavigationView navigationView;
    private DrawerLayout drawer;
    private ImageView menu_iv;
    private RecyclerView group_rv;
    private ArrayList<HashMap<String, String>> groupList;
    private LinearLayout personal_cart;
    private Button add_card;
    private ImageView cart;
    Boolean is_logged_in;
    private TextView nav_name_tv,email_head,prsonalCart, noti_count;
    private String user_id = "";
    private ImageView imgPro;
    private View headerView;
    private LinearLayout noContent;
    private RelativeLayout notiLayout;
    private TextView cart_count;
    private ImageView noti;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_cart);
        MethodClass.hide_keyboard(CartActivity.this);
        is_logged_in = PreferenceManager.getDefaultSharedPreferences(CartActivity.this).getBoolean("is_logged_in",false);
        user_id = PreferenceManager.getDefaultSharedPreferences(CartActivity.this).getString("user_id","");
        group_rv=(RecyclerView) findViewById(R.id.group_rv);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        personal_cart=(LinearLayout)findViewById(R.id.personal_cart);
        prsonalCart=(TextView) findViewById(R.id.prsonalCart);
        add_card=(Button) findViewById(R.id.add_card);
        cart=(ImageView)findViewById(R.id.cart);
        cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MethodClass.go_to_next_activity(CartActivity.this,ShoppingCartActivity.class);
            }
        });
        noti_count = (TextView) findViewById(R.id.noti_count);
        notiLayout = (RelativeLayout) findViewById(R.id.notiLayout);
        noti = (ImageView) findViewById(R.id.noti);
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        headerView = navigationView.getHeaderView(0);
        nav_name_tv=(TextView) headerView.findViewById(R.id.nav_name_tv);
        email_head=(TextView) headerView.findViewById(R.id.email);
        imgPro=(ImageView) headerView.findViewById(R.id.imgPro);
        updatepro=(LinearLayout) headerView.findViewById(R.id.updatepro);
        if(is_logged_in){
            updatepro.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    MethodClass.go_to_next_activity(CartActivity.this,CustomerEditProfileActivity.class);
                }
            });
        }
        navigationView.setNavigationItemSelectedListener(this);
        menu_iv=(ImageView)findViewById(R.id.img_navbtn);
        menu_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer.openDrawer(Gravity.START);
            }
        });
        noContent=(LinearLayout)findViewById(R.id.noContent);
        LinearLayout login_layout=(LinearLayout)findViewById(R.id.login_layout);
        LinearLayout logout_layout=(LinearLayout)findViewById(R.id.logout_layout);
        if(is_logged_in){
            login_layout.setVisibility(View.GONE);
            logout_layout.setVisibility(View.VISIBLE);
        }else{
            login_layout.setVisibility(View.VISIBLE);
            logout_layout.setVisibility(View.GONE);
        }
        personal_cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MethodClass.go_to_next_activity(CartActivity.this, ShoppingCartActivity.class);
            }
        });
        add_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String server_url = getString(R.string.SERVER_URL)+"check-group-cart";
                final String token = getString(R.string.AUTH_KEY);
                HashMap<String, String> params1 = new HashMap<String, String>();
                params1.put("secret_key", token);
                params1.put("owner_id", user_id);
                Log.e("avv", new JSONObject(params1).toString());
                showProgressDialog();
                JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, server_url, new JSONObject(params1), new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("ERROR", response.toString());
                        //Toast.makeText(BussinessSignupActivity.this, response.toString(), Toast.LENGTH_LONG).show();
                        try {
                            hideProgressDialog();
                            String status = response.getString("status");
                            //Toast.makeText(BussinessSignupActivity.this, status, Toast.LENGTH_LONG).show();
                            if (status.equals("SUCCESS")) {
                                MethodClass.go_to_next_activity(CartActivity.this,GroupCartActivity.class);
                            }else if(status.equals("OVER")){
                                new SweetAlertDialog(CartActivity.this, SweetAlertDialog.ERROR_TYPE)
                                        .setTitleText("Upgrade Membership")
                                        .setContentText("Please upgrade your membership to add more group cart.")
                                        .setConfirmText("Upgrade")
                                        .setCancelText("Not Now")
                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sDialog) {
                                                sDialog.dismissWithAnimation();
                                                MethodClass.go_to_next_activity(CartActivity.this,SubscriptionActivity.class);
                                            }
                                        })
                                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sDialog) {
                                                sDialog.dismissWithAnimation();
                                            }
                                        })
                                        .show();
                            }else {
                                new SweetAlertDialog(CartActivity.this, SweetAlertDialog.ERROR_TYPE)
                                        .setTitleText("Network Error")
                                        .setContentText("Please check your internet connection.")
                                        .setConfirmText("Settings")
                                        .setCancelText("Okay")
                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sDialog) {
                                                sDialog.dismissWithAnimation();
                                                startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 1);
                                            }
                                        })
                                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sDialog) {
                                                sDialog.dismissWithAnimation();
                                            }
                                        })
                                        .show();
                            }
                        } catch (JSONException e) {
                            Log.e("ERROR", e.toString());
                            hideProgressDialog();
                            new SweetAlertDialog(CartActivity.this, SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText("Network Error")
                                    .setContentText("Please check your internet connection.")
                                    .setConfirmText("Settings")
                                    .setCancelText("Okay")
                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sDialog) {
                                            sDialog.dismissWithAnimation();
                                            startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 1);
                                        }
                                    })
                                    .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sDialog) {
                                            sDialog.dismissWithAnimation();
                                        }
                                    })
                                    .show();
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("ERROR", error.toString());
                        hideProgressDialog();
                        new SweetAlertDialog(CartActivity.this, SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("Network Error")
                                .setContentText("Please check your internet connection.")
                                .setConfirmText("Settings")
                                .setCancelText("Okay")
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sDialog) {
                                        sDialog.dismissWithAnimation();
                                        startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 1);
                                    }
                                })
                                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sDialog) {
                                        sDialog.dismissWithAnimation();
                                    }
                                })
                                .show();
                    }
                });
                jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(50 * 1000, 3,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                // Access the RequestQueue through your singleton class.
                MySingleton.getInstance(CartActivity.this).addToRequestQueue(jsObjRequest);

               /* final Dialog dialog = new Dialog(CartActivity.this);
                dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.group_cart_popup);
                ImageView close = (ImageView) dialog.findViewById(R.id.close);
                Button save = (Button) dialog.findViewById(R.id.save);
                Button cancel = (Button) dialog.findViewById(R.id.cancel);
                close.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });
                save.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        MethodClass.go_to_next_activity(CartActivity.this, GroupCartActivity.class);
                    }
                });
                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });
                dialog.show();*/

            }
        });
        notiLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (is_logged_in) {
                    Intent I = new Intent(CartActivity.this, CustomerNotificationActivity.class);
                    startActivityForResult(I, 1);
                } else {

                }

            }
        });

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        return false;
    }

    public void groupcart(){
        String server_url = getString(R.string.SERVER_URL)+"cart";
        final String token = getString(R.string.AUTH_KEY);
        HashMap<String, String> params1 = new HashMap<String, String>();
        params1.put("secret_key", token);
        params1.put("customer_id", user_id);
        Log.e("avv", new JSONObject(params1).toString());
        showProgressDialog();
        JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, server_url, new JSONObject(params1), new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.e("ERROR", response.toString());
                //Toast.makeText(BussinessSignupActivity.this, response.toString(), Toast.LENGTH_LONG).show();
                try {
                    hideProgressDialog();
                    String status = response.getString("status");
                    //Toast.makeText(BussinessSignupActivity.this, status, Toast.LENGTH_LONG).show();
                    if (status.equals("SUCCESS")) {
                        String user_data = response.getString("data");
                        JSONObject datObj = new JSONObject(user_data);
                        String personal_cart = datObj.getString("personal_cart");
                        String group_cart_own = datObj.getString("group_cart_own");
                        String group_cart_other = datObj.getString("group_cart_other");
                        if(!personal_cart.equals("null") && !personal_cart.equals(null)){
                            JSONObject prCart = new JSONObject(personal_cart);
                            String cart_name = prCart.getString("cart_name");
                            String no_of_user = prCart.getString("no_of_user");
                            String no_of_product = prCart.getString("no_of_product");
                            prsonalCart.setText(no_of_product+" Items");
                        }
                        JSONArray prmARR = new JSONArray(group_cart_own);
                        JSONArray prmARR2 = new JSONArray(group_cart_other);
                        if(prmARR.length()>0){
                            groupList=new ArrayList<HashMap<String, String>>();
                            for (int i = 0; i <prmARR.length() ; i++) {
                                JSONObject proOBJ = prmARR.getJSONObject(i);
                                String name = proOBJ.getString("cart_name");
                                String id = proOBJ.getString("id");
                                String owner_id = proOBJ.getString("owner_id");
                                String no_of_users = proOBJ.getString("no_of_user");

                                HashMap<String,String> map=new HashMap<String, String>();
                                if(i == prmARR.length()-1){
                                    for (int j = 0; j <prmARR2.length() ; j++) {
                                    HashMap<String,String> map2=new HashMap<String, String>();
                                    JSONObject proOBJ2 = prmARR2.getJSONObject(j);
                                    String names = proOBJ2.getString("cart_name");
                                    String ids = proOBJ2.getString("id");
                                    String owner_ids = proOBJ2.getString("owner_id");
                                    String no_of_userss = proOBJ2.getString("no_of_user");
                                    map2.put(CART_NAME,names);
                                    map2.put(CART_ID,ids);
                                    map2.put(CART_MEMBER,no_of_userss);
                                    map2.put(OWNER_ID,owner_ids);
                                    groupList.add(map2);
                                }
                            }
                                map.put(CART_NAME,name);
                                map.put(CART_ID,id);
                                map.put(OWNER_ID,owner_id);
                                map.put(CART_MEMBER,no_of_users);
                                groupList.add(map);
                            }
                            GroupCartListAdapter adapter = new GroupCartListAdapter(CartActivity.this, groupList);
                            group_rv.setAdapter(adapter);
                            group_rv.setVisibility(View.VISIBLE);
                            noContent.setVisibility(View.GONE);
                        }else if(prmARR2.length()>0){
                            groupList=new ArrayList<HashMap<String, String>>();
                            for (int j = 0; j <prmARR2.length() ; j++) {
                                HashMap<String,String> map2=new HashMap<String, String>();
                                JSONObject proOBJ2 = prmARR2.getJSONObject(j);
                                String names = proOBJ2.getString("cart_name");
                                String ids = proOBJ2.getString("id");
                                String owner_ids = proOBJ2.getString("owner_id");
                                String no_of_userss = proOBJ2.getString("no_of_user");
                                map2.put(CART_NAME,names);
                                map2.put(CART_ID,ids);
                                map2.put(CART_MEMBER,no_of_userss);
                                map2.put(OWNER_ID,owner_ids);
                                groupList.add(map2);
                            }
                            GroupCartListAdapter adapter = new GroupCartListAdapter(CartActivity.this, groupList);
                            group_rv.setAdapter(adapter);
                            group_rv.setVisibility(View.VISIBLE);
                            noContent.setVisibility(View.GONE);
                        } else{
                            group_rv.setVisibility(View.GONE);
                            noContent.setVisibility(View.VISIBLE);
                        }

                    }else {
                        group_rv.setVisibility(View.GONE);
                        noContent.setVisibility(View.VISIBLE);
                        new SweetAlertDialog(CartActivity.this, SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("Network Error")
                                .setContentText("Please check your internet connection.")
                                .setConfirmText("Settings")
                                .setCancelText("Okay")
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sDialog) {
                                        sDialog.dismissWithAnimation();
                                        startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 1);
                                    }
                                })
                                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sDialog) {
                                        sDialog.dismissWithAnimation();
                                    }
                                })
                                .show();
                    }
                } catch (JSONException e) {
                    group_rv.setVisibility(View.GONE);
                    noContent.setVisibility(View.VISIBLE);
                    Log.e("ERROR", e.toString());
                    hideProgressDialog();
                    new SweetAlertDialog(CartActivity.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Network Error")
                            .setContentText("Please check your internet connection.")
                            .setConfirmText("Settings")
                            .setCancelText("Okay")
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();
                                    startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 1);
                                }
                            })
                            .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();
                                }
                            })
                            .show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                group_rv.setVisibility(View.GONE);
                noContent.setVisibility(View.VISIBLE);
                Log.e("ERROR", error.toString());
                hideProgressDialog();
                new SweetAlertDialog(CartActivity.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Network Error")
                        .setContentText("Please check your internet connection.")
                        .setConfirmText("Settings")
                        .setCancelText("Okay")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();
                                startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 1);
                            }
                        })
                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();
                            }
                        })
                        .show();
            }
        });
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(50 * 1000, 3,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        // Access the RequestQueue through your singleton class.
        MySingleton.getInstance(this).addToRequestQueue(jsObjRequest);
    }
    Dialog mDialog;
    private void showProgressDialog(){
        mDialog = new Dialog(this);
        mDialog.setCancelable(false);
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        mDialog.setContentView(R.layout.custom_progress_dialog);
        mDialog.show();
    }
    private void hideProgressDialog(){
        mDialog.dismiss();
    }

    public void openmenu(View view) {
        drawer.openDrawer(Gravity.START);
    }
    public void order(View view){
        if(is_logged_in){
            MethodClass.go_to_next_activity(this, CustomerOrderActivity.class);
        }else{
            MethodClass.go_to_next_activity(this, LoginActivity.class);
        }
    }
    public void edit(View view){
        if(is_logged_in){
            MethodClass.go_to_next_activity(this, CustomerEditProfileActivity.class);
        }else{
            MethodClass.go_to_next_activity(this, LoginActivity.class);
        }
    }
    public void refer(View view){
        if(is_logged_in){
            MethodClass.go_to_next_activity(this, ReferralActivity.class);
        }else{
            MethodClass.go_to_next_activity(this, LoginActivity.class);
        }

    }
    public void cartshop(View view){
        if(is_logged_in){
            MethodClass.go_to_next_activity(this, CartActivity.class);
        }else{
            MethodClass.go_to_next_activity(this, LoginActivity.class);
        }


    }
    public void signup(View view){
        MethodClass.go_to_next_activity(this, SignUpActivity.class);
    }
    public void login(View view){
        MethodClass.go_to_next_activity(this, LoginActivity.class);
    }
    public void btmLogin(View view){
        MethodClass.go_to_next_activity(this, LoginActivity.class);
    }
    public void browse(View view){
        MethodClass.go_to_next_activity(this, BrowseActivity.class);
    }
    public void home(View view){
        MethodClass.go_to_next_activity(this, HomeActivity.class);
    }
    public void shoppingcart(View view){
        MethodClass.go_to_next_activity(this, ShoppingCartActivity.class);
    }
    public void help(View view){
        MethodClass.go_to_next_activity(this, HelperActivity.class);
    }
    public void account(View view){
        if(is_logged_in){
            MethodClass.go_to_next_activity(this, AccountActivity.class);
        }else{
            MethodClass.go_to_next_activity(this, LoginActivity.class);
        }
    }
    public void btmLogout(View view){
        Intent intent=new Intent(this, SplashActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
        Map<String,?> prefs = PreferenceManager.getDefaultSharedPreferences(this).getAll();
        for(Map.Entry<String,?> prefToReset : prefs.entrySet()){

            if(!prefToReset.getKey().equals("first_time")){
                PreferenceManager.getDefaultSharedPreferences(this).edit().remove(prefToReset.getKey()).commit();
            }
        }
        startActivity(intent);
    }
    public void logout(View view){
        Intent intent=new Intent(this, SplashActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
        Map<String,?> prefs = PreferenceManager.getDefaultSharedPreferences(this).getAll();
        for(Map.Entry<String,?> prefToReset : prefs.entrySet()){

            if(!prefToReset.getKey().equals("first_time")){
                PreferenceManager.getDefaultSharedPreferences(this).edit().remove(prefToReset.getKey()).commit();
            }
        }
        startActivity(intent);
    }
    public void addressbook(View view){
        if(is_logged_in){
            MethodClass.go_to_next_activity(this, AddressBookActivity.class);
        }else{
            MethodClass.go_to_next_activity(this, LoginActivity.class);
        }
    }
    public void yourItem(View view){
        if(is_logged_in){
            MethodClass.go_to_next_activity(this, YourItemsActivity.class);
        }else{
            MethodClass.go_to_next_activity(this, LoginActivity.class);
        }
    }
    public void show_edit(View view){
        if(is_logged_in){
            MethodClass.go_to_next_activity(this, CustomerProfileShowActivity.class);
        }else{
            MethodClass.go_to_next_activity(this, LoginActivity.class);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Common.set_nav_wallet_bal(CartActivity.this);
        groupcart();
        LinearLayout signin = (LinearLayout) headerView.findViewById(R.id.signin);
        LinearLayout signup = (LinearLayout) headerView.findViewById(R.id.signup);
        LinearLayout signout = (LinearLayout) headerView.findViewById(R.id.signout);
        LinearLayout login_layout=(LinearLayout)findViewById(R.id.login_layout);
        LinearLayout logout_layout=(LinearLayout)findViewById(R.id.logout_layout);
        cart_count = (TextView) findViewById(R.id.cart_count);
        if(is_logged_in){
            login_layout.setVisibility(View.GONE);
            logout_layout.setVisibility(View.VISIBLE);
            signin.setVisibility(View.GONE);
            signup.setVisibility(View.GONE);
            signout.setVisibility(View.VISIBLE);
            nav_name_tv.setText(PreferenceManager.getDefaultSharedPreferences(this).getString("fname","")+" "+PreferenceManager.getDefaultSharedPreferences(this).getString("lname",""));
            email_head.setText(PreferenceManager.getDefaultSharedPreferences(this).getString("email",""));
            Picasso.with(this)
                    .load(PROFILE_IMG_URL+PreferenceManager.getDefaultSharedPreferences(this).getString("profile_image",""))
                    .placeholder(R.drawable.ic_user) //this is optional the image to display while the url image is downloading
                    .error(R.drawable.ic_user)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                    .into(imgPro);
            if(!PreferenceManager.getDefaultSharedPreferences(this).getString("cart","").equals("") && !PreferenceManager.getDefaultSharedPreferences(this).getString("cart","").equals("null") && !PreferenceManager.getDefaultSharedPreferences(this).getString("cart","").equals(null)){
                cart_count.setText(PreferenceManager.getDefaultSharedPreferences(this).getString("cart",""));
                cart_count.setVisibility(View.VISIBLE);
            }else {
                cart_count.setVisibility(View.GONE);
            }
            String cus_req_count = PreferenceManager.getDefaultSharedPreferences(this).getString("new_request_count", "0");
            if (!cus_req_count.equals("0")) {
                noti_count.setText(cus_req_count);
                noti_count.setVisibility(View.VISIBLE);
            } else {
                noti_count.setVisibility(View.GONE);
            }

            LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver,
                    new IntentFilter("updateNoti"));

        }else{
            login_layout.setVisibility(View.VISIBLE);
            logout_layout.setVisibility(View.GONE);
            signin.setVisibility(View.VISIBLE);
            signup.setVisibility(View.VISIBLE);
            signout.setVisibility(View.GONE);
            nav_name_tv.setText("Hi, user");
            email_head.setText("");
        }


    }
    @Override
    protected void onPause() {
        // Unregister since the activity is not visible
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
        super.onPause();
    }

    // handler for received Intents for the "my-event" event
    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Extract data included in the Intent
            String users_id = getIntent().getStringExtra("user_id");
            String cus_req_count = PreferenceManager.getDefaultSharedPreferences(CartActivity.this).getString("new_request_count", "0");
            if (user_id.equals(users_id)) {
                if (!cus_req_count.equals("0")) {
                    noti_count.setText(cus_req_count);
                    noti_count.setVisibility(View.VISIBLE);
                } else {
                    noti_count.setVisibility(View.GONE);
                }
            }

        }
    };
}
