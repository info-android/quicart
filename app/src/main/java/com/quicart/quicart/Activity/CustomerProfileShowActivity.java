package com.quicart.quicart.Activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.quicart.quicart.Helper.MethodClass;
import com.quicart.quicart.R;


public class CustomerProfileShowActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_customer_profile_show);
    }
    public void back(View view) {
        super.onBackPressed();
    }
    public void edit(View view){
        MethodClass.go_to_next_activity(this, CustomerEditProfileActivity.class);
    }

}
