package com.quicart.quicart.Activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.quicart.quicart.R;

import static com.quicart.quicart.Helper.Constants.EMAIL_PATTERN;


public class MyAccountActivity extends AppCompatActivity {
    private EditText edt_f_name,edt_l_name,edt_ph_no,edt_email;
    private TextView f_name_hint_tv,l_name_hint_tv,ph_no_hint_tv,email_hint_tv;
    private TextView f_name_error_tv,l_name_error_tv,ph_no_error_tv,email_error_tv,chnge_pswrd_tv;
    private Button save_btn;

    private String f_name,l_name,email,ph_no;
    private ImageView back_btn;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_my_account);

        edt_f_name=(EditText)findViewById(R.id.edt_f_name);
        edt_l_name=(EditText)findViewById(R.id.edt_l_name);
        edt_ph_no=(EditText)findViewById(R.id.edt_ph_no);
        edt_email=(EditText)findViewById(R.id.edt_email);

        f_name_hint_tv=(TextView) findViewById(R.id.f_name_hint_tv);
        l_name_hint_tv=(TextView) findViewById(R.id.l_name_hint_tv);
        ph_no_hint_tv=(TextView) findViewById(R.id.ph_no_hint_tv);
        email_hint_tv=(TextView) findViewById(R.id.email_hint_tv);

        f_name_error_tv=(TextView) findViewById(R.id.f_name_error_tv);
        l_name_error_tv=(TextView) findViewById(R.id.l_name_error_tv);
        ph_no_error_tv=(TextView) findViewById(R.id.ph_no_error_tv);
        email_error_tv=(TextView) findViewById(R.id.email_error_tv);
        chnge_pswrd_tv=(TextView) findViewById(R.id.chnge_pswrd_tv);

        back_btn=(ImageView) findViewById(R.id.back_btn);

        save_btn=(Button) findViewById(R.id.save_btn);

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                finish();
            }
        });

        save_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                f_name=edt_f_name.getText().toString().trim();
                l_name=edt_l_name.getText().toString().trim();
                email=edt_email.getText().toString().trim();
                ph_no=edt_ph_no.getText().toString().trim();
                if (f_name.length()==0){
                    error_condition_visible_only(f_name_error_tv,"Please enter first name",edt_f_name,f_name_hint_tv);
                    return;

                }
                if (l_name.length()==0){
                    error_condition_visible_only(l_name_error_tv,"Please enter last name",edt_l_name,l_name_hint_tv);
                    return;
                }
                if (ph_no.length()==0){
                    error_condition_visible_only(ph_no_error_tv,"Please enter phone number",edt_ph_no,ph_no_hint_tv);
                    return;
                } if (ph_no.length()>10){
                    error_condition_visible_only(ph_no_error_tv,"Please 10 digit phone number",edt_ph_no,ph_no_hint_tv);
                    return;
                }
                if (email.length()==0){
                    error_condition_visible_only(email_error_tv,"Please enter email address",edt_email,email_hint_tv);
                    return;
                } if (!email.matches(EMAIL_PATTERN)){
                    error_condition_visible_only(email_error_tv,"Please enter valid email address",edt_email,email_hint_tv);
                    return;
                }
            }
        });



    }

    private void error_condition_visible_only(TextView textView, String msg,EditText editText,TextView hint_tv){

        f_name_error_tv.setVisibility(View.GONE);
        l_name_error_tv.setVisibility(View.GONE);
        email_error_tv.setVisibility(View.GONE);
        ph_no_error_tv.setVisibility(View.GONE);

        textView.setVisibility(View.VISIBLE);
        textView.setText(msg);

        f_name_hint_tv.setTextColor(getResources().getColor(R.color.black));
        l_name_hint_tv.setTextColor(getResources().getColor(R.color.black));
        ph_no_hint_tv.setTextColor(getResources().getColor(R.color.black));
        email_hint_tv.setTextColor(getResources().getColor(R.color.black));
        hint_tv.setTextColor(getResources().getColor(R.color.violet));

        edt_f_name.setBackgroundResource(R.drawable.edt_bg);
        edt_l_name.setBackgroundResource(R.drawable.edt_bg);
        edt_ph_no.setBackgroundResource(R.drawable.edt_bg);
        edt_email.setBackgroundResource(R.drawable.edt_bg);
        editText.setBackgroundResource(R.drawable.edt_error_bg);
    }

    private void close_error_msg(final EditText editText){

        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (editText.getText().toString().trim().length()==0){

                }

            }
        });
    }
}
