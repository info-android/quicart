package com.quicart.quicart.Activity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Switch;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.quicart.quicart.Adapter.Category_Notification_Adapter;
import com.quicart.quicart.Helper.MySingleton;
import com.quicart.quicart.R;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import static com.quicart.quicart.Helper.Constants.DATE;
import static com.quicart.quicart.Helper.Constants.SHOPPER_DESC;
import static com.quicart.quicart.Helper.Constants.SHOPPER_ID;
import static com.quicart.quicart.Helper.Constants.SHOPPER_NAME;



public class NotificationSettingsActivity extends AppCompatActivity {
    private ImageView back_btn;
    private Switch promoNoti;
    private Switch emailNoti;
    private Switch msgNoti;
    private Switch pushNoti;
    private RecyclerView rv_MyShopper;
    private ArrayList<HashMap<String, String>> orderlist;
    private String user_id = "";
    private Integer first = 1;

    private RadioButton mspan;
    private RadioButton wspan;
    private RadioButton otspan;
    private RadioGroup span;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_notification_settings);
        back_btn=(ImageView)findViewById(R.id.back_btn);
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                finish();
            }
        });
        user_id = PreferenceManager.getDefaultSharedPreferences(NotificationSettingsActivity.this).getString("user_id","");
        promoNoti = (Switch) findViewById(R.id.promoNoti);
        emailNoti = (Switch) findViewById(R.id.emailNoti);
        msgNoti = (Switch) findViewById(R.id.msgNoti);
        pushNoti = (Switch) findViewById(R.id.pushNoti);
        rv_MyShopper = (RecyclerView) findViewById(R.id.rv_MyShopper);

        getNoti();

        pushNoti.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(first >2) {
                    if(isChecked){
                        String server_url = getString(R.string.SERVER_URL)+"send-notification-update";
                        final String token = getString(R.string.AUTH_KEY);
                        HashMap<String, String> params1 = new HashMap<String, String>();
                        params1.put("secret_key", token);
                        params1.put("id", user_id);
                        params1.put("send_notification", "Y");

                        Log.e("avv", new JSONObject(params1).toString());
                        showProgressDialog();
                        JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, server_url, new JSONObject(params1), new Response.Listener<JSONObject>() {

                            @Override
                            public void onResponse(JSONObject response) {
                                Log.e("ERROR", response.toString());
                                //Toast.makeText(BussinessSignupActivity.this, response.toString(), Toast.LENGTH_LONG).show();
                                try {
                                    hideProgressDialog();
                                    String status = response.getString("status");
                                    //Toast.makeText(BussinessSignupActivity.this, status, Toast.LENGTH_LONG).show();
                                    if (status.equals("SUCCESS")) {


                                    }else {

                                        new SweetAlertDialog(NotificationSettingsActivity.this, SweetAlertDialog.ERROR_TYPE)
                                                .setTitleText("Network Error")
                                                .setContentText("Please check your internet connection.")
                                                .setConfirmText("Settings")
                                                .setCancelText("Okay")
                                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                    @Override
                                                    public void onClick(SweetAlertDialog sDialog) {
                                                        sDialog.dismissWithAnimation();
                                                        startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 1);
                                                    }
                                                })
                                                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                    @Override
                                                    public void onClick(SweetAlertDialog sDialog) {
                                                        sDialog.dismissWithAnimation();
                                                    }
                                                })
                                                .show();
                                    }
                                } catch (JSONException e) {

                                    Log.e("ERROR", e.toString());
                                    hideProgressDialog();
                                    new SweetAlertDialog(NotificationSettingsActivity.this, SweetAlertDialog.ERROR_TYPE)
                                            .setTitleText("Network Error")
                                            .setContentText("Please check your internet connection.")
                                            .setConfirmText("Settings")
                                            .setCancelText("Okay")
                                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                @Override
                                                public void onClick(SweetAlertDialog sDialog) {
                                                    sDialog.dismissWithAnimation();
                                                    startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 1);
                                                }
                                            })
                                            .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                @Override
                                                public void onClick(SweetAlertDialog sDialog) {
                                                    sDialog.dismissWithAnimation();
                                                }
                                            })
                                            .show();
                                }
                            }
                        }, new Response.ErrorListener() {

                            @Override
                            public void onErrorResponse(VolleyError error) {

                                Log.e("ERROR", error.toString());
                                hideProgressDialog();
                                new SweetAlertDialog(NotificationSettingsActivity.this, SweetAlertDialog.ERROR_TYPE)
                                        .setTitleText("Network Error")
                                        .setContentText("Please check your internet connection.")
                                        .setConfirmText("Settings")
                                        .setCancelText("Okay")
                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sDialog) {
                                                sDialog.dismissWithAnimation();
                                                startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 1);
                                            }
                                        })
                                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sDialog) {
                                                sDialog.dismissWithAnimation();
                                            }
                                        })
                                        .show();
                            }
                        });
                        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(50 * 1000, 3,
                                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                        // Access the RequestQueue through your singleton class.
                        MySingleton.getInstance(NotificationSettingsActivity.this).addToRequestQueue(jsObjRequest);
                    }else {
                        String server_url = getString(R.string.SERVER_URL)+"send-notification-update";
                        final String token = getString(R.string.AUTH_KEY);
                        HashMap<String, String> params1 = new HashMap<String, String>();
                        params1.put("secret_key", token);
                        params1.put("id", user_id);
                        params1.put("send_notification", "N");

                        Log.e("avv", new JSONObject(params1).toString());
                        showProgressDialog();
                        JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, server_url, new JSONObject(params1), new Response.Listener<JSONObject>() {

                            @Override
                            public void onResponse(JSONObject response) {
                                Log.e("ERROR", response.toString());
                                //Toast.makeText(BussinessSignupActivity.this, response.toString(), Toast.LENGTH_LONG).show();
                                try {
                                    hideProgressDialog();
                                    String status = response.getString("status");
                                    //Toast.makeText(BussinessSignupActivity.this, status, Toast.LENGTH_LONG).show();
                                    if (status.equals("SUCCESS")) {

                                    }else {

                                        new SweetAlertDialog(NotificationSettingsActivity.this, SweetAlertDialog.ERROR_TYPE)
                                                .setTitleText("Network Error")
                                                .setContentText("Please check your internet connection.")
                                                .setConfirmText("Settings")
                                                .setCancelText("Okay")
                                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                    @Override
                                                    public void onClick(SweetAlertDialog sDialog) {
                                                        sDialog.dismissWithAnimation();
                                                        startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 1);
                                                    }
                                                })
                                                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                    @Override
                                                    public void onClick(SweetAlertDialog sDialog) {
                                                        sDialog.dismissWithAnimation();
                                                    }
                                                })
                                                .show();
                                    }
                                } catch (JSONException e) {

                                    Log.e("ERROR", e.toString());
                                    hideProgressDialog();
                                    new SweetAlertDialog(NotificationSettingsActivity.this, SweetAlertDialog.ERROR_TYPE)
                                            .setTitleText("Network Error")
                                            .setContentText("Please check your internet connection.")
                                            .setConfirmText("Settings")
                                            .setCancelText("Okay")
                                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                @Override
                                                public void onClick(SweetAlertDialog sDialog) {
                                                    sDialog.dismissWithAnimation();
                                                    startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 1);
                                                }
                                            })
                                            .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                @Override
                                                public void onClick(SweetAlertDialog sDialog) {
                                                    sDialog.dismissWithAnimation();
                                                }
                                            })
                                            .show();
                                }
                            }
                        }, new Response.ErrorListener() {

                            @Override
                            public void onErrorResponse(VolleyError error) {

                                Log.e("ERROR", error.toString());
                                hideProgressDialog();
                                new SweetAlertDialog(NotificationSettingsActivity.this, SweetAlertDialog.ERROR_TYPE)
                                        .setTitleText("Network Error")
                                        .setContentText("Please check your internet connection.")
                                        .setConfirmText("Settings")
                                        .setCancelText("Okay")
                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sDialog) {
                                                sDialog.dismissWithAnimation();
                                                startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 1);
                                            }
                                        })
                                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sDialog) {
                                                sDialog.dismissWithAnimation();
                                            }
                                        })
                                        .show();
                            }
                        });
                        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(50 * 1000, 3,
                                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                        // Access the RequestQueue through your singleton class.
                        MySingleton.getInstance(NotificationSettingsActivity.this).addToRequestQueue(jsObjRequest);
                    }
                }

            }
        });


        promoNoti.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(first >1) {
                    if(isChecked){

                        String server_url = getString(R.string.SERVER_URL)+"send-promotional-notification-update";
                        final String token = getString(R.string.AUTH_KEY);
                        HashMap<String, String> params1 = new HashMap<String, String>();
                        params1.put("secret_key", token);
                        params1.put("id", user_id);
                        params1.put("send_promotional", "Y");

                        Log.e("avv", new JSONObject(params1).toString());
                        showProgressDialog();
                        JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, server_url, new JSONObject(params1), new Response.Listener<JSONObject>() {

                            @Override
                            public void onResponse(JSONObject response) {
                                Log.e("ERROR", response.toString());
                                //Toast.makeText(BussinessSignupActivity.this, response.toString(), Toast.LENGTH_LONG).show();
                                try {
                                    hideProgressDialog();
                                    String status = response.getString("status");
                                    //Toast.makeText(BussinessSignupActivity.this, status, Toast.LENGTH_LONG).show();
                                    if (status.equals("SUCCESS")) {


                                    }else {

                                        new SweetAlertDialog(NotificationSettingsActivity.this, SweetAlertDialog.ERROR_TYPE)
                                                .setTitleText("Network Error")
                                                .setContentText("Please check your internet connection.")
                                                .setConfirmText("Settings")
                                                .setCancelText("Okay")
                                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                    @Override
                                                    public void onClick(SweetAlertDialog sDialog) {
                                                        sDialog.dismissWithAnimation();
                                                        startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 1);
                                                    }
                                                })
                                                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                    @Override
                                                    public void onClick(SweetAlertDialog sDialog) {
                                                        sDialog.dismissWithAnimation();
                                                    }
                                                })
                                                .show();
                                    }
                                } catch (JSONException e) {

                                    Log.e("ERROR", e.toString());
                                    hideProgressDialog();
                                    new SweetAlertDialog(NotificationSettingsActivity.this, SweetAlertDialog.ERROR_TYPE)
                                            .setTitleText("Network Error")
                                            .setContentText("Please check your internet connection.")
                                            .setConfirmText("Settings")
                                            .setCancelText("Okay")
                                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                @Override
                                                public void onClick(SweetAlertDialog sDialog) {
                                                    sDialog.dismissWithAnimation();
                                                    startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 1);
                                                }
                                            })
                                            .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                @Override
                                                public void onClick(SweetAlertDialog sDialog) {
                                                    sDialog.dismissWithAnimation();
                                                }
                                            })
                                            .show();
                                }
                            }
                        }, new Response.ErrorListener() {

                            @Override
                            public void onErrorResponse(VolleyError error) {

                                Log.e("ERROR", error.toString());
                                hideProgressDialog();
                                new SweetAlertDialog(NotificationSettingsActivity.this, SweetAlertDialog.ERROR_TYPE)
                                        .setTitleText("Network Error")
                                        .setContentText("Please check your internet connection.")
                                        .setConfirmText("Settings")
                                        .setCancelText("Okay")
                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sDialog) {
                                                sDialog.dismissWithAnimation();
                                                startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 1);
                                            }
                                        })
                                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sDialog) {
                                                sDialog.dismissWithAnimation();
                                            }
                                        })
                                        .show();
                            }
                        });
                        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(50 * 1000, 3,
                                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                        // Access the RequestQueue through your singleton class.
                        MySingleton.getInstance(NotificationSettingsActivity.this).addToRequestQueue(jsObjRequest);
                    }else {
                        String server_url = getString(R.string.SERVER_URL)+"send-promotional-notification-update";
                        final String token = getString(R.string.AUTH_KEY);
                        HashMap<String, String> params1 = new HashMap<String, String>();
                        params1.put("secret_key", token);
                        params1.put("id", user_id);
                        params1.put("send_promotional", "N");

                        Log.e("avv", new JSONObject(params1).toString());
                        showProgressDialog();
                        JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, server_url, new JSONObject(params1), new Response.Listener<JSONObject>() {

                            @Override
                            public void onResponse(JSONObject response) {
                                Log.e("ERROR", response.toString());
                                //Toast.makeText(BussinessSignupActivity.this, response.toString(), Toast.LENGTH_LONG).show();
                                try {
                                    hideProgressDialog();
                                    String status = response.getString("status");
                                    //Toast.makeText(BussinessSignupActivity.this, status, Toast.LENGTH_LONG).show();
                                    if (status.equals("SUCCESS")) {

                                    }else {

                                        new SweetAlertDialog(NotificationSettingsActivity.this, SweetAlertDialog.ERROR_TYPE)
                                                .setTitleText("Network Error")
                                                .setContentText("Please check your internet connection.")
                                                .setConfirmText("Settings")
                                                .setCancelText("Okay")
                                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                    @Override
                                                    public void onClick(SweetAlertDialog sDialog) {
                                                        sDialog.dismissWithAnimation();
                                                        startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 1);
                                                    }
                                                })
                                                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                    @Override
                                                    public void onClick(SweetAlertDialog sDialog) {
                                                        sDialog.dismissWithAnimation();
                                                    }
                                                })
                                                .show();
                                    }
                                } catch (JSONException e) {

                                    Log.e("ERROR", e.toString());
                                    hideProgressDialog();
                                    new SweetAlertDialog(NotificationSettingsActivity.this, SweetAlertDialog.ERROR_TYPE)
                                            .setTitleText("Network Error")
                                            .setContentText("Please check your internet connection.")
                                            .setConfirmText("Settings")
                                            .setCancelText("Okay")
                                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                @Override
                                                public void onClick(SweetAlertDialog sDialog) {
                                                    sDialog.dismissWithAnimation();
                                                    startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 1);
                                                }
                                            })
                                            .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                @Override
                                                public void onClick(SweetAlertDialog sDialog) {
                                                    sDialog.dismissWithAnimation();
                                                }
                                            })
                                            .show();
                                }
                            }
                        }, new Response.ErrorListener() {

                            @Override
                            public void onErrorResponse(VolleyError error) {

                                Log.e("ERROR", error.toString());
                                hideProgressDialog();
                                new SweetAlertDialog(NotificationSettingsActivity.this, SweetAlertDialog.ERROR_TYPE)
                                        .setTitleText("Network Error")
                                        .setContentText("Please check your internet connection.")
                                        .setConfirmText("Settings")
                                        .setCancelText("Okay")
                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sDialog) {
                                                sDialog.dismissWithAnimation();
                                                startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 1);
                                            }
                                        })
                                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sDialog) {
                                                sDialog.dismissWithAnimation();
                                            }
                                        })
                                        .show();
                            }
                        });
                        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(50 * 1000, 3,
                                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                        // Access the RequestQueue through your singleton class.
                        MySingleton.getInstance(NotificationSettingsActivity.this).addToRequestQueue(jsObjRequest);
                    }
                }

            }
        });
        emailNoti.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(first >1) {
                    if(isChecked){
                        String server_url = getString(R.string.SERVER_URL)+"send-email-update";
                        final String token = getString(R.string.AUTH_KEY);
                        HashMap<String, String> params1 = new HashMap<String, String>();
                        params1.put("secret_key", token);
                        params1.put("id", user_id);
                        params1.put("send_email", "Y");

                        Log.e("avv", new JSONObject(params1).toString());
                        showProgressDialog();
                        JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, server_url, new JSONObject(params1), new Response.Listener<JSONObject>() {

                            @Override
                            public void onResponse(JSONObject response) {
                                Log.e("ERROR", response.toString());
                                //Toast.makeText(BussinessSignupActivity.this, response.toString(), Toast.LENGTH_LONG).show();
                                try {
                                    hideProgressDialog();
                                    String status = response.getString("status");
                                    //Toast.makeText(BussinessSignupActivity.this, status, Toast.LENGTH_LONG).show();
                                    if (status.equals("SUCCESS")) {


                                    }else {

                                        new SweetAlertDialog(NotificationSettingsActivity.this, SweetAlertDialog.ERROR_TYPE)
                                                .setTitleText("Network Error")
                                                .setContentText("Please check your internet connection.")
                                                .setConfirmText("Settings")
                                                .setCancelText("Okay")
                                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                    @Override
                                                    public void onClick(SweetAlertDialog sDialog) {
                                                        sDialog.dismissWithAnimation();
                                                        startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 1);
                                                    }
                                                })
                                                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                    @Override
                                                    public void onClick(SweetAlertDialog sDialog) {
                                                        sDialog.dismissWithAnimation();
                                                    }
                                                })
                                                .show();
                                    }
                                } catch (JSONException e) {

                                    Log.e("ERROR", e.toString());
                                    hideProgressDialog();
                                    new SweetAlertDialog(NotificationSettingsActivity.this, SweetAlertDialog.ERROR_TYPE)
                                            .setTitleText("Network Error")
                                            .setContentText("Please check your internet connection.")
                                            .setConfirmText("Settings")
                                            .setCancelText("Okay")
                                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                @Override
                                                public void onClick(SweetAlertDialog sDialog) {
                                                    sDialog.dismissWithAnimation();
                                                    startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 1);
                                                }
                                            })
                                            .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                @Override
                                                public void onClick(SweetAlertDialog sDialog) {
                                                    sDialog.dismissWithAnimation();
                                                }
                                            })
                                            .show();
                                }
                            }
                        }, new Response.ErrorListener() {

                            @Override
                            public void onErrorResponse(VolleyError error) {

                                Log.e("ERROR", error.toString());
                                hideProgressDialog();
                                new SweetAlertDialog(NotificationSettingsActivity.this, SweetAlertDialog.ERROR_TYPE)
                                        .setTitleText("Network Error")
                                        .setContentText("Please check your internet connection.")
                                        .setConfirmText("Settings")
                                        .setCancelText("Okay")
                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sDialog) {
                                                sDialog.dismissWithAnimation();
                                                startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 1);
                                            }
                                        })
                                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sDialog) {
                                                sDialog.dismissWithAnimation();
                                            }
                                        })
                                        .show();
                            }
                        });
                        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(50 * 1000, 3,
                                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                        // Access the RequestQueue through your singleton class.
                        MySingleton.getInstance(NotificationSettingsActivity.this).addToRequestQueue(jsObjRequest);
                    }else {
                        String server_url = getString(R.string.SERVER_URL)+"send-email-update";
                        final String token = getString(R.string.AUTH_KEY);
                        HashMap<String, String> params1 = new HashMap<String, String>();
                        params1.put("secret_key", token);
                        params1.put("id", user_id);
                        params1.put("send_email", "N");

                        Log.e("avv", new JSONObject(params1).toString());
                        showProgressDialog();
                        JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, server_url, new JSONObject(params1), new Response.Listener<JSONObject>() {

                            @Override
                            public void onResponse(JSONObject response) {
                                Log.e("ERROR", response.toString());
                                //Toast.makeText(BussinessSignupActivity.this, response.toString(), Toast.LENGTH_LONG).show();
                                try {
                                    hideProgressDialog();
                                    String status = response.getString("status");
                                    //Toast.makeText(BussinessSignupActivity.this, status, Toast.LENGTH_LONG).show();
                                    if (status.equals("SUCCESS")) {

                                    }else {

                                        new SweetAlertDialog(NotificationSettingsActivity.this, SweetAlertDialog.ERROR_TYPE)
                                                .setTitleText("Network Error")
                                                .setContentText("Please check your internet connection.")
                                                .setConfirmText("Settings")
                                                .setCancelText("Okay")
                                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                    @Override
                                                    public void onClick(SweetAlertDialog sDialog) {
                                                        sDialog.dismissWithAnimation();
                                                        startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 1);
                                                    }
                                                })
                                                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                    @Override
                                                    public void onClick(SweetAlertDialog sDialog) {
                                                        sDialog.dismissWithAnimation();
                                                    }
                                                })
                                                .show();
                                    }
                                } catch (JSONException e) {

                                    Log.e("ERROR", e.toString());
                                    hideProgressDialog();
                                    new SweetAlertDialog(NotificationSettingsActivity.this, SweetAlertDialog.ERROR_TYPE)
                                            .setTitleText("Network Error")
                                            .setContentText("Please check your internet connection.")
                                            .setConfirmText("Settings")
                                            .setCancelText("Okay")
                                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                @Override
                                                public void onClick(SweetAlertDialog sDialog) {
                                                    sDialog.dismissWithAnimation();
                                                    startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 1);
                                                }
                                            })
                                            .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                @Override
                                                public void onClick(SweetAlertDialog sDialog) {
                                                    sDialog.dismissWithAnimation();
                                                }
                                            })
                                            .show();
                                }
                            }
                        }, new Response.ErrorListener() {

                            @Override
                            public void onErrorResponse(VolleyError error) {

                                Log.e("ERROR", error.toString());
                                hideProgressDialog();
                                new SweetAlertDialog(NotificationSettingsActivity.this, SweetAlertDialog.ERROR_TYPE)
                                        .setTitleText("Network Error")
                                        .setContentText("Please check your internet connection.")
                                        .setConfirmText("Settings")
                                        .setCancelText("Okay")
                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sDialog) {
                                                sDialog.dismissWithAnimation();
                                                startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 1);
                                            }
                                        })
                                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sDialog) {
                                                sDialog.dismissWithAnimation();
                                            }
                                        })
                                        .show();
                            }
                        });
                        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(50 * 1000, 3,
                                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                        // Access the RequestQueue through your singleton class.
                        MySingleton.getInstance(NotificationSettingsActivity.this).addToRequestQueue(jsObjRequest);
                    }
                }

            }
        });
        span = (RadioGroup) findViewById(R.id.span);
        mspan = (RadioButton) findViewById(R.id.mspan);
        wspan = (RadioButton) findViewById(R.id.wspan);
        otspan = (RadioButton) findViewById(R.id.otspan);


        span.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if(checkedId == R.id.mspan){
                    String frequency = "D";
                    freuqency(frequency);
                }
                if(checkedId == R.id.wspan){
                    String frequency = "W";
                    freuqency(frequency);
                }
                if(checkedId == R.id.otspan){
                    String frequency = "BW";
                    freuqency(frequency);
                }
            }
        });
        msgNoti.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(first >1) {
                    if(isChecked){
                        String server_url = getString(R.string.SERVER_URL)+"send-sms-update";
                        final String token = getString(R.string.AUTH_KEY);
                        HashMap<String, String> params1 = new HashMap<String, String>();
                        params1.put("secret_key", token);
                        params1.put("id", user_id);
                        params1.put("send_sms", "Y");

                        Log.e("avv", new JSONObject(params1).toString());
                        showProgressDialog();
                        JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, server_url, new JSONObject(params1), new Response.Listener<JSONObject>() {

                            @Override
                            public void onResponse(JSONObject response) {
                                Log.e("ERROR", response.toString());
                                //Toast.makeText(BussinessSignupActivity.this, response.toString(), Toast.LENGTH_LONG).show();
                                try {
                                    hideProgressDialog();
                                    String status = response.getString("status");
                                    //Toast.makeText(BussinessSignupActivity.this, status, Toast.LENGTH_LONG).show();
                                    if (status.equals("SUCCESS")) {


                                    }else {

                                        new SweetAlertDialog(NotificationSettingsActivity.this, SweetAlertDialog.ERROR_TYPE)
                                                .setTitleText("Network Error")
                                                .setContentText("Please check your internet connection.")
                                                .setConfirmText("Settings")
                                                .setCancelText("Okay")
                                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                    @Override
                                                    public void onClick(SweetAlertDialog sDialog) {
                                                        sDialog.dismissWithAnimation();
                                                        startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 1);
                                                    }
                                                })
                                                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                    @Override
                                                    public void onClick(SweetAlertDialog sDialog) {
                                                        sDialog.dismissWithAnimation();
                                                    }
                                                })
                                                .show();
                                    }
                                } catch (JSONException e) {

                                    Log.e("ERROR", e.toString());
                                    hideProgressDialog();
                                    new SweetAlertDialog(NotificationSettingsActivity.this, SweetAlertDialog.ERROR_TYPE)
                                            .setTitleText("Network Error")
                                            .setContentText("Please check your internet connection.")
                                            .setConfirmText("Settings")
                                            .setCancelText("Okay")
                                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                @Override
                                                public void onClick(SweetAlertDialog sDialog) {
                                                    sDialog.dismissWithAnimation();
                                                    startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 1);
                                                }
                                            })
                                            .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                @Override
                                                public void onClick(SweetAlertDialog sDialog) {
                                                    sDialog.dismissWithAnimation();
                                                }
                                            })
                                            .show();
                                }
                            }
                        }, new Response.ErrorListener() {

                            @Override
                            public void onErrorResponse(VolleyError error) {

                                Log.e("ERROR", error.toString());
                                hideProgressDialog();
                                new SweetAlertDialog(NotificationSettingsActivity.this, SweetAlertDialog.ERROR_TYPE)
                                        .setTitleText("Network Error")
                                        .setContentText("Please check your internet connection.")
                                        .setConfirmText("Settings")
                                        .setCancelText("Okay")
                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sDialog) {
                                                sDialog.dismissWithAnimation();
                                                startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 1);
                                            }
                                        })
                                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sDialog) {
                                                sDialog.dismissWithAnimation();
                                            }
                                        })
                                        .show();
                            }
                        });
                        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(50 * 1000, 3,
                                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                        // Access the RequestQueue through your singleton class.
                        MySingleton.getInstance(NotificationSettingsActivity.this).addToRequestQueue(jsObjRequest);
                    }else {
                        String server_url = getString(R.string.SERVER_URL)+"send-sms-update";
                        final String token = getString(R.string.AUTH_KEY);
                        HashMap<String, String> params1 = new HashMap<String, String>();
                        params1.put("secret_key", token);
                        params1.put("id", user_id);
                        params1.put("send_sms", "N");

                        Log.e("avv", new JSONObject(params1).toString());
                        showProgressDialog();
                        JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, server_url, new JSONObject(params1), new Response.Listener<JSONObject>() {

                            @Override
                            public void onResponse(JSONObject response) {
                                Log.e("ERROR", response.toString());
                                //Toast.makeText(BussinessSignupActivity.this, response.toString(), Toast.LENGTH_LONG).show();
                                try {
                                    hideProgressDialog();
                                    String status = response.getString("status");
                                    //Toast.makeText(BussinessSignupActivity.this, status, Toast.LENGTH_LONG).show();
                                    if (status.equals("SUCCESS")) {

                                    }else {

                                        new SweetAlertDialog(NotificationSettingsActivity.this, SweetAlertDialog.ERROR_TYPE)
                                                .setTitleText("Network Error")
                                                .setContentText("Please check your internet connection.")
                                                .setConfirmText("Settings")
                                                .setCancelText("Okay")
                                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                    @Override
                                                    public void onClick(SweetAlertDialog sDialog) {
                                                        sDialog.dismissWithAnimation();
                                                        startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 1);
                                                    }
                                                })
                                                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                    @Override
                                                    public void onClick(SweetAlertDialog sDialog) {
                                                        sDialog.dismissWithAnimation();
                                                    }
                                                })
                                                .show();
                                    }
                                } catch (JSONException e) {

                                    Log.e("ERROR", e.toString());
                                    hideProgressDialog();
                                    new SweetAlertDialog(NotificationSettingsActivity.this, SweetAlertDialog.ERROR_TYPE)
                                            .setTitleText("Network Error")
                                            .setContentText("Please check your internet connection.")
                                            .setConfirmText("Settings")
                                            .setCancelText("Okay")
                                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                @Override
                                                public void onClick(SweetAlertDialog sDialog) {
                                                    sDialog.dismissWithAnimation();
                                                    startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 1);
                                                }
                                            })
                                            .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                @Override
                                                public void onClick(SweetAlertDialog sDialog) {
                                                    sDialog.dismissWithAnimation();
                                                }
                                            })
                                            .show();
                                }
                            }
                        }, new Response.ErrorListener() {

                            @Override
                            public void onErrorResponse(VolleyError error) {

                                Log.e("ERROR", error.toString());
                                hideProgressDialog();
                                new SweetAlertDialog(NotificationSettingsActivity.this, SweetAlertDialog.ERROR_TYPE)
                                        .setTitleText("Network Error")
                                        .setContentText("Please check your internet connection.")
                                        .setConfirmText("Settings")
                                        .setCancelText("Okay")
                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sDialog) {
                                                sDialog.dismissWithAnimation();
                                                startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 1);
                                            }
                                        })
                                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sDialog) {
                                                sDialog.dismissWithAnimation();
                                            }
                                        })
                                        .show();
                            }
                        });
                        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(50 * 1000, 3,
                                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                        // Access the RequestQueue through your singleton class.
                        MySingleton.getInstance(NotificationSettingsActivity.this).addToRequestQueue(jsObjRequest);
                    }
                }

            }
        });



    }
    public void freuqency(String freq){
        String server_url = getString(R.string.SERVER_URL)+"notification-frequency";
        final String token = getString(R.string.AUTH_KEY);
        HashMap<String, String> params1 = new HashMap<String, String>();
        params1.put("secret_key", token);
        params1.put("id", user_id);
        if(freq.equals("BW")){
            params1.put("notify_frequency", "B");
        }else{
            params1.put("notify_frequency", freq);
        }

        Log.e("avv", new JSONObject(params1).toString());
        showProgressDialog();
        JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, server_url, new JSONObject(params1), new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.e("ERROR", response.toString());
                //Toast.makeText(BussinessSignupActivity.this, response.toString(), Toast.LENGTH_LONG).show();
                try {
                    hideProgressDialog();
                    String status = response.getString("status");
                    //Toast.makeText(BussinessSignupActivity.this, status, Toast.LENGTH_LONG).show();
                    if (status.equals("SUCCESS")) {

                    }else {

                        new SweetAlertDialog(NotificationSettingsActivity.this, SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("Network Error")
                                .setContentText("Please check your internet connection.")
                                .setConfirmText("Settings")
                                .setCancelText("Okay")
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sDialog) {
                                        sDialog.dismissWithAnimation();
                                        startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 1);
                                    }
                                })
                                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sDialog) {
                                        sDialog.dismissWithAnimation();
                                    }
                                })
                                .show();
                    }
                } catch (JSONException e) {

                    Log.e("ERROR", e.toString());
                    hideProgressDialog();
                    new SweetAlertDialog(NotificationSettingsActivity.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Network Error")
                            .setContentText("Please check your internet connection.")
                            .setConfirmText("Settings")
                            .setCancelText("Okay")
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();
                                    startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 1);
                                }
                            })
                            .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();
                                }
                            })
                            .show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                Log.e("ERROR", error.toString());
                hideProgressDialog();
                new SweetAlertDialog(NotificationSettingsActivity.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Network Error")
                        .setContentText("Please check your internet connection.")
                        .setConfirmText("Settings")
                        .setCancelText("Okay")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();
                                startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 1);
                            }
                        })
                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();
                            }
                        })
                        .show();
            }
        });
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(50 * 1000, 3,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        // Access the RequestQueue through your singleton class.
        MySingleton.getInstance(this).addToRequestQueue(jsObjRequest);
    }
    public void getNoti(){
        String server_url = getString(R.string.SERVER_URL)+"customer-notification";
        final String token = getString(R.string.AUTH_KEY);
        HashMap<String, String> params1 = new HashMap<String, String>();
        params1.put("secret_key", token);
        params1.put("id", user_id);

        Log.e("avv", new JSONObject(params1).toString());
        showProgressDialog();
        JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, server_url, new JSONObject(params1), new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.e("ERROR", response.toString());
                //Toast.makeText(BussinessSignupActivity.this, response.toString(), Toast.LENGTH_LONG).show();
                try {
                    hideProgressDialog();
                    String status = response.getString("status");
                    //Toast.makeText(BussinessSignupActivity.this, status, Toast.LENGTH_LONG).show();
                    if (status.equals("SUCCESS")) {

                        String data = response.getString("data");

                        JSONObject datObj = new JSONObject(data);
                        String send_notification = datObj.getString("send_notification");
                        String send_sms = datObj.getString("send_sms");
                        String send_email = datObj.getString("send_email");
                        String send_promotional = datObj.getString("send_promotional");
                        String notify_frequency = datObj.getString("notify_frequency");
                        String category = datObj.getString("category");
                        String user_category = datObj.getString("user_category");

                        if(send_notification.equals("Y")){
                            pushNoti.setChecked(true);
                        }else {
                            pushNoti.setChecked(false);
                        }
                        if(notify_frequency.equals("W")){
                            wspan.setChecked(true);
                        }else if(notify_frequency.equals("D")){
                            mspan.setChecked(false);
                        }else if(notify_frequency.equals("B")){
                            otspan.setChecked(false);
                        }

                        if(send_sms.equals("Y")){
                            msgNoti.setChecked(true);
                        }else {
                            msgNoti.setChecked(false);
                        }

                        if(send_email.equals("Y")){
                            emailNoti.setChecked(true);
                        }else {
                            emailNoti.setChecked(false);
                        }

                        if(send_promotional.equals("Y")){
                            promoNoti.setChecked(true);
                        }else {
                            promoNoti.setChecked(false);
                        }
                        first = 3;

                        JSONArray catArr = new JSONArray(category);
                        orderlist = new ArrayList<HashMap<String, String>>();
                        for (int i = 0; i <catArr.length() ; i++) {
                            String id = catArr.getJSONObject(i).getString("id");
                            String name = catArr.getJSONObject(i).getString("name");
                            HashMap<String, String> list = new HashMap<String, String>();
                            list.put(SHOPPER_ID, id);
                            list.put(SHOPPER_NAME, name);
                            list.put(SHOPPER_DESC, user_category);
                            list.put(DATE, String.valueOf(catArr.length()));
                            orderlist.add(list);
                        }
                        Category_Notification_Adapter adapter = new Category_Notification_Adapter(NotificationSettingsActivity.this, orderlist);
                        rv_MyShopper.setAdapter(adapter);
                    }else {

                        new SweetAlertDialog(NotificationSettingsActivity.this, SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("Network Error")
                                .setContentText("Please check your internet connection.")
                                .setConfirmText("Settings")
                                .setCancelText("Okay")
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sDialog) {
                                        sDialog.dismissWithAnimation();
                                        startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 1);
                                    }
                                })
                                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sDialog) {
                                        sDialog.dismissWithAnimation();
                                    }
                                })
                                .show();
                    }
                } catch (JSONException e) {

                    Log.e("ERROR", e.toString());
                    hideProgressDialog();
                    new SweetAlertDialog(NotificationSettingsActivity.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Network Error")
                            .setContentText("Please check your internet connection.")
                            .setConfirmText("Settings")
                            .setCancelText("Okay")
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();
                                    startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 1);
                                }
                            })
                            .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();
                                }
                            })
                            .show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                Log.e("ERROR", error.toString());
                hideProgressDialog();
                new SweetAlertDialog(NotificationSettingsActivity.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Network Error")
                        .setContentText("Please check your internet connection.")
                        .setConfirmText("Settings")
                        .setCancelText("Okay")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();
                                startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 1);
                            }
                        })
                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();
                            }
                        })
                        .show();
            }
        });
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(50 * 1000, 3,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        // Access the RequestQueue through your singleton class.
        MySingleton.getInstance(this).addToRequestQueue(jsObjRequest);
    }
    Dialog mDialog;
    private void showProgressDialog(){
        mDialog = new Dialog(this);
        mDialog.setCancelable(false);
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        mDialog.setContentView(R.layout.custom_progress_dialog);
        mDialog.show();
    }
    private void hideProgressDialog(){
        mDialog.dismiss();
    }
}
